/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
//import java.io.PrintWriter;
//import java.io.StringWriter;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.JTextComponent;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.Trial_Category;
import org.quantumleaphealth.model.trial.Trial_CategoryPK;
import org.quantumleaphealth.model.trial.Trial.Registry;
import org.quantumleaphealth.model.trial.TrialResponse;
import org.quantumleaphealth.model.trial.Trial_Mutation;
import org.quantumleaphealth.model.trial.Trial_MutationPK;
import org.quantumleaphealth.xml.DecodingException;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;





import com.lowagie.text.Font;

/**
 * Modal dialog that retrieves and stores trial data to a database.
 * 
 * @author Tom Bechtold
 * @version 2008-10-20
 */
class RecordChooser extends JDialog implements ActionListener
{
	/**
	 * The username used to authenticate the user on the database server
	 */
	private final JTextField username;
	/**
	 * The password used to authenticate the user on the database server
	 */
	private final JPasswordField password;
	/**
	 * The database server
	 */
	private final JComboBox server = new JComboBox();

	/**
	 * @return the server
	 */
	public JComboBox getServerComboBox()
	{
		return server;
	}

	/**
	 * The criterion used to search for the trial's primary ID
	 */
	private final JTextField criterion;
	/**
	 * Whether or not to include listed trials
	 */
	private final JCheckBox listed;
	/**
	 * The button to click for searching
	 */
	private final JButton search;
	/**
	 * The list of results from the search
	 */
	private final JList results = new JList();
	/**
	 * The button to click for OK
	 */
	private final JButton ok;
	/**
	 * Whether or not the cancel button was pressed
	 */
	private boolean cancelPressed = false;
	/**
	 * The widths of the string columns in the current connection
	 */
	private Map<String, Integer> stringWidths;

	/**
	 * The name of the JDBC resource bundle
	 */
	private static final String RESOURCE_BUNDLE = "database";
	/**
	 * The key to lookup server urls in JDBC resource bundle
	 */
	private static final String KEY_URL = "org.quantumleaphealth.webservice";
	/**
	 * The key suffix to lookup descriptive name in JDBC resource bundle
	 */
	private static final String KEY_DESCRIPTION = "description";
	/**
	 * The key suffix to lookup web service url in resource bundle
	 */
	private static final String KEY_WS_URL = "url";
	
	/**
	 * The key suffix to lookup server port in resource bundle
	 */
	private static final String KEY_WS_PORT = "port";
	/**
	 * The property name for setting database login
	 */
	private static final String PROPERTY_USERNAME = "user";
	/**
	 * The property name for setting database password
	 */
	private static final String PROPERTY_PASSWORD = "password";

	/**
	 * The database servers' names, users and respective properties. Note that
	 * the development server has additional debugging properties.
	 */
	private static final List<Properties> SERVERS = loadServerProperties();
	/**
	 * JDK logger
	 */
	private static final Logger LOGGER = Logger.getLogger(RecordChooser.class.getName());

	/**
	 * The default SQL used to load all trials' fields that match either a
	 * primary id or the name. This SQL is SQL-92 compliant.
	 * 
	 * @see #actionPerformed(ActionEvent)
	 * @see #KEY_FIND
	 */
/*	
	private static final String SQL_FIND = "SELECT id, registry, open, listed, primaryid, type, secondarytype, phase, sponsor, "
			+ "name, title, purpose, treatmentPlan, procedures, burden, duration, followup, linkMarshaled, eligibilityCriteriaMarshaled, lastModified "
			+ "FROM trial WHERE ((LOWER(primaryid) LIKE LOWER(?)) or (LOWER(name) LIKE ?)) AND listed=? ORDER BY primaryId";
*/			
	
	private static final String SQL_FIND = "(SELECT id, registry, open, listed, primaryid, type, secondarytype, phase, sponsor, name, title, purpose, treatmentPlan, procedures, burden, duration, followup, linkMarshaled, eligibilityCriteriaMarshaled, lastModified, postdate, 'Current' as recordStatus, (SELECT last_value + 1 from trial_shadow_id_seq) as sortOrder,'' as versioncomment, pdqid  FROM trial WHERE ((LOWER(primaryid) LIKE LOWER(?)) or (LOWER(name) LIKE ?)) AND listed=? ) union (SELECT trialid, registry, open, listed, primaryid, type, secondarytype, phase, sponsor, name, title, purpose, treatmentPlan, procedures, burden, duration, followup, linkMarshaled, eligibilityCriteriaMarshaled, lastModified, postdate, to_char(storedate, 'Mon DD, YYYY HH12:MI AM') as recordStatus, id as sortOrder, versioncomment, pdqid FROM trial_shadow WHERE ((LOWER(primaryid) LIKE LOWER(?)) or (LOWER(name) LIKE ?)) ) order by primaryid, sortorder desc";

	private static final String SQL_FIND_CATEGORIES = "SELECT category from trial_category where trialid = ?";
	
	private static final String SQL_DELETE_CATEGORIES = "DELETE from trial_category where trialid = ?";
	
	private static final String SQL_INSERT_CATEGORIES = "INSERT INTO trial_category( trialid, category, created ) values ( ?, ?, ? )";
	
	private static final String SQL_FIND_MUTATIONS = "SELECT mutation from trial_mutation where trialid = ?";
	
	private static final String SQL_DELETE_MUTATIONS = "DELETE from trial_mutation where trialid = ?";
	
	private static final String SQL_INSERT_MUTATIONS = "INSERT INTO trial_mutation( trialid, mutation, created ) values ( ?, ?, ? )";

	private static final String SQL_INSERT_TRIAL_SHADOW = "insert into trial_shadow (id, trialid, registry, open, primaryid, type, phase, sponsor, name, title, purpose, treatmentplan, procedures, burden, duration, followup, linkmarshaled, eligibilitycriteriamarshaled, eligibilitycriterialastmodified, lastbatchmatched, lastmodified, lastregistered, listed, clinicaltrialid, secondarytype, storedate, versioncomment, postdate) " + 
                                                          "values " +                     //1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7
                                                          "(nextval('trial_shadow_id_seq'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	/**
	 * The SQL character used in LIKE clause for wildcard
	 */
	private static final String SQL_WILDCARD = "%";
	/**
	 * The SQL character used for parameter replacement
	 */
	private static final char SQL_PARAMETER = '?';
	/**
	 * The key used to lookup <code>SQL_FIND</code> in the database resource
	 * bundle.
	 * 
	 * @see #SQL_FIND
	 */
	private static final String KEY_FIND = "sql-find";
	/**
	 * The default SQL used to update a trial's fields. The following fields are
	 * not updated:
	 * <ul>
	 * <li>id</li>
	 * <li>registry</li>
	 * <li>open</li>
	 * </ul>
	 * This SQL is SQL-92 compliant.
	 * 
	 * @see #store(Trial)
	 * @see #KEY_UPDATE
	 */
	private static final String SQL_UPDATE = "UPDATE trial SET listed=?, primaryId=?, type=?, secondarytype=?, phase=?, sponsor=?, "
			+ "name=?, title=?, purpose=?, treatmentPlan=?, procedures=?, burden=?, duration=?, followup=?, linkMarshaled=?, "
			+ "eligibilityCriteriaMarshaled=?, eligibilityCriteriaLastModified=?, lastModified=?, postDate=? " + "WHERE id=?";
	/**
	 * The key used to lookup <code>SQL_STORE</code> in the database resource
	 * bundle. If the bundle does not contain a value then the database is
	 * read-only.
	 * 
	 * @see #store(Trial)
	 * @see #SQL_UPDATE
	 */
	private static final String KEY_UPDATE = "sql-update";
	/**
	 * The default SQL used to load patient data that match a principal and
	 * credentials. The principal is matched using the SQL <code>LIKE</code>
	 * operator while the credentials are matched exactly. This SQL is SQL-92
	 * compliant.
	 * 
	 * @see #getPatients
	 * @see #KEY_FIND
	 */
	private static final String SQL_PATIENTS = "SELECT principal, patientHistoryMarshaled, userType, avatarHistoryMarshaled "
			+ "FROM UserPatient WHERE ((LOWER(principal) LIKE LOWER(?)) and (credentials = ?)) ORDER BY principal, id";
	/**
	 * The key used to lookup <code>SQL_FIND</code> in the database resource
	 * bundle.
	 * 
	 * @see #SQL_FIND
	 */
	private static final String KEY_PATIENTS = "sql-patients";

	/**
	 * String constant representing description of 'Production' Persistence
	 * Environment.
	 */
	private static final String SERVER_DESCRIPTION_VALUE_PRODUCTION = "PRODUCTION";

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -4515969856326199200L;

	/**
	 * Returns server properties loaded from the JDBC resource bundle. The
	 * returned properties include the connection URL stored under key
	 * <code>KEY_URL</code> and all key-value pairs whose resource bundle key
	 * starts with <code>KEY_URL + URL</code>.
	 * 
	 * @return server properties loaded from the JDBC resource bundle or
	 *         <code>null</code> if the resource bundle cannot be loaded or no
	 *         urls found
	 */
	private static List<Properties> loadServerProperties()
	{
		List<Properties> list = new LinkedList<Properties>();
		try
		{
			// Load each server's properties from the resource bundle
			ResourceBundle resourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE);
			// Load optional drivers ala jdbc.drivers property
			/*try
			{
				StringTokenizer tokens = new StringTokenizer(resourceBundle.getString("jdbc.drivers"), ":");
				while (tokens.hasMoreTokens())
					Class.forName(tokens.nextToken());
			}
			catch (Throwable throwable)
			{
			}*/
			StringTokenizer tokens = new StringTokenizer(resourceBundle.getString(KEY_URL), ",");
			while (tokens.hasMoreTokens())
			{
				// Verify the url can be handled by a loaded JDBC driver
				String url = tokens.nextToken().trim();

				// Load all properties for the url (with colons, spaces and
				// equals replaced by underscore)
				Properties properties = new Properties();
				properties.setProperty(KEY_URL, url);
				String prefix = KEY_URL + '.' + url + '.';
				Enumeration<String> keys = resourceBundle.getKeys();
				while (keys.hasMoreElements())
				{
					String key = keys.nextElement();
					if (key.startsWith(prefix))
						properties.setProperty(key.substring(prefix.length()), trim(resourceBundle
								.getString(key)));
				}
				list.add(properties);
			}
		}
		catch (MissingResourceException missingResourceException)
		{
			// If resource bundle not found then return null
			return null;
		}
		return list.size() == 0 ? null : list;
	}

	/**
	 * Describes a trial
	 */
	private class TrialDescription
	{
		/**
		 * The trial being described
		 */
		private final Trial trial;
		private final String currentStatus;
		private final String versionComment;

		/**
		 * Instantiate an descriptive trial object
		 * 
		 * @param trial
		 *            the trial
		 * @throws IllegalArgumentException
		 *             if parameter or its primary id is empty
		 */
		TrialDescription(Trial trial, String currentStatus, String versionComment) throws IllegalArgumentException
		{
/*			if ((trial == null) || (trial.getId() == null) // || (trial.getId().longValue() <= 0)
					|| (trim(trial.getPrimaryID()) == null))
				throw new IllegalArgumentException("trial's id and/or primaryID not specified");*/
			this.trial = trial;
			this.currentStatus = currentStatus;
			this.versionComment = versionComment;
		}

		/**
		 * Returns the primary id and, if present, the name
		 * 
		 * @return the primary id and, if present, the name
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			StringBuilder builder = new StringBuilder(trial.getPrimaryID());
			Registry registry = trial.getRegistry();
			if ((registry != null) && !Registry.UNASSIGNED.equals(registry))
				builder.append(" (").append(registry).append(')');
			String name = trim(trial.getName());
			if (name != null)
				builder.append(": ").append(name);
			
			return String.format("%-21.21s: %50.50s: %s", currentStatus, builder.toString(), versionComment);
		}
	}

public RecordChooser() 
{
	this.username = null;
	this.search = null;
	this.password = null;
	this.ok = null;
	this.listed = null;
	this.criterion = null;
}

public static void main( String... argv)
{
	Trial trial = new Trial();
	trial.setName("This is my trial name...");
	trial.setPrimaryID("003");
	trial.setId( 1L );
	
	TrialDescription td = new RecordChooser().new TrialDescription( trial, "Some Status I will tack onto the end.", "Some version comment");
}

	/**
	 * Associate the dialog with an owner and layout the input elements.
	 * 
	 * @param owner
	 *            the owner of this modal dialog
	 */
	RecordChooser(Frame owner)
	{
		// Set the title to the owner's title
		super(owner, owner.getTitle(), true);

		// Create input fields and buttons; prepopulate defaults
		String osUser = trim(System.getProperty("user.name"));
		username = new JTextField(osUser, 7);
		username.setToolTipText("User name for logging in to database");
		// Build server list and determine database selection and username
		int index = 0;
		boolean dbUserSet = false;
		while ((SERVERS != null) && (index < SERVERS.size()))
		{
			Properties props = SERVERS.get(index);
			server.addItem(props.getProperty(KEY_DESCRIPTION, props.getProperty(KEY_URL, "server " + (index + 1))));
			server.setSelectedIndex(index);
			index++;
		}
		server.setToolTipText("Database server that stores trial information");
		password = new JPasswordField(10);
		password.setToolTipText("Password for logging in to database");
		criterion = new JTextField(20);
		criterion.setToolTipText("Protocol's primary identifier (whole or part) to search for");
		listed = new JCheckBox("Listed?", true);
		listed.setToolTipText("Trial is currently listed");
		search = new JButton("Search");
		search.setToolTipText("Click to search for a trial on its primary identifier");
		ok = new JButton("Ok");
		ok.setToolTipText("Click to proceed");
		JButton cancel = new JButton("Cancel");
		cancel.setToolTipText("Click to cancel");

		// Both columns fill horizontally and have horizontal gap;
		// Remainder is the last column in row
		JPanel mainPanel = new JPanel(new BorderLayout());
		JPanel row = new JPanel();
		row.add(new JLabel("Server:"));
		row.add(server);
		row.add(new JLabel("Username:"));
		row.add(username);
		row.add(new JLabel("Password:"));
		row.add(password);
		mainPanel.add(row, BorderLayout.NORTH);
		row = new JPanel();
		row.add(new JLabel("Primary ID:"));
		row.add(criterion);
		row.add(listed);
		row.add(search);
		mainPanel.add(row, BorderLayout.CENTER);

		// Results list is single-selection and has minimum size
		results.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		results.setToolTipText("Click on a trial to select");
		results.setFont( new java.awt.Font("Courier New", Font.BOLD, 10) );
		results.setVisibleRowCount(10);
		JScrollPane scrollPane = new JScrollPane(results);

		// OK/Cancel buttons go along bottom
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(ok);
		buttonPanel.add(cancel);
		add(mainPanel, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		// Entering a text field selects its contents
		FocusListener focusListener = new FocusAdapter()
		{
			@Override
			public void focusGained(FocusEvent focusEvent)
			{
				if (focusEvent.getComponent() instanceof JTextComponent)
					((JTextComponent) (focusEvent.getComponent())).select(0, Integer.MAX_VALUE);
			}
		};
		username.addFocusListener(focusListener);
		password.addFocusListener(focusListener);
		criterion.addFocusListener(focusListener);
		// Changing the server enables and focuses authentication fields
		server.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent itemEvent)
			{
				stringWidths = null;
				username.setEnabled(true);
				password.setEnabled(true);
				if (trim(username.getText()) == null)
					username.requestFocusInWindow();
				else
					password.requestFocusInWindow();
			}
		});
		// Selecting a result enables the ok button; double-clicking simulates
		// ok button.
		results.addListSelectionListener(new ListSelectionListener()
		{
			public void valueChanged(ListSelectionEvent listSelectionEvent)
			{
				ok.setEnabled(true);
			}
		});
		results.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent mouseEvent)
			{
				if (mouseEvent.getClickCount() < 2)
					return;
				results.ensureIndexIsVisible(results.locationToIndex(mouseEvent.getPoint()));
				ok();
			}
		});
		// Search button and criterion entry are handled identically by this
		// class
		criterion.addActionListener(this);
		search.addActionListener(this);
		// OK/Cancel are dispatched to respective methods; closing window =
		// cancel
		ok.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent actionEvent)
			{
				ok();
			}
		});
		cancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent actionEvent)
			{
				cancel();
			}
		});
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent windowEvent)
			{
				cancel();
			}
		});

		// Pack frame, center in owner and restrict resizing
		pack();
		setLocationRelativeTo(owner);
		setResizable(false);
	}

	/**
	 * Returns whether server information is loaded into this component
	 * 
	 * @return <code>true</code> if server information is loaded into this
	 *         component
	 */
	static boolean isLoaded()
	{
		return (SERVERS != null) && (SERVERS.size() > 0);
	}

	/**
	 * Returns the root pane that calls <code>cancel()</code> if the escape key
	 * is pressed.
	 * 
	 * @return the root pane
	 * @see javax.swing.JDialog#createRootPane()
	 */
	@Override
	protected JRootPane createRootPane()
	{
		final String esc = "ESCAPE";
		// Get root pane from superclass
		JRootPane rootPane = super.createRootPane();
		// Assign escape keystroke to escape action
		rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(esc), esc);
		// Escape action calls cancel()
		rootPane.getActionMap().put(esc, new AbstractAction()
		{
			public void actionPerformed(ActionEvent actionEvent)
			{
				cancel();
			}

			private static final long serialVersionUID = -2197365472248495006L;
		});
		return rootPane;
	}

	/**
	 * Return a trial that is searched for and selected by the user. This method
	 * enables fields in the dialog and then shows it. The return value is the
	 * selected list item.
	 * 
	 * @return the desired trial or <code>null</code> if none selected
	 */
	Trial load()
	{
		// Enable fields and show; authentication fields reflect factory status
		criterion.setEnabled(true);
		listed.setEnabled(true);
		search.setEnabled(true);
		results.setEnabled(true);
		ok.setEnabled(results.getModel().getSize() > 0);
		// Request focus on p/w if username already stored
		if (password.isEnabled() && (trim(username.getText()) != null))
			password.requestFocusInWindow();
		else if (username.isEnabled())
			username.requestFocusInWindow();
		else
			criterion.requestFocusInWindow();
		setVisible(true);

		// Return the selected result
		Object result = results.getSelectedValue();
		return (result == null) || !(result instanceof TrialDescription) ? null : ((TrialDescription) (result)).trial;
	}

	/**
	 * Store a trial into a user-selected database. This method prompts the user
	 * for authentication if connection has not been established before.
	 * 
	 * @param trial
	 *            the trial to save
	 * @return whether or not the trial was stored
	 */
	boolean store(Trial trial)
	{
		if (trial == null)
			return false;
		// ID mandatory
		if ((trial.getId() == null)) // || (trial.getId().longValue() <= 0))
		{
			JOptionPane.showMessageDialog(this, "This trial cannot be saved to a database", "Cannot store trial",
					JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		// Show dialog to collect missing authentication information or switch
		// database
		criterion.setEnabled(false);
		listed.setEnabled(false);
		search.setEnabled(false);
		results.setEnabled(false);
		ok.setEnabled(true);
		cancelPressed = false;
		setVisible(true);
		// If user cancels then return nothing
		if (cancelPressed || username.isEnabled())
			return false;

		// Persist trial using database stored procedure if available
		Properties properties = getConnectionProperties();

		
		 /* BCT-340 Trials with no matchable criterion(sic) should fail. Refuse
		 * to save to Production when Eligibility Criteria is empty
		 */
		
		String serverDescription = (properties == null) ? null : properties.getProperty(KEY_DESCRIPTION);

		if ((trial.getEligibilityCriteria() == null || trial.getEligibilityCriteria().size() < 1)
				&& SERVER_DESCRIPTION_VALUE_PRODUCTION.equalsIgnoreCase(serverDescription))
		{
			JOptionPane
					.showMessageDialog(
							this,
							"Trials lacking Eligibility Criteria may not be saved to Production. Are you working with the most recent version of your Trial?",
							"Action Not Permitted", JOptionPane.WARNING_MESSAGE);
			return false;
		}

		assert (properties != null) : "cannot get properties for validated connection";
		Throwable connectionProblem = null;
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String sqlUpdate = properties.getProperty(KEY_UPDATE, SQL_UPDATE);
		try
		{
			
			TrialResponse trialResponse = new TrialResponse(trial,"","",trial.getEligibilityCriteriaMarshaled(),null);
			
			String url = trim(properties.getProperty(KEY_WS_URL));
			int port = Integer.parseInt(trim(properties.getProperty(KEY_WS_PORT)));
			
			URI uri = UriBuilder.fromUri(url+"/bct-trialcode-service-v2/services/trialcode/v53/store")
					.port(port)
					.build();
			
			Client client = ClientBuilder.newClient().register(MoxyJsonFeature.class);
			Response response = client.target(uri)
					.request()
					.put(Entity.entity(trialResponse, MediaType.APPLICATION_JSON));
			
			String output = response.readEntity(String.class);
			
			if("false".equalsIgnoreCase(output))
				connectionProblem = new Throwable();
						
		}
		catch (Throwable throwable)
		{
			connectionProblem = throwable;
		}
	
		setCursor(Cursor.getDefaultCursor());
		if (connectionProblem != null)
		{
			LOGGER.log(Level.WARNING, "Cannot store trial using " + sqlUpdate, connectionProblem);
			JOptionPane.showMessageDialog(this, connectionProblem.getMessage(),
					"Cannot store trial using " + sqlUpdate, JOptionPane.WARNING_MESSAGE);
			return false;
		}
		JOptionPane.showMessageDialog(null, "Trial " + trial.getPrimaryID() + " stored", "Trial stored",
				JOptionPane.INFORMATION_MESSAGE);
		return true;
	}

	/**
	 * @return the widths of the string columns (lower-case) for the loaded
	 *         trial or <code>null</code> if no loaded trial
	 */
	Map<String, Integer> getStringWidths()
	{
		return stringWidths;
	}

	/**
	 * Sets an object or <code>null</code> into a callable statement. This
	 * method supports the following SQL types:
	 * <ul>
	 * <li><code>BIGINT</code>: <code>Long</code></li>
	 * <li><code>BOOLEAN</code>: <code>Boolean</code></li>
	 * <li><code>CLOB, LONGVARCHAR, VARCHAR</code>: <code>String</code></li>
	 * <li><code>TIMESTAMP</code>: <code>java.util.Date</code></li>
	 * <li><code>BLOB</code>: <code>byte[]</code></li>
	 * <li><code>INTEGER</code>: integers and enumerations (stored ordinally)</li>
	 * </ul>
	 * 
	 * @param preparedStatement
	 *            the statement to store the value into
	 * @param parameterIndex
	 *            the parameter index to store the value at
	 * @param type
	 *            the SQL type defined in <code>SqlTypes</code>
	 * @param value
	 *            the value to store
	 * @throws ClassCastException
	 *             if <code>value</code> is not represented by <code>type</code>
	 * @throws SQLException
	 *             if parameterIndex does not correspond to a parameter marker
	 *             in the SQL statement; if a database access error occurs or
	 *             this method is called on a closed PreparedStatement
	 *             SQLFeatureNotSupportedException if sqlType is a ARRAY, BLOB,
	 *             CLOB, DATALINK, JAVA_OBJECT, NCHAR, NCLOB, NVARCHAR,
	 *             LONGNVARCHAR, REF, ROWID, SQLXML or STRUCT data type and the
	 *             JDBC driver does not support this data type
	 * @see java.sql.Types
	 */
	@SuppressWarnings("unchecked")
	private void setObject(PreparedStatement preparedStatement, int parameterIndex, int type, Object value)
			throws ClassCastException, SQLException
	{
		if (preparedStatement == null)
			return;
		if ((value == null) || ((value instanceof byte[]) && (((byte[]) (value)).length == 0)))
		{
			preparedStatement.setNull(parameterIndex, type);
			return;
		}
		// Use appropriate set statement based upon SQL type; cast exceptions
		// handled by invoking routine
		switch (type)
		{
		case Types.BIT:
		case Types.BOOLEAN:
			preparedStatement.setBoolean(parameterIndex, ((Boolean) (value)).booleanValue());
			return;
		case Types.BIGINT:
			preparedStatement.setLong(parameterIndex, ((Long) (value)).longValue());
			return;
		case Types.SMALLINT:
			preparedStatement.setShort( parameterIndex, ((Short) (value)).shortValue() );
			return;
		case Types.CLOB:
		case Types.LONGVARCHAR:
		case Types.VARCHAR:
			preparedStatement.setString(parameterIndex, (String) (value));
			return;
		case Types.TIMESTAMP:
			preparedStatement.setTimestamp(parameterIndex, new Timestamp(((Date) (value)).getTime()));
			return;
		case Types.BLOB:
			preparedStatement.setBytes(parameterIndex, (byte[]) (value));
			return;
		case Types.INTEGER:
			if (value instanceof Enum)
				value = new Integer(((Enum) (value)).ordinal());
			preparedStatement.setInt(parameterIndex, ((Integer) (value)).intValue());
			return;
		}
	}

	/**
	 * If a trial is selected then close the dialog, otherwise validate the
	 * database connection and disable authentication fields.
	 */
	private void ok()
	{
		// If dialog is in loading mode then simply close it since a result has
		// been selected
		if (search.isEnabled())
		{
			setVisible(false);
			return;
		}
		// If dialog is in storing mode then validate connection by getting its
		// string widths
		Properties properties = getConnectionProperties();
		if (properties == null)
			return;
		ok.setEnabled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		//Connection connection = null;
		//PreparedStatement statement = null;
		//Throwable connectionProblem = null;
		//String sql = properties.getProperty(KEY_FIND, SQL_FIND);
		/*try
		{
			connection = getConnection(properties);
			statement = connection.prepareStatement(sql);
			setSearchParameters(statement, null, false);
			stringWidths = getStringWidths(statement);
		}
		catch (Throwable throwable)
		{
			connectionProblem = throwable;
		}
		finally
		{
			// Always close database pointers when finished
			try
			{
				statement.close();
			}
			catch (Throwable throwable)
			{
			}
			try
			{
				connection.close();
			}
			catch (Throwable throwable)
			{
			}
		}*/
		ok.setEnabled(true);
		setCursor(Cursor.getDefaultCursor());

		// If connection problem then display error and try again
		/*if (connectionProblem != null)
		{
			LOGGER.log(Level.WARNING, "Cannot access database with " + sql, connectionProblem);
			JOptionPane.showMessageDialog(this, connectionProblem.getMessage(), "Cannot access database with " + sql,
					JOptionPane.WARNING_MESSAGE);
			return;
		}*/
		// If no connection problem then disable authentication and close dialog
		username.setEnabled(false);
		password.setEnabled(false);
		setVisible(false);
	}

	/**
	 * Deselect any result and close the dialog
	 */
	private void cancel()
	{
		cancelPressed = true;
		if (results.getSelectedIndex() >= 0)
			results.getSelectionModel().clearSelection();
		setVisible(false);
	}

	private Set<Trial_Category> getTrialCategories( Connection connection, Long trialId ) throws SQLException
	{
		PreparedStatement statement = connection.prepareStatement( SQL_FIND_CATEGORIES );
		statement.setLong(1, trialId);
		
		ResultSet resultSet = statement.executeQuery();
		Set<Trial_Category> categorySet = new HashSet<Trial_Category>();
		
		while ( resultSet.next() )
		{
			Trial_CategoryPK trialCategoryPK = new Trial_CategoryPK();
			trialCategoryPK.setTrialid( trialId );
			trialCategoryPK.setCategory( resultSet.getShort( 1 ) );
			Trial_Category trialCategory = new Trial_Category();
			trialCategory.setTrial_CategoryPK( trialCategoryPK );
			categorySet.add( trialCategory );
		}
		return categorySet;
	}
	
	private Set<Trial_Mutation> getTrialMutations( Connection connection, Long trialId ) throws SQLException
	{
		PreparedStatement statement = connection.prepareStatement( SQL_FIND_MUTATIONS );
		statement.setLong(1, trialId);
		
		ResultSet resultSet = statement.executeQuery();
		Set<Trial_Mutation> mutationSet = new HashSet<Trial_Mutation>();
		
		while ( resultSet.next() )
		{
			Trial_MutationPK trialMutationPK = new Trial_MutationPK();
			trialMutationPK.setTrialid( trialId );
			trialMutationPK.setMutation( resultSet.getShort( 1 ) );
			Trial_Mutation trialMutation = new Trial_Mutation();
			trialMutation.setTrial_MutationPK( trialMutationPK );
			mutationSet.add( trialMutation );
		}
		return mutationSet;
	}

	/**
	 * Load and display the trials that meet the criterion.
	 * 
	 * @param actionEvent
	 *            ignored
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent actionEvent)
	{
		// Validate connection and criterion
		
		Properties properties = getConnectionProperties();
		if (properties == null)
			return;
		String criterionString = trim(criterion.getText());
		if (criterionString == null)
		{
			JOptionPane.showMessageDialog(this, "Please enter trial id", "Enter trial ID", JOptionPane.WARNING_MESSAGE);
			criterion.requestFocusInWindow();
			return;
		}

		// Run find query
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		DefaultListModel model = new DefaultListModel();
		results.setModel(model);
		String listedStr = listed.isSelected()?"true":"false";
		
		try
		{
					
			String url = trim(properties.getProperty(KEY_WS_URL));
			int port = Integer.parseInt(trim(properties.getProperty(KEY_WS_PORT)));
			
			URI uri = UriBuilder.fromUri(url+"/bct-trialcode-service-v2/services/trialcode/v53/loadTrials")
					.port(port)
					.queryParam("criterionString", criterionString)
					.queryParam("listed",listedStr)
					.build();
			
			Client client = ClientBuilder.newClient().register(MoxyJsonFeature.class);
			final Response response = client.target(uri)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.get();
			
			List<TrialResponse> trialList = response.readEntity(new GenericType<List<TrialResponse>>(){});

			for (TrialResponse trialResponse: trialList)
			{
				//JOptionPane.showMessageDialog(this, "PostDate: "+trialResponse.getPostDate(), "Last modified: "+trialResponse.getTrial().getLastModified(), JOptionPane.WARNING_MESSAGE);
				//Setting the post Date
				try {
					if(trialResponse.getPostDate() != null)
					{
					    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy 'at' HH:mm:ss");
					    Date parsedDate = dateFormat.parse(trialResponse.getPostDate());
					    Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
					    trialResponse.getTrial().setPostDate(timestamp);
					}
				} catch(Exception e) { //this generic but you can control another types of exception
				    // look the origin of exception 
				}
				trialResponse.getTrial().setEligibilityCriteriaMarshaled(trialResponse.getEligibility());
				model.addElement(new TrialDescription(trialResponse.getTrial(), trialResponse.getCurrentStatus(), trialResponse.getVersionComment()));
				
			}
		}
		catch (Exception e)
		{
			
			JOptionPane.showMessageDialog(this, "Cannot connect to the trailcode service. Please make sure that the trial code version and web service version are matching.", "Error: ", JOptionPane.WARNING_MESSAGE);
		}
		
		setCursor(Cursor.getDefaultCursor());

		
		username.setEnabled(false);
		password.setEnabled(false);

		// If results are zero then show message and return
		if (model.size() == 0)
		{
			JOptionPane.showMessageDialog(this, "No trials found that match \"" + criterionString + '"', "No results",
					JOptionPane.WARNING_MESSAGE);
			criterion.requestFocusInWindow();
			return;
		}
		// If results are one then select it and focus on ok, otherwise focus on
		// list
		ok.setEnabled(true);
		if (model.size() == 1)
		{
			results.setSelectedIndex(0);
			ok.requestFocusInWindow();
		}
		else
		{
			results.setEnabled(true);
			results.requestFocusInWindow();
		}
	}

	/**
	 * Sets each <code>VARCHAR</code> parameter in a statement to a search
	 * string.
	 * 
	 * @param preparedStatement
	 *            the statement
	 * @param criterionString
	 *            the search string or <code>null</code> to match all records
	 * @param listed
	 *            <code>true</code> to find listed trials, <code>false</code> to
	 *            find unlisted trials
	 * @throws IllegalArgumentException
	 *             if <code>preparedStatement</code> is not provided
	 * @throws SQLException
	 *             if the parameters cannot be set
	 */
	private static void setSearchParameters(PreparedStatement preparedStatement, String criterionString, boolean listed)
			throws IllegalArgumentException, SQLException
	{
		if (preparedStatement == null)
			throw new IllegalArgumentException("no statement provided");
		// Apply search criterion (enclosed with sql wildcard) to all varchar
		// parameters and boolean to all boolean parameters
		ParameterMetaData parameterMetaData = preparedStatement.getParameterMetaData();
		for (int index = 1; index <= parameterMetaData.getParameterCount(); index++)
			switch (parameterMetaData.getParameterType(index))
			{
			case Types.VARCHAR:
				preparedStatement.setString(index, criterionString == null ? SQL_WILDCARD : SQL_WILDCARD
						+ criterionString + SQL_WILDCARD);
				break;
			// JDBC3: Both BIT and BOOLEAN JDBC types equate to boolean java
			// type
			case Types.BIT:
			case Types.BOOLEAN:
				preparedStatement.setBoolean(index, listed);
				break;
			default:
				throw new java.sql.SQLException("Trial search SQL '" + preparedStatement.toString()
						+ "' has an unsupported type " + parameterMetaData.getParameterType(index) + " in parameter #"
						+ index);
			}
	}

	/**
	 * @param preparedStatement
	 *            the statement
	 * @return the column width of each <code>VARCHAR</code> column (lower-case)
	 *         in the statement's result set
	 * @throws IllegalArgumentException
	 * @throws SQLException
	 */
	private static Map<String, Integer> getStringWidths(PreparedStatement preparedStatement)
			throws IllegalArgumentException, SQLException
	{
		if (preparedStatement == null)
			throw new IllegalArgumentException("no statement provided");
		ResultSetMetaData resultSetMetaData = preparedStatement.getMetaData();
		int count = resultSetMetaData.getColumnCount();
		Map<String, Integer> maximumStringLengths = new HashMap<String, Integer>(count, 1.0f);
		for (int index = 1; index <= count; index++)
			if (resultSetMetaData.getColumnType(index) == Types.VARCHAR)
				maximumStringLengths.put(resultSetMetaData.getColumnName(index).toLowerCase(), resultSetMetaData
						.getColumnDisplaySize(index));
		return maximumStringLengths;
	}

	/**
	 * Returns connection and authentication database properties. The returned
	 * set is guaranteed to contain the following keys:
	 * <ol>
	 * <li><code>KEY_URL</code>: database server url</li>
	 * <li><code>PROPERTY_USERNAME</code>: database server login</li>
	 * <li><code>PROPERTY_PASSWORD</code>: database server password</li>
	 * </ol>
	 * 
	 * @return properties used to connect to a database or <code>null</code> if
	 *         no server properties or authentication provided
	 */
	private Properties getConnectionProperties()
	{
		// Validate server properties; create clone so we can add authentication
		int index = server.getSelectedIndex();
		Properties properties = (index >= 0) && (index < SERVERS.size()) ? (Properties) (SERVERS.get(index).clone())
				: null;
		if (properties == null)
		{
			JOptionPane.showMessageDialog(this, "No connection information for server " + (index + 1)
					+ "; cannot continue.", "Cannot connect", JOptionPane.ERROR_MESSAGE);
			return null;
		}
/*		
		//StringWriter writer = new StringWriter();
		//properties.list(new PrintWriter(writer));
		
		
		//JOptionPane.showMessageDialog(this, writer.getBuffer().toString(),
				//"PROPS", JOptionPane.ERROR_MESSAGE);  
		  
		// Validate url available
		String value = trim(properties.getProperty(KEY_URL));
		JOptionPane.showMessageDialog(this, value,
				"VALUE", JOptionPane.ERROR_MESSAGE);
		String url = trim(properties.getProperty(KEY_WS_URL));
		JOptionPane.showMessageDialog(this, url,
				"URL", JOptionPane.ERROR_MESSAGE);
		if (value == null)
		{
			JOptionPane.showMessageDialog(this, "Cannot find server " + (index + 1) + "; cannot continue.",
					"Cannot find server", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		// Validate and store authentication
		value = trim(username.getText());
		if (value.length() == 0)
		{
			JOptionPane.showMessageDialog(this, "Please enter user name", "Cannot connect to database",
					JOptionPane.WARNING_MESSAGE);
			username.requestFocusInWindow();
			return null;
		}
		properties.setProperty(PROPERTY_USERNAME, value);
		value = trim(new String(password.getPassword()));
		if (value == null)
		{
			JOptionPane.showMessageDialog(this, "Please enter password", "Cannot connect to database",
					JOptionPane.WARNING_MESSAGE);
			password.requestFocusInWindow();
			return null;
		}
		properties.setProperty(PROPERTY_PASSWORD, value);*/
		return properties;
	}

	/**
	 * Returns a trimmed string
	 * 
	 * @param string
	 *            the string to trim
	 * @return the trimmed string or <code>null</code> if the trimmed string is
	 *         emtpy
	 */
	private static String trim(String string)
	{
		if (string == null)
			return null;
		string = string.trim();
		return (string.length() == 0) ? null : string;
	}

	/**
	 * Returns a connection to the database server. This method uses the
	 * <code>properties</code> value for the key <code>KEY_URL</code> for url.
	 * 
	 * @param properties
	 *            the connection properties
	 * @return a connection to the database server
	 * @throws SQLException
	 *             if <code>properties</code> is <code>null</code> or a
	 *             connection could not be established
	 */
	/*private Connection getConnection(Properties properties) throws SQLException
	{
		String url = (properties == null) ? null : properties.getProperty(KEY_URL);
		if (url == null)
			throw new SQLException("Could not determine server to connect to");
		return DriverManager.getConnection(url, properties);
	}*/
	
	private String getWebServiceURL(Properties properties) throws SQLException
	{
		String url = (properties == null) ? null : properties.getProperty(KEY_URL);
		if (url == null)
			throw new SQLException("Could not determine server to connect to");
		return url;
	}

	/**
	 * Retrieve patient principal and histories.
	 * 
	 * @param principal
	 *            the optional principal to match (using LIKE)
	 * @param credentials
	 *            the optional credentials to match (exact)
	 * @return the list of matching patients (principal and history fields
	 *         populated only) or <code>null</code> if none match
	 * @throws SQLException
	 *             if patients could not be loaded from the database
	 */
	List<UserPatient> getPatients(String principal, String credentials) throws SQLException
	{
		if(principal == null)
			principal="";
		if(credentials == null)
			credentials="";
		Properties properties = getConnectionProperties();
		
		String url = trim(properties.getProperty(KEY_WS_URL));
		int port = Integer.parseInt(trim(properties.getProperty(KEY_WS_PORT)));
		String sqlcmd = trim(properties.getProperty(KEY_PATIENTS));
		
		if(sqlcmd == null)
			return null;
		
		URI uri = UriBuilder.fromUri(url+"/bct-trialcode-service-v2/services/trialcode/v53/getPatients")
				.port(port)
				.queryParam("principal", principal)
				.queryParam("credentials",credentials)
				.queryParam("sql",sqlcmd)
				.build();
		
		Client client = ClientBuilder.newClient().register(MoxyJsonFeature.class);
		final Response response = client.target(uri)
				.request(MediaType.APPLICATION_JSON_TYPE)
				.get();
		
		List<UserPatient> userPatientList = response.readEntity(new GenericType<List<UserPatient>>(){});
		
		LinkedList<UserPatient> patients = new LinkedList<UserPatient>();
		patients.addAll(userPatientList);
		
		return patients;
	}
}

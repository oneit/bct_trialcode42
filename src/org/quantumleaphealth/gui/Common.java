/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;

/**
 * Contains constants and methods to look up localized text.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public final class Common
{

	/**
	 * Separates the components of keys used to lookup localized strings
	 */
	static final char SEPARATOR = '.';

	/**
	 * Prepended to key to find localized label for action
	 */
	static final String PREFIX_ACTION = "action.";

	/**
	 * Prepended to key to find localized label for component
	 */
	static final String PREFIX_LABEL = "label.";

	/**
	 * Prefix for key to look up localized operator conjunction
	 */
	static final String PREFIX_OPERATOR = "operator.";

	/**
	 * The suffix for the key to look up the label next to the first matchable
	 * in an IF/THEN criteria
	 */
	static final char SUFFIX_IF = '0';

	/**
	 * The suffix for the key to look up the label next to the second matchable
	 * in an IF/THEN criteria
	 */
	static final char SUFFIX_THEN = '1';

	/**
	 * Prefix for keys when lookup up localized conjuctions between requirement
	 * choices
	 */
	static final String PREFIX_CONJUNCTION = "conjunction.";

	/**
	 * Prefix for keys when lookup up localized comparator
	 */
	static final String PREFIX_COMPARE = "compare.";

	/**
	 * Appended to attribute name to find localized comparator label
	 */
	static final String SUFFIX_MINIMUM = ".MINIMUM";

	/**
	 * Appended to attribute name to find localized comparator label
	 */
	static final String SUFFIX_MAXIMUM = ".MAXIMUM";

	/**
	 * The suffix for the key to look up the label for the "any" choice in
	 * multiple attributes
	 */
	static final char SUFFIX_ANY = '*';

	/**
	 * The suffix for the key to look up the label for the "none" choice in
	 * multiple attributes
	 */
	static final String SUFFIX_NONE = "none";

	/**
	 * The suffix for the key to look up the label for the unnamed "other"
	 * choice in attributes
	 */
	static final char SUFFIX_OTHER = '0';

	/**
	 * The suffix for the key to look up the label for bilateral location
	 * element
	 */
	static final String SUFFIX_LOCATIONBILATERAL = ".locationbilateral";

	/**
	 * The suffix for the key to look up the label for maximum elapsed time
	 * element
	 */
	static final String SUFFIX_MAXIMUMELAPSED = ".maximumelapsed";

	/**
	 * The suffix for the key to look up the label for intervention element
	 */
	static final String SUFFIX_INTERVENTION = ".intervention";

	/**
	 * Prefix for keys when lookup up localized regimen label
	 */
	static final String PREFIX_REGIMEN = "regimen";

	/**
	 * Prefix for keys when lookup up localized setting
	 */
	static final String PREFIX_SETTING = "setting";

	/**
	 * Key to lookup up recent malignancy label
	 */
	static final String KEY_RECENT = "recent";

	/**
	 * Key to lookup up breast cancer malignancy label
	 */
	static final String KEY_BREASTCANCER = "breastcancer";

	/**
	 * Key to look up localized text for progression
	 */
	static final String KEY_PROGRESSION = "progression";

	/**
	 * Background for buttons used by the eligibility criteria user interface
	 */
	static final Color BUTTON_BACKGROUND = new Color(159, 189, 221);

	/**
	 * Background color of unselected panel
	 */
	static final Color BACKGROUND = SystemColor.control;

	/**
	 * Background color of selected panel
	 */
	static final Color BACKGROUND_SELECTED = SystemColor.textHighlight;

	/**
	 * Foreground color of unselected panel
	 */
	static final Color FOREGROUND = SystemColor.controlText;

	/**
	 * Foreground color of selected panel
	 */
	static final Color FOREGROUND_SELECTED = SystemColor.textHighlightText;

	/**
	 * Border of unrollover panel
	 */
	static final Border NONROLLOVER = BorderFactory.createLineBorder(BACKGROUND);

	/**
	 * Border of rollover panel
	 */
	static final Border ROLLOVER = BorderFactory.createLineBorder(SystemColor.textInactiveText);

	/**
	 * Package's resource bundle
	 */
	static final ResourceBundle PACKAGEBUNDLE = ResourceBundle.getBundle("EligibilityCriteria");

	/**
	 * Looks up localized text in a resource bundle, first as all capital
	 * letters, then as is.
	 * 
	 * @param key
	 *            the key to lookup the text with
	 * @param bundle
	 *            the bundle
	 * @return the localized text or <code>key</code> if not found
	 * @throws IllegalArgumentException
	 *             if <code>key</code> is <code>null</code>
	 */
	static String getCapitalized(String key, ResourceBundle bundle) throws IllegalArgumentException
	{
		if (key == null)
			throw new IllegalArgumentException("No key specified");
		if (bundle == null)
			return '?' + key + '?';
		try
		{
			return bundle.getString(key.toUpperCase());
		}
		catch (MissingResourceException missingResourceException)
		{
			try
			{
				return bundle.getString(key);
			}
			catch (MissingResourceException missingResourceException2)
			{
				return '?' + key + '?';
			}
		}
	}

	/**
	 * Looks up localized text in a resource bundle.
	 * 
	 * @param key
	 *            the key to lookup the text with
	 * @param bundle
	 *            the bundle
	 * @return the localized text or <code>key</code> if not found
	 * @throws IllegalArgumentException
	 *             if <code>key</code> is <code>null</code>
	 */
	static String getString(String key, ResourceBundle bundle) throws IllegalArgumentException
	{
		if (key == null)
			throw new IllegalArgumentException("No key specified");
		if (bundle == null)
			return '?' + key + '?';
		try
		{
			return bundle.getString(key);
		}
		catch (MissingResourceException missingResourceException2)
		{
			return '?' + key + '?';
		}
	}

	/**
	 * Looks up localized text in the package's resource bundle.
	 * 
	 * @param key
	 *            the key to lookup the text with
	 * @param bundle
	 *            the bundle
	 * @return the localized text or <code>key</code> if not found
	 * @throws IllegalArgumentException
	 *             if <code>key</code> is <code>null</code>
	 */
	static String getString(String key) throws IllegalArgumentException
	{
		return getString(key, PACKAGEBUNDLE);
	}

	/**
	 * Returns a trimmed string
	 * 
	 * @param string
	 *            the string to trim
	 * @return a trimmed string or <code>null</code> if the trimmed string has
	 *         no length
	 */
	static String trimString(String string)
	{
		if (string != null)
			string = string.trim();
		return (string == null) || (string.length() == 0) ? null : string;
	}

	/**
	 * Returns the string with first character capitalized
	 * 
	 * @param string
	 *            the string to capitalize the first character
	 * @return the string with first character capitalized
	 */
	static String convertFirstCharToUpper(String string)
	{
		if ((string == null) || (string.length() < 2))
			return string;
		return string.substring(0, 1).toUpperCase() + string.substring(1);
	}

	/**
	 * Return a new button that uses <code>key</code> for the name, action
	 * command and localized label. The button's label is found by prepending
	 * <code>PREFIX_BUTTON</code>.
	 * 
	 * @param key
	 *            key suffix to lookup the localized label
	 * @param bundle
	 *            contains the localized label
	 * @param listener
	 *            optional listener to the button
	 * @return a new button
	 */
	static JButton createButton(String key, ActionListener listener)
	{
		JButton button = new JButton(Common.getString(PREFIX_ACTION + key));
		button.setBackground(BUTTON_BACKGROUND);
		button.setName(key);
		button.setActionCommand(key);
		if (listener != null)
			button.addActionListener(listener);
		return button;
	}

	/**
	 * Returns a text area that behaves as a word-wrapping label
	 * 
	 * @return a text area that behaves as a word-wrapping label
	 * @param text
	 *            the text to set in the text area
	 */
	static JTextArea createWordWrapTextArea(String text)
	{
		JTextArea textArea = new JTextArea(text);
		textArea.setOpaque(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		textArea.setBorder(null);
		return textArea;
	}

	/**
	 * Returns an estimate of the height of a button. This method queries
	 * <code>UIManager</code> defaults to add up the following metrics:
	 * <ol>
	 * <li>Size of "Button.font" (default 10)
	 * <li>Top and bottom insets of "Button.margin" (default 4)
	 * <li>Top and bottom insets of border of "Button.border" (default 9)
	 * </ol>
	 * 
	 * @return
	 */
	static int getButtonHeight()
	{
		Font font = UIManager.getFont("Button.font");
		int unitIncrement = (font != null) ? font.getSize() : 10;
		Insets insets = UIManager.getInsets("Button.margin");
		unitIncrement += (insets != null) ? (insets.bottom + insets.top) : 4;
		Border border = UIManager.getBorder("Button.border");
		insets = (border != null) ? border.getBorderInsets(new JButton()) : null;
		unitIncrement += (insets != null) ? (insets.bottom + insets.top) : 9;
		return unitIncrement;
	}
}

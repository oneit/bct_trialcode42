/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.model.trial.ComparativeCriterion;
import org.quantumleaphealth.model.trial.DoNotMatchCriterion;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.model.trial.TreatmentRegimenCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Builds the user interface for Breast Cancer criteria.
 * 
 * @author Tom Bechtold
 * @version 2008-10-02
 */
public class BreastCancerUserInterface implements CriteriaUserInterface, Serializable
{
	/**
	 * The list of available libraries
	 */
	private static final List<Library> libraries = new LinkedList<Library>();
	/**
	 * The cached list of library names
	 */
	private static final List<String> libraryNames = new LinkedList<String>();

	/**
	 * Populate the static class variables
	 */
	private static void initialize()
	{
		// Special No Match Section
		List<AbstractCriterionUserInterface> list = new LinkedList<AbstractCriterionUserInterface>();
		list.add(new DoNotMatchCriterionUserInterface());
		libraries.add(new Library("Do Not Match", list));
		
		// General Patient Characteristics
		list = new LinkedList<AbstractCriterionUserInterface>();
		list.add(new ComparativeCriterionPanel(AGE, new CharacteristicCode[] { YEAR }));
		list.add(new AssociativeCriterionUserInterface(GENDER, new CharacteristicCode[] { FEMALE, MALE }, null, false,
				false));
		list.add(new AssociativeCriterionUserInterface(MENOPAUSAL, new CharacteristicCode[] { PREMENOPAUSAL,
				POSTMENOPAUSAL }, null, false, false));
		list.add(new AssociativeCriterionUserInterface(MENOPAUSE, new CharacteristicCode[] { MENOPAUSE_NATURAL,
				MENOPAUSE_OOPHRECTOMY, MENOPAUSE_RADIATION, MENOPAUSE_HORMONE, MENOPAUSE_CHEMO }, null, false, false));
		list.add(new AssociativeCriterionUserInterface(HORMONE_REPLACEMENT_THERAPY, new CharacteristicCode[] {
				COMPLETED, CONCURRENT }, HORMONE_REPLACEMENT_THERAPY_AGGREGATORS, false, false));
		list.add(new BooleanCriterionUserInterface(PARTICIPATION_CURRENT));
		list.add(new AssociativeCriterionUserInterface(HISPANIC, new CharacteristicCode[] {AFFIRMATIVE, NEGATIVE, UNSURE}, null, false, false));
		list.add(new BooleanCriterionUserInterface(ASHKENAZI_JEWISH));
		list.add(new AssociativeCriterionUserInterface(RACE, new CharacteristicCode[] {AMERICAN_INDIAN_ALASKAN_NATIVE, ASIAN, BLACK, PACIFIC_ISLANDER, CAUCASIAN}, null, false, false));

		libraries.add(new Library("General Patient Characteristics", list));

		// Other Health Conditions
		list = new LinkedList<AbstractCriterionUserInterface>();
		list.add(new AssociativeCriterionUserInterface(ECOG_ZUBROD, new CharacteristicCode[] { ECOG_ZUBROD_0,
				ECOG_ZUBROD_1, ECOG_ZUBROD_2, ECOG_ZUBROD_3, ECOG_ZUBROD_4 }, null, false, false));
		list.add(new AssociativeCriterionUserInterface(PRIMARYCANCER, new CharacteristicCode[] { PRIMARYCANCER_BONE,
				PRIMARYCANCER_CNS, PRIMARYCANCER_CERVICAL_INVASIVE, PRIMARYCANCER_CERVICAL_INSITU,
				PRIMARYCANCER_COLON_RECTAL, PRIMARYCANCER_HODGKINS, PRIMARYCANCER_INTESTINAL, PRIMARYCANCER_KIDNEY,
				PRIMARYCANCER_LEUKEMIA, PRIMARYCANCER_LUNG, PRIMARYCANCER_LYMPHOMA, PRIMARYCANCER_OVARIAN,
				PRIMARYCANCER_PANCREATIC, PRIMARYCANCER_PROSTATE, PRIMARYCANCER_BASAL_SQUAMOUS, PRIMARYCANCER_MELANOMA,
				PRIMARYCANCER_THYROID, PRIMARYCANCER_UTERINE }, null, true, true));
		list.add(new BooleanCriterionUserInterface(HIV));
		list.add(new AssociativeCriterionUserInterface(HEMATOPOIETIC, new CharacteristicCode[] { HEMATOPOIETIC_ANEMIA,
				HEMATOPOIETIC_BLEEDING_DIATHESIS }, null, true, true));
		list.add(new AssociativeCriterionUserInterface(AUTOIMMUNE, new CharacteristicCode[] { SCLERODERMA,
				SYSTEMIC_LUPUS_ERYTHEMATOSUS }, null, true, true));
		list.add(new AssociativeCriterionUserInterface(PULMONARY, new CharacteristicCode[] { PULMONARY_EMBOLISM, COPD,
				ASTHMA }, null, true, true));
		list.add(new AssociativeCriterionUserInterface(DIGESTIVE, HEPATIC_SET, DIGESTIVE_AGGREGATORS, true, true));
		list.add(new BooleanCriterionUserInterface(DIABETES));
		list.add(new AssociativeCriterionUserInterface(CARDIOVASCULAR, new CharacteristicCode[] { ANGINA, ARRHYTHMIA,
				CONGESTIVE_HEART_FAILURE, DEEP_VEIN_THROMBOSIS, HEART_ATTACK, HYPERTENSION }, null, true, true));
		list.add(new AssociativeCriterionUserInterface(RENAL, new CharacteristicCode[] { RENAL_FAILURE,
				RENAL_INSUFFICIENCY }, null, true, true));
		list.add(new AssociativeCriterionUserInterface(NEUROPSYCHIATRIC, new CharacteristicCode[] {
				PERIPHERAL_NEUROPATHY, STROKE }, null, true, true));
		list.add(new BooleanCriterionUserInterface(OSTEOPOROSIS));
		list.add(new AssociativeCriterionUserInterface(HORMONAL, new CharacteristicCode[] { HYPERTHYROIDISM,
				HYPOTHYROIDISM }, null, true, true));
		list.add(new AssociativeCriterionUserInterface(GYNECOLOGIC, new CharacteristicCode[] { ENDOMETRIAL_HYPERPLASIA,
				ENDOMETRIOSIS, ABNORMAL_VAGINAL_BLEEDING }, null, true, true));
		list.add(new BooleanCriterionUserInterface(PREGNANCY));
		list.add(new BooleanCriterionUserInterface(NURSING));
		libraries.add(new Library("Other Health Conditions", list));

		// Diagnosis
		list = new LinkedList<AbstractCriterionUserInterface>();
		list.add(new AssociativeCriterionUserInterface(PRIOR_BREAST_DISEASE, new CharacteristicCode[] { DUCTAL_INSITU,
				DUCTAL_INVASIVE, LOBULAR_INSITU, LOBULAR_INVASIVE, ATYPICAL_DUCTAL_HYPERPLASIA }, null, false, true));
		list.add(new AssociativeCriterionUserInterface(DIAGNOSISBREAST, new CharacteristicCode[] { DUCTAL_INSITU,
				DUCTAL_INVASIVE, LOBULAR_INVASIVE }, null, false, true));
		list.add(new BooleanCriterionUserInterface(BILATERAL_SYNCHRONOUS));
		list.add(new BooleanCriterionUserInterface(BILATERAL_ASYNCHRONOUS));
		list.add(new BooleanCriterionUserInterface(SURVIVOR));
		list.add(new AssociativeCriterionUserInterface(STAGE, new CharacteristicCode[] { STAGE_0, STAGE_I, STAGE_IIA,
				STAGE_IIB, STAGE_IIC, STAGE_IIIA, STAGE_IIIB, STAGE_IIIC, STAGE_IV }, STAGE_AGGREGATORS, false, false));
		list.add(new BooleanCriterionUserInterface(INFLAMMATORY));
		list.add(new BooleanCriterionUserInterface(RECURRENT_LOCALLY));
		list.add(new AssociativeCriterionUserInterface(TUMOR_STAGE_FINDING, new CharacteristicCode[] {
				TUMOR_STAGE_FINDING_T1, TUMOR_STAGE_FINDING_T2, TUMOR_STAGE_FINDING_T3 }, null, false, false));
		list.add(new ComparativeCriterionPanel(LYMPH_NODE_COUNT, null));
		list.add(new AssociativeCriterionUserInterface(ESTROGENRECEPTOR, new CharacteristicCode[] {
				ESTROGENRECEPTOR_POSITIVE, ESTROGENRECEPTOR_NEGATIVE, ESTROGENRECEPTOR_LOW }, ESTROGENRECEPTOR_AGGREGATORS, false, false));
		list.add(new AssociativeCriterionUserInterface(PROGESTERONERECEPTOR, new CharacteristicCode[] {
				PROGESTERONERECEPTOR_POSITIVE, PROGESTERONERECEPTOR_NEGATIVE, PROGESTERONERECEPTOR_LOW }, PROGESTERONERECEPTOR_AGGREGATORS,
				false, false));
		list.add(new AssociativeCriterionUserInterface(HER2NEUDIAGNOSIS, new CharacteristicCode[] {
				HER2NEUDIAGNOSIS_POSITIVE, HER2NEUDIAGNOSIS_NEGATIVE, HER2NEUDIAGNOSIS_LOW }, HER2NEUDIAGNOSIS_AGGREGATORS, false, false));
		list.add(new AssociativeCriterionUserInterface(LYMPHEDEMA, new CharacteristicCode[] { NEGATIVE, AFFIRMATIVE,
				UNSURE }, null, false, false));
		list.add(new BooleanCriterionUserInterface(MEASURABLE));
		list.add(new AssociativeCriterionUserInterface(DIAGNOSISAREA, new CharacteristicCode[] { DIAGNOSISAREA_BREAST, SUPERCLAVICULAR,
				INFRACLAVICULAR, CHESTWALL, DIAGNOSISAREA_BRAIN, DIAGNOSISAREA_SPINALCORD, DIAGNOSISAREA_BONE,
				DIAGNOSISAREA_LIVER, DIAGNOSISAREA_LUNG, DIAGNOSISAREA_LYMPHNODE, DIAGNOSISAREA_OVARIES, DIAGNOSISAREA_SKIN, 
				DIAGNOSISAREA_OTHER_LOCATION, DIAGNOSISAREA_LEPTOMENINGES, DIAGNOSISAREA_EYE, DIAGNOSISAREA_ABDOMINAL_LININGS, DIAGNOSISAREA_BLADDER, 
				DIAGNOSISAREA_GI_TRACT }, null, true, true));
		list.add(new AssociativeCriterionUserInterface(BREASTMETASTASIS_BRAIN, new CharacteristicCode[] { STABLE,
				PROGRESSING }, BREASTMETASTASIS_BRAIN_AGGREGATORS, false, false));
		list.add(new AssociativeCriterionUserInterface(BRCA1TEST, new CharacteristicCode[] { BRCA1TEST_POSITIVE,
				BRCA1TEST_NEGATIVE }, null, false, false));
		list.add(new AssociativeCriterionUserInterface(BRCA2TEST, new CharacteristicCode[] { BRCA2TEST_POSITIVE,
				BRCA2TEST_NEGATIVE }, null, false, false));
		list.add(new AssociativeCriterionUserInterface(PDL1, new CharacteristicCode[] { PDL1_POSITIVE,
				PDL1_NEGATIVE }, null, false, false));
		list.add(new AssociativeCriterionUserInterface(ANDROGENRECEPTOR, new CharacteristicCode[] { ANDROGENRECEPTOR_POSITIVE,
				ANDROGENRECEPTOR_NEGATIVE }, null, false, false));
		list.add(new AssociativeCriterionUserInterface(PI3K, new CharacteristicCode[] { PI3K_POSITIVE,
				PI3K_NEGATIVE }, null, false, false));
		list.add(new ComparativeCriterionPanel(GAIL_SCORE, null));
		list.add(new ComparativeCriterionPanel(CLAUS_MODEL, null));
		libraries.add(new Library("Breast Diagnosis/Current Status", list));

		// Treatment
		list = new LinkedList<AbstractCriterionUserInterface>();
		list.add(new SurgeryCriterionPanel());
		list.add(new RadiationCriterionPanel());
		// Build agent list for chemo
		LinkedList<CharacteristicCode> agents = new LinkedList<CharacteristicCode>();
		Collections.addAll(agents, CLASS_ALKYLATING_SET);
		Collections.addAll(agents, CLASS_ANTHRACYCLINE_SET);
		Collections.addAll(agents, CLASS_PLATINUM_SET);
		Collections.addAll(agents, CLASS_TAXANE_SET);
		Collections.addAll(agents, CLASS_VINCA_SET);
		Collections.addAll(agents, FIVE_FLUOROURACIL, GEMCITABINE, IRINOTECAN, METHOTREXATE, TOPOTECAN, CAPECITABINE,
				IXABEPILONE, HALAVEN, DAUNORUBICIN, MITOMYCIN);  // BCT-526 Add HALAVEN WGW
		list.add(new TreatmentRegimenCriterionPanel(CHEMOTHERAPY, new CharacteristicCode[] { NEOADJUVANT, ADJUVANT,
				METASTATIC }, agents.toArray(new CharacteristicCode[agents.size()]), CHEMOTHERAPY_AGGREGATORS, false));
		// Targeted/biological agents
		//Added new heirarchy for Biological
		agents = new LinkedList<CharacteristicCode>();
		Collections.addAll(agents, CLASS_ANTIHER2_SET);
		Collections.addAll(agents, CLASS_IMMUNOTHERAPY_SET);
		Collections.addAll(agents, CLASS_CDK_FOUR_FIVE_SET);
		Collections.addAll(agents, CLASS_PARP_SET);
		Collections.addAll(agents, CLASS_OTHER_SET);
		list.add(new TreatmentRegimenCriterionPanel(BIOLOGICAL, new CharacteristicCode[] { NEOADJUVANT, ADJUVANT,
				METASTATIC }, agents.toArray(new CharacteristicCode[agents.size()]), BIOLOGICAL_AGGREGATORS, false));
		// Build agent list for hormonal/endocrine
		agents = new LinkedList<CharacteristicCode>();
		Collections.addAll(agents, CLASS_ANTIESTROGEN_SET);
		Collections.addAll(agents, CLASS_AROMATASE_INHIBITOR_SET);
		Collections.addAll(agents, CLASS_OVARIAN_SUPPRESSION_SET);
		Collections.addAll(agents, MEGESTROL_ACETATE);
		list.add(new TreatmentRegimenCriterionPanel(ENDOCRINE, new CharacteristicCode[] { PREVENTION, NEOADJUVANT,
				ADJUVANT, METASTATIC }, agents.toArray(new CharacteristicCode[agents.size()]), ENDOCRINE_AGGREGATORS,
				true));
		list.add(new TreatmentRegimenCriterionPanel(BISPHOSPHONATE, new CharacteristicCode[] { OSTEOPOROSIS,
				ADVERSE_DRUG_EFFECT, BONECANCER_SECONDARY }, new CharacteristicCode[] { RISEDRONATE, PAMIDRONATE,
				IBANDRONATE, ALENDRONATE, ZOLEDRONATE, DENOSUMAB_XGEVA }, null, true)); // BCT-527 Add PROLIA WGW
		libraries.add(new Library("Treatment", list));

		// Laboratory
		list = new LinkedList<AbstractCriterionUserInterface>();
		list.add(new ComparativeCriterionPanel(CREATININE, new CharacteristicCode[] { MILLIGRAMS_PER_DECILITER }));
		list.add(new ComparativeCriterionPanel(CREATININE_CLEARANCE,
				new CharacteristicCode[] { MILLILITERS_PER_MINUTE }));
		list.add(new ComparativeCriterionPanel(BILIRUBIN, new CharacteristicCode[] { MILLIGRAMS_PER_DECILITER }));
		list.add(new ComparativeCriterionPanel(NEUTROPHIL, new CharacteristicCode[] { CELLS_PER_MICROLITER }));
		list.add(new ComparativeCriterionPanel(PLATELET_COUNT, new CharacteristicCode[] { CELLS_PER_LITER }));
		libraries.add(new Library("Laboratory", list));

		// Cache the library names into class variable
		for (Library library : libraries)
			libraryNames.add(library.name);
	}

	/**
	 * Returns a localized description of an abstract criterion. This method
	 * supports the subclasses of abstract criterion:
	 * <ul>
	 * <li><code>TreatmentProcedureCriterion</code> whose parent is
	 * <ul>
	 * <li><code>SURGERY</code></li>
	 * <li><code>RADIATION</code></li>
	 * </ul>
	 * </li>
	 * <li><code>TreatmentRegimenCriterion</code></li>
	 * <li><code>AssociativeCriterion</code></li>
	 * <li><code>ComparativeCriterion</code></li>
	 * </ul>
	 * 
	 * @param abstractCriterion
	 *            the criterion to describe
	 * @return a localized description of the criterion
	 * @throws IllegalArgumentException
	 *             if this method does not support the criterion
	 * @see org.quantumleaphealth.gui.CriteriaUserInterface#getDescription(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	public String getDescription(AbstractCriterion abstractCriterion) throws IllegalArgumentException
	{
		if (abstractCriterion == null)
			throw new IllegalArgumentException("criterion not specified");
		if (abstractCriterion instanceof TreatmentProcedureCriterion)
		{
			CharacteristicCode parent = ((TreatmentProcedureCriterion) (abstractCriterion)).getParent();
			if (RADIATION.equals(parent))
				return getRadiationDescription((TreatmentProcedureCriterion) (abstractCriterion));
			if (SURGERY.equals(parent))
				return getSurgeryDescription((TreatmentProcedureCriterion) (abstractCriterion));
		}
		if (abstractCriterion instanceof TreatmentRegimenCriterion)
			return getTreatmentDescription((TreatmentRegimenCriterion) (abstractCriterion));
		if (abstractCriterion instanceof AssociativeCriterion)
			return getAssociativeDescription((AssociativeCriterion) (abstractCriterion));
		if (abstractCriterion instanceof ComparativeCriterion)
			return getComparativeDescription((ComparativeCriterion) (abstractCriterion));
		if (abstractCriterion instanceof BooleanCriterion)
			return getBinaryDescription((BooleanCriterion) (abstractCriterion));

		if (abstractCriterion instanceof DoNotMatchCriterion)
		{	
			return getDoNotMatchDescription((DoNotMatchCriterion) (abstractCriterion));
		}	
		throw new IllegalArgumentException("cannot get description for " + abstractCriterion);
	}

	/**
	 * Returns the description of a binary criterion
	 * 
	 * @param booleanCriterion
	 *            the criterion to describe
	 * @return the description of a binary association criterion
	 * @throws IllegalArgumentException
	 *             if the criterion's characteristic is <code>null</code>
	 */
	private static String getDoNotMatchDescription(DoNotMatchCriterion doNotMatchCriterion) throws IllegalArgumentException
	{
		if (doNotMatchCriterion.getCharacteristicCode() == null)
			throw new IllegalArgumentException("criterion characteristic not set");

		StringBuilder builder = new StringBuilder(Common.getString(doNotMatchCriterion.getCharacteristicCode().toString()));
		if (doNotMatchCriterion.isInvert())
			builder.insert(0, "No ");
		else
			capitalizeFirstChar(builder);
		return builder.toString();
	}

	/**
	 * Returns the description of a binary criterion
	 * 
	 * @param booleanCriterion
	 *            the criterion to describe
	 * @return the description of a binary association criterion
	 * @throws IllegalArgumentException
	 *             if the criterion's characteristic is <code>null</code>
	 */
	private static String getBinaryDescription(BooleanCriterion booleanCriterion) throws IllegalArgumentException
	{
		if (booleanCriterion.getCharacteristicCode() == null)
			throw new IllegalArgumentException("criterion characteristic not set");

		StringBuilder builder = new StringBuilder(Common.getString(booleanCriterion.getCharacteristicCode().toString()));
		if (booleanCriterion.isInvert())
			builder.insert(0, "No ");
		else
			capitalizeFirstChar(builder);
		return builder.toString();
	}

	/**
	 * Returns the description of a set association criterion
	 * 
	 * @param associativeCriterion
	 *            the criterion to describe
	 * @return the description of a set association criterion
	 * @throws IllegalArgumentException
	 *             if the criterion's parent is <code>null</code>
	 */
	private static String getAssociativeDescription(AssociativeCriterion associativeCriterion)
			throws IllegalArgumentException
	{
		if (associativeCriterion.getParent() == null)
			throw new IllegalArgumentException("criterion parent not set");

		// If "any" requirement then display static any string
		StringBuilder builder = null;
		CharacteristicCode[] requirement = associativeCriterion.getRequirement();
		if (requirement == null)
		{
			builder = new StringBuilder(associativeCriterion.isInvert() ? "No " : "Any ");
			builder.append(Common.getString(associativeCriterion.getParent().toString()));
		}
		else
		{
			builder = getSetDescription(associativeCriterion);
			if (associativeCriterion.isInvert())
				builder.insert(0, "No ");
		}
		// Capitalize first character
		capitalizeFirstChar(builder);
		return builder.toString();
	}

	/**
	 * Returns the description of a comparative criterion. This method uses the
	 * characteristic to look up the criterion's name.
	 * 
	 * @param comparativeCriterion
	 *            the criterion to describe
	 * @return the description of a comparative criterion
	 * @throws IllegalArgumentException
	 *             if the criterion's characteristic is <code>null</code>
	 */
	private static String getComparativeDescription(ComparativeCriterion comparativeCriterion)
			throws IllegalArgumentException
	{
		if (comparativeCriterion.getCharacteristicCode() == null)
			throw new IllegalArgumentException("criterion characteristic not set");
		String key = comparativeCriterion.getCharacteristicCode().toString();
		StringBuilder builder = new StringBuilder(Common.getString(key));
		// Inverted means maximum
		builder.append(comparativeCriterion.isInvert() ? " is at most " : " is at least ");
		// Remove decimal part for integers
		String doubleString = Double.toString(comparativeCriterion.getRequirement());
		if (doubleString.endsWith(".0"))
			doubleString = doubleString.substring(0, doubleString.length() - 2);
		builder.append(doubleString);
		// Add units if available
		if (comparativeCriterion.getUnitOfMeasure() != null)
		{
			key += '.' + comparativeCriterion.getUnitOfMeasure().toString();
			String units = Common.getString(key);
			if (!key.equals(units))
				builder.append(' ').append(units);
		}
		// Capitalize first character
		capitalizeFirstChar(builder);
		return builder.toString();
	}

	/**
	 * Returns the description of a treatment procedure criterion
	 * 
	 * @param treatmentProcedureCriterion
	 *            the criterion to describe
	 * @return the description of a treatment procedure criterion
	 * @throws IllegalArgumentException
	 *             if the criterion's parent is not <code>SURGERY</code>
	 */
	private static String getSurgeryDescription(TreatmentProcedureCriterion treatmentProcedureCriterion)
			throws IllegalArgumentException
	{
		if (!SURGERY.equals(treatmentProcedureCriterion.getParent()))
			throw new IllegalArgumentException("parent must be surgery");
		// any surgery|surgery1,surgery2,...
		StringBuilder builder = new StringBuilder(treatmentProcedureCriterion.isInvert() ? "No " : "Any ");
		if (treatmentProcedureCriterion.getRequirement() == null)
			builder.append("surgical procedure");
		else
			builder.append(getSetDescription(treatmentProcedureCriterion));

		// [in bilateral location]
		List<CharacteristicCode> qualifiers = getList(treatmentProcedureCriterion.getQualifiers());
		if (qualifiers.remove(BILATERAL_SYNCHRONOUS))
			builder.append(" in bilateral location");
		// for recent malignancy|for breast cancer
		builder.append(qualifiers.remove(MOSTRECENT) ? " for recent malignancy" : " for breast cancer");

		// [of at least threshold thresholdUnitOfMeasure]
		if (treatmentProcedureCriterion.getMaximumElapsedTimeUnits() != null)
			builder.append(" with maximum time between surgery and enrollment of ").append(
					treatmentProcedureCriterion.getMaximumElapsedTime()).append(' ').append(
					Common.getString(SURGERY.toString() + '.'
							+ treatmentProcedureCriterion.getMaximumElapsedTimeUnits().toString()));

		return builder.toString();
	}

	/**
	 * Returns the description of a treatment procedure criterion
	 * 
	 * @param treatmentProcedureCriterion
	 *            the criterion to describe
	 * @return the description of a treatment procedure criterion
	 * @throws IllegalArgumentException
	 *             if the criterion's parent is <code>null</code>
	 */
	private static String getRadiationDescription(TreatmentProcedureCriterion treatmentProcedureCriterion)
			throws IllegalArgumentException
	{
		if (!RADIATION.equals(treatmentProcedureCriterion.getParent()))
			throw new IllegalArgumentException("parent must be radiation");
		// Any|No
		StringBuilder builder = new StringBuilder(treatmentProcedureCriterion.isInvert() ? "No " : "Any ");
		// [prior|concurrent]
		List<CharacteristicCode> qualifiers = getList(treatmentProcedureCriterion.getQualifiers());
		if (qualifiers.remove(COMPLETED))
			builder.append("prior ");
		else if (qualifiers.remove(CONCURRENT))
			builder.append("concurrent ");

		// technique1,technique2,... radiotherapy
		if (treatmentProcedureCriterion.getInterventions() != null)
			builder
					.append(
							getSetDescription(RADIATION, treatmentProcedureCriterion.getInterventions(),
									SetOperation.INTERSECT)).append(' ');
		builder.append("radiotherapy");

		// [for recent malignancy|for breast cancer]
		if (qualifiers.remove(MOSTRECENT))
			builder.append(" for recent malignancy");
		else if (qualifiers.remove(BREAST_CANCER))
			builder.append(" for breast cancer");

		// involving any site|site1,site2,...
		builder.append(" involving ");
		if (treatmentProcedureCriterion.getRequirement() == null)
			builder.append("any site");
		else
			builder.append(getSetDescription(treatmentProcedureCriterion));

		return builder.toString();
	}

	/**
	 * Returns the description of a treatment regimen criterion
	 * 
	 * @param treatmentRegimenCriterion
	 *            the criterion to describe
	 * @return the description of a treatment regimen criterion
	 * @throws IllegalArgumentException
	 *             if the criterion's parent is <code>null</code>
	 */
	private static String getTreatmentDescription(TreatmentRegimenCriterion treatmentRegimenCriterion)
			throws IllegalArgumentException
	{
		if (treatmentRegimenCriterion.getParent() == null)
			throw new IllegalArgumentException("criterion parent not set");
		StringBuilder builder = new StringBuilder();

		// No|No more than x|Any|At least x
		if (treatmentRegimenCriterion.isInvert())
			builder.append(treatmentRegimenCriterion.getCount() <= 0 ? "No" : "No more than "
					+ treatmentRegimenCriterion.getCount());
		else
			builder.append(treatmentRegimenCriterion.getCount() <= 1 ? "Any" : "At least "
					+ treatmentRegimenCriterion.getCount());

		// [prior|concurrent]
		List<CharacteristicCode> qualifiers = getList(treatmentRegimenCriterion.getQualifiers());
		if (qualifiers.remove(COMPLETED))
			builder.append(" prior");
		else if (qualifiers.remove(CONCURRENT))
			builder.append(" concurrent");

		// parent|treatment with agent1,agent2,...
		if (treatmentRegimenCriterion.getRequirement() == null)
			builder.append(' ').append(Common.getString(treatmentRegimenCriterion.getParent().toString()));
		else
			builder.append(" treatment with ").append(getSetDescription(treatmentRegimenCriterion));

		// [of at least threshold thresholdUnitOfMeasure]
		if (treatmentRegimenCriterion.getThresholdUnitOfMeasure() != null)
			builder.append(" of at least ").append(treatmentRegimenCriterion.getThreshold()).append(' ').append(
					Common.getString(THRESHOLD.toString() + '.'
							+ treatmentRegimenCriterion.getThresholdUnitOfMeasure().toString()));

		// in any|in the setting1,setting2 setting
		if (treatmentRegimenCriterion.getSettings() == null)
			builder.append(" in any");
		else
			builder.append(" in the ").append(
					getSetDescription(SETTING, treatmentRegimenCriterion.getSettings(), SetOperation.INTERSECT));
		builder.append(" setting");

		// [for recent malignancy]
		if (qualifiers.remove(MOSTRECENT))
			builder.append(" for recent malignancy");
		// [with progression]
		if (qualifiers.remove(PROGRESSING))
			builder.append(" with progression");

		return builder.toString();
	}

	/**
	 * Returns the enumeration of a criterion's elements. This method supports
	 * the special case of a single binary set, where the sets contents is just
	 * the parent. In this case this method returns the parent's localized
	 * description.
	 * 
	 * @param associativeCriterion
	 *            the criterion to enumerate
	 * @return the enumeration of a set's elements
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	private static StringBuilder getSetDescription(AssociativeCriterion associativeCriterion)
			throws IllegalArgumentException
	{
		if (associativeCriterion == null)
			throw new IllegalArgumentException("no criterion specified");
		return getSetDescription(associativeCriterion.getParent(), associativeCriterion.getRequirement(),
				associativeCriterion.getSetOperation());
	}

	/**
	 * Returns the enumeration of a set's elements. This method supports a
	 * prefix to prepend to the list.
	 * 
	 * @param parent
	 *            the set's parent
	 * @param set
	 *            the set to describe
	 * @param setOperation
	 *            the set operation that joins the elements
	 * @return the enumeration of a set's elements
	 */
	private static StringBuilder getSetDescription(CharacteristicCode parent, CharacteristicCode[] set,
			SetOperation setOperation)
	{
		StringBuilder builder = new StringBuilder();
		if ((parent == null) || (set == null) || (setOperation == null))
			return builder;

		// Add optional prefix to remove redundancy
		String prefix = Common.getString(parent.toString() + ".prefix");
		if (prefix.indexOf(parent.toString()) < 0)
			builder.append(prefix);
		else
			prefix = null;

		// Add each element separated by a conjunction
		String conjunction = null;
		for (CharacteristicCode element : set)
		{
			if (conjunction == null)
				conjunction = ' ' + Common.getString(Common.PREFIX_CONJUNCTION + setOperation.toString());
			else
				builder.append(conjunction);
			builder.append(Common.getString(parent.toString() + '.' + element.toString()));
		}

		// If subset operator then append the word "ONLY" in lowercase
		if ((conjunction != null) && (setOperation == SetOperation.SUBSET))
		{
			builder.append(' ');
			builder.append(Common.getString(Common.PREFIX_COMPARE + setOperation.toString()).toLowerCase());
		}
		return builder;
	}

	/**
	 * Returns a list that backs up an array's elements. This is a convenience
	 * method that wraps <code>Arrays.asList()</code> to return an empty
	 * <code>List</code> if <code>array</code> is <code>null</code>.
	 * 
	 * @param array
	 *            the array to back or <code>null</code> for an empty list
	 * @return a list that backs up the array
	 */
	private static List<CharacteristicCode> getList(CharacteristicCode[] array)
	{
		return (array == null) ? new LinkedList<CharacteristicCode>() : new LinkedList<CharacteristicCode>(Arrays
				.asList(array));
	}

	/**
	 * Maps the library name to its user interface components
	 */
	private static class Library
	{
		private final String name;
		private final List<AbstractCriterionUserInterface> list;

		Library(String name, List<AbstractCriterionUserInterface> list)
		{
			this.name = name;
			this.list = list;
		}
	}

	/**
	 * Returns the list of library names in order of display
	 * 
	 * @return the list of library names in order of display
	 * @see org.quantumleaphealth.gui.CriteriaUserInterface#getLibraryNames()
	 */
	public List<String> getLibraryNames()
	{
		if (libraries.isEmpty())
			initialize();
		return libraryNames;
	}

	/**
	 * Returns the user interface components contained in a library
	 * 
	 * @param libraryName
	 *            the library's name
	 * @return the user interface components contained in a library or
	 *         <code>null</code> if library does not exist
	 * @see org.quantumleaphealth.gui.CriteriaUserInterface#getLibraryUserInterface(java.lang.String)
	 */
	public List<AbstractCriterionUserInterface> getLibraryUserInterface(String libraryName)
	{
		if (libraries.isEmpty())
			initialize();
		// Loop through all available libraries
		for (Library library : libraries)
			if (library.name.equals(libraryName))
				return library.list;
		// If not found then return nothing
		return null;
	}

	/**
	 * Returns the name of a criterion's library
	 * 
	 * @param criterion
	 *            the criterion to describe
	 * @return the name of a criterion's library or <code>null</code> if the
	 *         criterion is not part of this interface's libraries
	 * @see org.quantumleaphealth.gui.CriteriaUserInterface#getLibraryName(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	public String getLibraryName(AbstractCriterion criterion)
	{
		if (libraries.isEmpty())
			initialize();
		// Find first compatible interfaces in available libraries
		for (Library library : libraries)
			for (AbstractCriterionUserInterface userInterface : library.list)
				if (userInterface.isCompatible(criterion))
					return library.name;
		// If not found then return nothing
		return null;
	}

	/**
	 * Capitalize the first character of a string
	 * 
	 * @param builder
	 *            the string to capitalize
	 */
	private static void capitalizeFirstChar(StringBuilder builder)
	{
		if ((builder != null) && (builder.length() > 0) && Character.isLowerCase(builder.charAt(0)))
			builder.setCharAt(0, Character.toUpperCase(builder.charAt(0)));
	}

	/**
	 * Displays the criteria in a frame
	 */
	private static void createAndShowGUI()
	{

		// Set UI to native look-and-feel with custom font and background color
		Font font = new Font("Verdana", Font.PLAIN, 10);
		// BCT-337 - set background color to light purple
		float[] hsbColorValues = Color.RGBtoHSB(240, 230, 250, null);
		Color backgroundColor = Color.getHSBColor(hsbColorValues[0], hsbColorValues[1], hsbColorValues[2]);
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements())
		{
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof javax.swing.plaf.FontUIResource)
				UIManager.put(key, font);
			if (value instanceof Color)
			{
				if (key instanceof String)
				{
					if (((String) key).endsWith(".background"))
						if (((String) key).contains("Pane"))
							UIManager.put(key, backgroundColor);
				}
				else if (key instanceof StringBuffer)
				{
					if (((StringBuffer) key).toString().endsWith(".background"))
						if (((StringBuffer) key).toString().contains("Pane"))
							UIManager.put(key, backgroundColor);
				}
			}
		}
		try
		{
			// Ignore any exceptions as this is not critical functionality
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (ClassNotFoundException classNotFoundException)
		{
		}
		catch (InstantiationException instantiationException)
		{
		}
		catch (IllegalAccessException illegalAccessException)
		{
		}
		catch (UnsupportedLookAndFeelException unsupportedLookAndFeelException)
		{
		}

		// Construct and show the graphical user interface
		new TrialFrame(new BreastCancerUserInterface());
	}

	/**
	 * Creates and shows the user interface using this class and
	 * <code>SwingUtilities.invokeLater()</code>.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args)
	{
		// Schedule a job for the event-dispatching thread: creating and showing
		// the GUI.
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				createAndShowGUI();
			}
		});
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 985529774015027260L;
}

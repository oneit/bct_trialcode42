/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import org.quantumleaphealth.model.trial.AbstractCriterion;

/**
 * A panel that contains libraries of editable criteria arranged in a "palette"
 * format.
 * 
 * @author Tom Bechtold
 * @version 2008-02-22
 */
class CriteriaUserInterfacePanel extends JPanel
{

	/**
	 * Wraps a criterion panel inside a container whose toggle button activates
	 * the criterion panel.
	 */
	private class ToggleCriterionPanel extends JPanel
	{
		/**
		 * A criterion's user interface
		 */
		private final AbstractCriterionUserInterface abstractCriterionUserInterface;
		/**
		 * Button that activates the user interface
		 */
		private final JToggleButton button;

		/**
		 * Layout a wide toggle button above a criterion user interface. The
		 * toggle button's label is set to the criterion panel's name.
		 * 
		 * @param userInterface
		 *            the criterion user interface
		 */
		public ToggleCriterionPanel(AbstractCriterionUserInterface userInterface)
		{
			super(new WideTitledColumnLayoutManager(0));
			this.abstractCriterionUserInterface = userInterface;
			if (userInterface == null)
				throw new IllegalArgumentException("must specify criterion panel");
			if (!(userInterface instanceof Component))
				throw new IllegalArgumentException("criterion interface is not a component");
			// Setup toggle button with background, alignment and user
			// interface's label
			button = new JToggleButton(Common.convertFirstCharToUpper(userInterface.getLabel()));
			button.setBackground(Common.BUTTON_BACKGROUND);
			button.setHorizontalAlignment(JToggleButton.LEFT);
			button.setRolloverEnabled(true);
			button.setName(button.getText());
			// Add button as the panel's title component and hidden criterion
			// panel underneath
			add(button);
			add((Component) userInterface);
			userInterface.setActive(false);
			// Selecting the button shows the criterion panel and requests focus
			button.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent actionEvent)
				{
					abstractCriterionUserInterface.setActive(button.isSelected());
				}
			});
		}

		/**
		 * Sets the visibility and deactivates the user interface
		 * 
		 * @param flag
		 *            whether the component is visible
		 * @see javax.swing.JComponent#setVisible(boolean)
		 */
		@Override
		public void setVisible(boolean flag)
		{
			if (!flag)
			{
				button.setSelected(false);
				abstractCriterionUserInterface.setActive(false);
			}
			super.setVisible(flag);
		}

		/**
		 * Version ID for serialization
		 */
		private static final long serialVersionUID = 5631997939022208416L;
	}

	/**
	 * Contains an activation button and input panels for a category of
	 * attributes.
	 */
	private class ToggleCriteriaLibraryPanel extends JPanel
	{
		/**
		 * This library's criteria components
		 */
		protected final ToggleCriterionPanel[] toggleCriterionPanel;
		/**
		 * Button that activates this panel
		 */
		protected final JToggleButton button;

		/**
		 * Lays out the button on top and the criteria below and activates the
		 * criteria if
		 * 
		 * @param label
		 *            the localized library name that this panel represents
		 * @param criteria
		 *            the criteria that this library contains
		 */
		private ToggleCriteriaLibraryPanel(String label, List<AbstractCriterionUserInterface> criteria)
		{
			// Use GridBag with one column and etched border
			super(new WideTitledColumnLayoutManager(true, 20));
			setBorder(BorderFactory.createEtchedBorder());
			// The title component is a button that uses the localized label
			button = new JToggleButton(label);
			button.setBackground(Common.BUTTON_BACKGROUND);
			button.setHorizontalAlignment(JToggleButton.LEFT);
			button.setRolloverEnabled(true);
			add(button);
			// When the button is clicked, set its children visibility
			button.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent actionEvent)
				{
					setChildrenVisibility();
				}
			});
			// Each criterion is a row
			List<ToggleCriterionPanel> toggleCriterionList = new LinkedList<ToggleCriterionPanel>();
			for (AbstractCriterionUserInterface criterion : criteria)
			{
				ToggleCriterionPanel child = new ToggleCriterionPanel(criterion);
				add(child);
				child.setVisible(false);
				toggleCriterionList.add(child);
				// When a child's button is activated, deactivate all of the
				// others
				child.button.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent actionEvent)
					{
						for (ToggleCriterionPanel childPanel : toggleCriterionPanel)
							if (childPanel.button.isSelected() && (childPanel.button != actionEvent.getSource()))
							{
								childPanel.button.setSelected(false);
								childPanel.abstractCriterionUserInterface.setActive(false);
							}
					}
				});
			}
			toggleCriterionPanel = toggleCriterionList.toArray(new ToggleCriterionPanel[toggleCriterionList.size()]);
		}

		private void setChildrenVisibility()
		{
			for (ToggleCriterionPanel childPanel : toggleCriterionPanel)
				childPanel.setVisible(button.isSelected());
		}

		/**
		 * Displays a criterion in the library. This method deactivates all of
		 * the criteria except the one represented by <code>criterion</code>. It
		 * sets its button to whether the library contains the criterion.
		 * 
		 * @param abstractCriterion
		 *            the criterion to display
		 * @return whether or not the criterion was displayed in this library
		 */
		private boolean setCriterion(AbstractCriterion abstractCriterion)
		{
			// There is only one matching criterion in the entire GUI
			boolean found = false;
			for (ToggleCriterionPanel child : toggleCriterionPanel)
				if (!found && child.abstractCriterionUserInterface.isCompatible(abstractCriterion))
				{
					child.abstractCriterionUserInterface.setCriterion(abstractCriterion);
					child.button.setSelected(true);
					found = true;
				}
				else
				{
					child.abstractCriterionUserInterface.setActive(false);
					child.button.setSelected(false);
				}
			button.setSelected(found);
			setChildrenVisibility();
			return found;
		}

		/**
		 * Gets the currently shown criterion.
		 * 
		 * @return a new criterion that represents the currently shown panel or
		 *         <code>null</code> if no panel is currently shown in this
		 *         library
		 * @throws IllegalStateException
		 *             if the currently shown panel does not have enough
		 *             information to create a criterion
		 */
		protected AbstractCriterion getCriterion() throws IllegalStateException
		{
			// There is at most only one shown attribute panel in the entire GUI
			for (ToggleCriterionPanel child : toggleCriterionPanel)
			{
				AbstractCriterion abstractCriterion = child.abstractCriterionUserInterface.getCriterion();
				if (abstractCriterion != null)
					return abstractCriterion;
			}
			// If no displayed criterion then return null
			return null;
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 7384524211434339957L;
	}

	/**
	 * Contains the libraries in this user interface.
	 */
	private final ToggleCriteriaLibraryPanel[] libraryPanel;

	/**
	 * Contains a wide column of categories and activates the one whose button
	 * is clicked.
	 * 
	 * @param criteriaUserInterface
	 *            the user interface contained in this panel
	 * @throws IllegalArgumentException
	 *             if the user interface is <code>null</code>
	 */
	CriteriaUserInterfacePanel(CriteriaUserInterface criteriaUserInterface) throws IllegalArgumentException
	{
		super(new WideTitledColumnLayoutManager());
		if (criteriaUserInterface == null)
			throw new IllegalArgumentException("user interface is not specified");

		// Add each library as a row
		List<String> libraryNames = criteriaUserInterface.getLibraryNames();
		List<ToggleCriteriaLibraryPanel> toggleCriteriaLibraryList = new ArrayList<ToggleCriteriaLibraryPanel>(
				libraryNames.size());
		for (String libraryName : libraryNames)
		{
			ToggleCriteriaLibraryPanel toggleCriteriaLibraryPanel = new ToggleCriteriaLibraryPanel(libraryName,
					criteriaUserInterface.getLibraryUserInterface(libraryName));
			add(toggleCriteriaLibraryPanel);
			toggleCriteriaLibraryList.add(toggleCriteriaLibraryPanel);
			// If a library button is pressed, deactivate any active libraries
			toggleCriteriaLibraryPanel.button.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent actionEvent)
				{
					for (ToggleCriteriaLibraryPanel childPanel : libraryPanel)
						if (childPanel.button.isSelected() && (childPanel.button != actionEvent.getSource()))
						{
							childPanel.button.setSelected(false);
							childPanel.setChildrenVisibility();
						}
				}
			});

		}
		libraryPanel = toggleCriteriaLibraryList.toArray(new ToggleCriteriaLibraryPanel[toggleCriteriaLibraryList
				.size()]);
	}

	/**
	 * Shows a criterion's values within the GUI. This method deactivates all of
	 * the libraries except the one represented by <code>criterion</code> and
	 * updates its input fields to match the requirements.
	 * 
	 * @return whether the criterion was shown
	 * @param criterion
	 *            the criterion to show or <code>null</code> to deactivate all
	 *            categories
	 */
	boolean setCriterion(AbstractCriterion criterion)
	{
		// There is only one matching attribute in the entire GUI
		boolean found = false;
		for (ToggleCriteriaLibraryPanel childPanel : libraryPanel)
			if (!found)
			{
				if (childPanel.setCriterion(criterion))
					found = true;
			}
			else
			{
				// Deactivate the library
				childPanel.button.setSelected(false);
				childPanel.setChildrenVisibility();
			}
		return found;
	}

	/**
	 * Gets the currently shown criterion. This method extracts the criterion
	 * from the currently shown panel.
	 * 
	 * @return a new criterion that represents the currently shown panel or
	 *         <code>null</code> if no panel is currently shown
	 * @throws IllegalStateException
	 *             if the currently shown panel does not have enough information
	 *             to create a criterion
	 */
	AbstractCriterion getCriterion() throws IllegalStateException
	{
		// There is at most only one active category and one shown attribute
		// panel in the entire GUI
		for (ToggleCriteriaLibraryPanel childPanel : libraryPanel)
		{
			AbstractCriterion criterion = childPanel.getCriterion();
			if (criterion != null)
				return criterion;
		}
		// If nothing returned then return null
		return null;
	}

	/**
	 * Version ID for serialization
	 */
	private static final long serialVersionUID = 7026279875813360125L;
}

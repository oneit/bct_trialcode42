/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.EligibilityCriteria;
import org.quantumleaphealth.model.trial.EligibilityCriterion;

/**
 * A panel that edits a set of eligibility criteria.
 * 
 * @author Tom Bechtold
 * @version 2008-06-07
 */
class EligibilityCriteriaPanel extends JPanel implements ActionListener
{

	/**
	 * The user interface that builds criteria
	 */
	private final CriteriaUserInterface userInterface;

	/**
	 * The source of the eligibility criteria
	 */
	private final JComboBox source;
	/**
	 * Enables undo functionality
	 */
	private final JButton undoButton;
	/**
	 * The date/time the criteria were last modified
	 */
	private final JLabel lastModifiedLabel;
	/**
	 * The date/time the eligibility criteria were last modified or 0 if never
	 */
	private long lastModified = 0l;

	/**
	 * Contains the panel that holds all the library panels
	 */
	private JPanel libraryColumn;

	/**
	 * Contains each library of eligibility criteria
	 */
	private final LibraryPanel[] libraryPanels;

	/**
	 * Enables edit, recategorized, renumber and delete functions
	 */
	private final JPopupMenu popupMenu;

	/**
	 * The menu item that represents the Move Up action
	 */
	private final JMenuItem upMenuItem;

	/**
	 * The menu item that represents the Move Down action
	 */
	private final JMenuItem downMenuItem;

	/**
	 * The popup menu that contains the libraries
	 */
	private final JPopupMenu libraryPopupMenu;

	/**
	 * The eligibility criterion selector
	 */
	private final EligibilityCriterionSelector selector;

	/**
	 * Edits an eligibility criterion
	 */
	private EligibilityCriterionDialog dialog = null;

	/**
	 * The criteria archive for undo functionality.
	 */
	private final LinkedList<List<EligibilityCriterion>> criteriaArchive = new LinkedList<List<EligibilityCriterion>>()
	{
		/**
		 * The date/time the eligibility criteria were last modified when loaded
		 * or 0 if never
		 */
		private long lastModifiedLoaded = 0l;

		/**
		 * Push deep-cloned list onto stack so we can independently change
		 * original version
		 * 
		 * @param criterionList
		 *            the list to push onto stack
		 * @see java.util.LinkedList#addFirst(java.lang.Object)
		 */
		public void addFirst(List<EligibilityCriterion> criterionList)
		{
			if (criterionList == null)
				return;
			ArrayList<EligibilityCriterion> clone = new ArrayList<EligibilityCriterion>(criterionList.size());
			for (EligibilityCriterion child : criterionList)
				clone.add((EligibilityCriterion) child.clone());
			super.addFirst(clone);
			// Enable the undo button
			undoButton.setEnabled(true);
			// Update the last modified
			lastModified = System.currentTimeMillis();
			updateLastModifiedLabel();
		}

		/**
		 * Remove the last item from the archive and update the undo button and
		 * last modified
		 * 
		 * @see java.util.LinkedList#removeFirst()
		 */
		@Override
		public List<EligibilityCriterion> removeFirst()
		{
			List<EligibilityCriterion> top = super.removeFirst();
			undoButton.setEnabled(!isEmpty());
			lastModified = isEmpty() ? lastModifiedLoaded : System.currentTimeMillis();
			updateLastModifiedLabel();
			return top;
		}

		/**
		 * Clear the list and update the undo button and last modified
		 * 
		 * @see java.util.LinkedList#clear()
		 */
		@Override
		public void clear()
		{
			super.clear();
			undoButton.setEnabled(false);
			lastModifiedLoaded = lastModified;
			updateLastModifiedLabel();
		}

		// Serial version UID for serialization
		private static final long serialVersionUID = 3039331510905318760L;
	};

	/**
	 * Represents the add functionality
	 */
	private static final String ADD = "add";

	/**
	 * Represents the paste functionality
	 */
	private static final String PASTE = "paste";

	/**
	 * Represents the undo functionality
	 */
	private static final String UNDO = "undo";

	/**
	 * Represents the edit functionality
	 */
	private static final String EDIT = "edit";

	/**
	 * Represents the up functionality
	 */
	private static final String UP = "up";

	/**
	 * Represents the down functionality
	 */
	private static final String DOWN = "down";

	/**
	 * Represents the delete functionality
	 */
	private static final String DELETE = "delete";

	/**
	 * Represents the clone functionality
	 */
	private static final String CLONE = "clone";

	/**
	 * The suffix to append to look up library submenu
	 */
	private static final String SUFFIX_SUBMENU = "submenu";

	/**
	 * The last library is for uncategorized criteria
	 */
	private static final String MISCELLANEOUS = "Miscellaneous";
	/**
	 * The date/time formatter for the last modified label
	 */
	private static final DateFormat FORMATTER = DateFormat.getDateTimeInstance();

	/**
	 * Displays buttons at <code>BorderLayout.NORTH</code> and a vertically
	 * scrolled list of eligibility criteria at <code>BorderLayout.CENTER</code>
	 * .
	 * 
	 * @param userInterface
	 *            the user interface that builds criteria
	 * @throws IllegalArgumentException
	 *             if the user interface is <code>null</code>
	 */
	public EligibilityCriteriaPanel(CriteriaUserInterface userInterface)
	{
		// Add attribute library panel to left side
		super(new BorderLayout());
		this.userInterface = userInterface;
		if (userInterface == null)
			throw new IllegalArgumentException("user interface not specified");
		// Source, buttons and last modified are on top, library panels in
		// center
		JPanel buttonPanel = new JPanel();
		JLabel label = new JLabel(Common.getString("label.source"));
		label.setDisplayedMnemonic(label.getText().charAt(0));
		source = new JComboBox();
		for (EligibilityCriteria.Source s : EligibilityCriteria.Source.values())
			source.addItem(Common.getString("label.source." + s.name()));
		source.setSelectedIndex(0);
		label.setLabelFor(source);
		buttonPanel.add(label);
		buttonPanel.add(source);
		buttonPanel.add(Common.createButton(ADD, this));
		buttonPanel.add(Common.createButton(PASTE, this));
		undoButton = Common.createButton(UNDO, this);
		undoButton.setEnabled(false);
		buttonPanel.add(undoButton);
		lastModifiedLabel = new JLabel();
		updateLastModifiedLabel();
		buttonPanel.add(lastModifiedLabel);
		add(buttonPanel, BorderLayout.NORTH);
		ArrayList<LibraryPanel> panelList = new ArrayList<LibraryPanel>();
		libraryColumn = new JPanel(new WideTitledColumnLayoutManager());
		for (String libraryName : userInterface.getLibraryNames())
		{
			LibraryPanel panel = new LibraryPanel(libraryName);
			panel.setVisible(false);
			libraryColumn.add(panel);
			panelList.add(panel);
		}
		// The miscellaneous library holds all non-criterion criteria
		LibraryPanel panel = new LibraryPanel(MISCELLANEOUS);
		panel.setVisible(false);
		libraryColumn.add(panel);
		panelList.add(panel);
		// Remember library panels and scroll them vertically
		libraryPanels = panelList.toArray(new LibraryPanel[panelList.size()]);
		JScrollPane scrollPane = new JScrollPane(libraryColumn, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// Estimate the unit increment from the font size plus highlighting
		// border widths
		scrollPane.getVerticalScrollBar().setUnitIncrement(libraryColumn.getFont().getSize() + 2);
		// BCT-309 - default scroll area to show the top, not the bottom
		scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMinimum());
		add(scrollPane, BorderLayout.CENTER);

		// Create pop-up menu for Delete, Clone, Up, Down and Library submenu
		// actions
		popupMenu = new JPopupMenu();
		popupMenu.add(createMenuItem(DELETE));
		upMenuItem = popupMenu.add(createMenuItem(UP));
		downMenuItem = popupMenu.add(createMenuItem(DOWN));
		popupMenu.add(createMenuItem(CLONE));
		// Submenu holds the library choices
		JMenu libraryMenu = new JMenu(Common.getString(Common.PREFIX_ACTION + SUFFIX_SUBMENU));
		libraryMenu.setName(SUFFIX_SUBMENU);
		libraryMenu.setActionCommand(SUFFIX_SUBMENU);
		for (LibraryPanel libraryPanel : libraryPanels)
		{
			JMenuItem menuItem = new JRadioButtonMenuItem(libraryPanel.getName());
			menuItem.setName(menuItem.getText());
			menuItem.setActionCommand(menuItem.getText());
			menuItem.addActionListener(this);
			libraryMenu.add(menuItem);
		}
		popupMenu.add(libraryMenu);
		libraryPopupMenu = libraryMenu.getPopupMenu();
		// Selector simulates the Edit click
		selector = new EligibilityCriterionSelector(popupMenu.add(createMenuItem(EDIT)));
	}

	/**
	 * Return a new menu item that uses <code>key</code> for the name, action
	 * command and localized label and listens to this class
	 * 
	 * @param key
	 *            key to lookup the localized label
	 * @return a new menu item
	 */
	private JMenuItem createMenuItem(String key)
	{
		JMenuItem menuItem = new JMenuItem(Common.getString(Common.PREFIX_ACTION + key));
		menuItem.setName(key);
		menuItem.setActionCommand(key);
		menuItem.addActionListener(this);
		return menuItem;
	}

	/**
	 * Ensures that the dialog is created. The dialog cannot be created until a
	 * <code>JFrame</code> owner is available. Therefore this method must only
	 * be called after the windowing system is completed.
	 */
	private void ensureDialogCreated()
	{
		if (dialog != null)
			return;
		// Create new dialog using this component's frame
		if ((getTopLevelAncestor() == null) || !(getTopLevelAncestor() instanceof JFrame))
			throw new IllegalStateException("Cannot create dialog without a frame!");
		JFrame frame = (JFrame) (getTopLevelAncestor());
		dialog = new EligibilityCriterionDialog(frame, userInterface);
		// Dialog is packed; set its location relative to its parent
		dialog.setLocationRelativeTo(frame);
	}

	/**
	 * Deletes and links selected panels or undoes the last add, delete or link
	 * operation.
	 * 
	 * @param actionEvent
	 *            the button-pressing event whose action command is either
	 *            <code>DELETE</code>, <code>UNDO</code> or a value in
	 *            <code>Criteria.Operator</code>
	 */
	public void actionPerformed(ActionEvent actionEvent)
	{

		// Add a new eligibility criterion
		if (ADD.equals(actionEvent.getActionCommand()))
		{
			ensureDialogCreated();
			dialog.setEligibilityCriterion(null);
			dialog.setVisible(true);
			EligibilityCriterion newCriterion = dialog.getEligibilityCriterion();
			// No criterion means the user canceled the dialog
			if (newCriterion == null)
				return;
			// Update the criterion's library to either its criterion's or last
			// library
			if (newCriterion.getLibrary() == null)
			{
				newCriterion.setLibrary(getCriterionLibraryName(newCriterion));
				// If no criterion then put into last library
				if (newCriterion.getLibrary() == null)
					newCriterion.setLibrary(MISCELLANEOUS);
			}
			// Archive and then add the criterion to its library's panel,
			// validate and show
			criteriaArchive.addFirst(getEligibilityCriteria());
			for (LibraryPanel libraryPanel : libraryPanels)
				if (libraryPanel.getName().equals(newCriterion.getLibrary()))
				{
					libraryPanel.add(newCriterion, -1);
					libraryPanel.revalidate();
					libraryPanel.repaint();
					break;
				}
			return;
		}

		// Paste new eligibility criteria from the system clipboard
		if (PASTE.equals(actionEvent.getActionCommand()))
		{
			List<EligibilityCriterion> pasted = EligibilityCriteriaParser.parseSystemClipboard(userInterface);
			int count = (pasted == null) ? 0 : pasted.size();
			if (count > 0)
			{
				criteriaArchive.addFirst(getEligibilityCriteria());
				addEligibilityCriteria(pasted);
			}
			JOptionPane.showMessageDialog(this, "Added " + count + " new eligibility criteria", "Pasted new criteria",
					count == 0 ? JOptionPane.WARNING_MESSAGE : JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		// Undo the archived eligibility criteria from the stack
		if (UNDO.equals(actionEvent.getActionCommand()))
		{
			if (!criteriaArchive.isEmpty())
			{
				// Remove all existing ones and add the latest archived
				for (LibraryPanel libraryPanel : libraryPanels)
					libraryPanel.removeCriterionPanels();
				addEligibilityCriteria(criteriaArchive.removeFirst());
				return;
			}
		}

		// Edit an existing eligibility criterion
		if (EDIT.equals(actionEvent.getActionCommand()))
		{
			ensureDialogCreated();
			dialog.setEligibilityCriterion(selector.lastSelection.eligibilityCriterion);
			dialog.setVisible(true);
			EligibilityCriterion newCriterion = dialog.getEligibilityCriterion();
			// No criterion means the user canceled the dialog
			if (newCriterion == null)
				return;
			// Recategorize if the criterion has a better library
			String criterionLibraryName = getCriterionLibraryName(newCriterion);
			if (MISCELLANEOUS.equals(newCriterion.getLibrary()) && (criterionLibraryName != null))
			{
				// Archive and remove the old panel from the old library
				criteriaArchive.addFirst(getEligibilityCriteria());
				actionPerformed(new ActionEvent(actionEvent.getSource(), ActionEvent.ACTION_PERFORMED, DELETE));
				// Add to the new library
				newCriterion.setLibrary(criterionLibraryName);
				for (LibraryPanel libraryPanel : libraryPanels)
					if (libraryPanel.getName().equals(criterionLibraryName))
					{
						libraryPanel.add(newCriterion, -1);
						libraryPanel.revalidate();
						libraryPanel.repaint();
						break;
					}
			}
			else
			{
				// Swap a new panel in for the old one in the same position
				int position = selector.lastSelection.getIndex();
				criteriaArchive.addFirst(getEligibilityCriteria());
				LibraryPanel libraryPanel = getLibraryPanel(selector.lastSelection);
				libraryPanel.remove(position);
				libraryPanel.add(newCriterion, position);
				libraryPanel.revalidate();
				libraryPanel.repaint();
			}
			return;
		}

		// Move criterion up within library
		if (UP.equals(actionEvent.getActionCommand()))
		{
			// Verify that the target can move up within its library
			LibraryPanel libraryPanel = getLibraryPanel(selector.lastSelection);
			int position = selector.lastSelection.getIndex();
			// Should never happen because we had disable the menu item before
			// showing popup
			if (position < 2)
			{
				JOptionPane.showMessageDialog(this, "Cannot move this criterion up", "Cannot go up",
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			// Archive, then remove and insert the criterion
			criteriaArchive.addFirst(getEligibilityCriteria());
			libraryPanel.remove(position);
			libraryPanel.add(selector.lastSelection, position - 1);
			// Renumber the swapped panels
			selector.lastSelection.setIndex(position - 1);
			((EligibilityCriterionPanel) (libraryPanel.getComponent(position))).setIndex(position);
			// Finally revalidate and repaint the updated GUI
			libraryPanel.revalidate();
			libraryPanel.repaint();
			return;
		}

		// Move criterion down within library
		if (DOWN.equals(actionEvent.getActionCommand()))
		{
			// Verify that the target can move down within its library
			LibraryPanel libraryPanel = getLibraryPanel(selector.lastSelection);
			int position = selector.lastSelection.getIndex();
			// Should never happen because we had disable the menu item before
			// showing popup
			if (position + 1 >= libraryPanel.getComponentCount())
			{
				JOptionPane.showMessageDialog(this, "Cannot move this criterion down", "Cannot go down",
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			// Archive, then remove and insert the criterion
			criteriaArchive.addFirst(getEligibilityCriteria());
			libraryPanel.remove(position);
			libraryPanel.add(selector.lastSelection, position + 1);
			// Renumber the swapped panels
			selector.lastSelection.setIndex(position + 1);
			((EligibilityCriterionPanel) (libraryPanel.getComponent(position))).setIndex(position);
			// Finally revalidate and repaint the updated GUI
			libraryPanel.revalidate();
			libraryPanel.repaint();
			return;
		}

		// Clone criterion to next spot within library
		if (CLONE.equals(actionEvent.getActionCommand()))
		{
			LibraryPanel libraryPanel = getLibraryPanel(selector.lastSelection);
			// Archive, then insert a clone into the next position
			criteriaArchive.addFirst(getEligibilityCriteria());
			EligibilityCriterion newEligibilityCriterion = (EligibilityCriterion) (selector.lastSelection.eligibilityCriterion
					.clone());
			int newEligibilityCriterionIndex = selector.lastSelection.getIndex() + 1;
			libraryPanel.add(newEligibilityCriterion, newEligibilityCriterionIndex);
			// Renumber any subsequent criteria below the inserted one
			for (int index = newEligibilityCriterionIndex + 1; index < libraryPanel.getComponentCount(); index++)
				((EligibilityCriterionPanel) (libraryPanel.getComponent(index))).setIndex(index);
			// Finally revalidate and repaint the updated GUI
			libraryPanel.revalidate();
			libraryPanel.repaint();
			return;
		}

		// Delete criterion
		if (DELETE.equals(actionEvent.getActionCommand()))
		{
			// Archive stack first
			criteriaArchive.addFirst(getEligibilityCriteria());
			LibraryPanel libraryPanel = getLibraryPanel(selector.lastSelection);
			int position = selector.lastSelection.getIndex();
			libraryPanel.remove(position);
			// If no criterion left then hide library; otherwise renumber,
			// revalidate and repaint it
			if (libraryPanel.getComponentCount() == 1)
				libraryPanel.setVisible(false);
			else
			{
				// Renumber any criteria below the deleted one
				for (int index = position; index < libraryPanel.getComponentCount(); index++)
					((EligibilityCriterionPanel) (libraryPanel.getComponent(index))).setIndex(index);
				libraryPanel.revalidate();
				libraryPanel.repaint();
			}
			return;
		}

		// If a library name was given then move the selected criterion to it
		LibraryPanel newLibraryPanel = null;
		for (LibraryPanel libraryPanel : libraryPanels)
			if (libraryPanel.getName().equals(actionEvent.getActionCommand()))
				newLibraryPanel = libraryPanel;
		if (newLibraryPanel != null)
		{
			// Do nothing if the new library is the same as the current one
			if (newLibraryPanel.getName().equals(getLibraryPanel(selector.lastSelection).getName()))
				return;
			// Remove the criterion from the old and then add it to the new
			// library
			criteriaArchive.addFirst(getEligibilityCriteria());
			actionPerformed(new ActionEvent(actionEvent.getSource(), ActionEvent.ACTION_PERFORMED, DELETE));
			selector.lastSelection.eligibilityCriterion.setLibrary(newLibraryPanel.getName());
			selector.lastSelection.setIndex(newLibraryPanel.getComponentCount());
			// If this is the first criterion in the new library then show it,
			// otherwise redisplay it
			newLibraryPanel.add(selector.lastSelection);
			if (newLibraryPanel.getComponentCount() == 2)
				newLibraryPanel.setVisible(true);
			else
				newLibraryPanel.revalidate();
			return;
		}

		// If no successful operation then warn the user
		getToolkit().beep();
	}

	/**
	 * Returns the parent library panel
	 * 
	 * @param eligibilityCriterionPanel
	 *            the panel whose parent is desired
	 * @return the parent library panel or <code>null</code> if not available
	 */
	private static LibraryPanel getLibraryPanel(EligibilityCriterionPanel eligibilityCriterionPanel)
	{
		return (eligibilityCriterionPanel == null) || !(eligibilityCriterionPanel.getParent() instanceof LibraryPanel) ? null
				: (LibraryPanel) (eligibilityCriterionPanel.getParent());
	}

	/**
	 * Returns the library of the eligibility criterion's first criterion
	 * 
	 * @param eligibilityCriterion
	 *            the eligibility criterion
	 * @return the library of the criterion's first criterion
	 */
	private String getCriterionLibraryName(EligibilityCriterion eligibilityCriterion)
	{
		// Look up the logic's first abstract criterion in the library
		AbstractCriterion firstCriterion = (eligibilityCriterion == null)
				|| eligibilityCriterion.getCriterion() == null ? null : eligibilityCriterion.getCriterion()
				.getFirstCriterion();
		return (firstCriterion == null) ? null : userInterface.getLibraryName(firstCriterion);
	}

	/**
	 * Returns the criteria in the order that they are displayed. This method
	 * builds the result by iterating through the displayed libraries.
	 * 
	 * @return the displayed criteria; guaranteed to be non-null
	 */
	protected EligibilityCriteria getEligibilityCriteria()
	{
		EligibilityCriteria eligibilityCriteria = new EligibilityCriteria();
		for (LibraryPanel libraryPanel : libraryPanels)
			for (Component component : libraryPanel.getComponents())
				if (component instanceof EligibilityCriterionPanel)
					eligibilityCriteria.add(((EligibilityCriterionPanel) (component)).eligibilityCriterion);
		// Update the source and last modified
		eligibilityCriteria
				.setSourceEligibilityCriteria(EligibilityCriteria.Source.values()[source.getSelectedIndex()]);
		eligibilityCriteria.setLastModified(lastModified);
		return eligibilityCriteria;
	}

	/**
	 * Sets the eligibility criteria to display and edit and warns the user if
	 * there were any criteria that were recategorized
	 * 
	 * @param eligibilityCriteria
	 *            the criteria to set or <code>null</code> to set an empty list
	 */
	protected void setEligibilityCriteria(EligibilityCriteria eligibilityCriteria)
	{
		// Remove all existing ones and add the new ones
		for (LibraryPanel libraryPanel : libraryPanels)
			libraryPanel.removeCriterionPanels();
		int count = addEligibilityCriteria(eligibilityCriteria);
		// Display a warning if any criterion were changed
		if (count > 0)
			JOptionPane.showMessageDialog(this, (count == 1 ? "One criterion was" : Integer.toString(count)
					+ " criteria were")
					+ " recategorized to " + libraryPanels[libraryPanels.length - 1].getName(),
					"Criteria recategorized", JOptionPane.WARNING_MESSAGE);
		// Store the source and last modified
		source.setSelectedIndex((eligibilityCriteria == null)
				|| (eligibilityCriteria.getSourceEligibilityCriteria() == null) ? 0 : eligibilityCriteria
				.getSourceEligibilityCriteria().ordinal());
		lastModified = (eligibilityCriteria == null) ? 0l : eligibilityCriteria.getLastModified();
		// Reset the undo functionality
		criteriaArchive.clear();
		// BCT-309 force Eligibility Criteria scroll area to initially display
		// at the top
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				libraryColumn.scrollRectToVisible(new Rectangle(1, 1, 1, 1));
			}
		});
	}

	/**
	 * Adds the eligibility criteria to the current set. This method inserts
	 * each criterion into the appropriate library. If a criterion's library is
	 * not found in <code>libraryPanels</code> then the criterion is categorized
	 * into the last library.
	 * 
	 * @return the number of recategorized criteria
	 * @param criteria
	 *            the criteria to add or <code>null</code> to do nothing
	 */
	private int addEligibilityCriteria(List<EligibilityCriterion> eligibilityCriteria)
	{
		if ((eligibilityCriteria == null) || (eligibilityCriteria.size() == 0))
			return 0;
		int changedLibrary = 0;
		for (EligibilityCriterion eligibilityCriterion : eligibilityCriteria)
		{
			boolean found = false;
			for (LibraryPanel libraryPanel : libraryPanels)
				if (libraryPanel.getName().equals(eligibilityCriterion.getLibrary()))
				{
					libraryPanel.add(eligibilityCriterion, -1);
					found = true;
					break;
				}
			// If no libraries matched then change the criterion's library to
			// the last one
			if (!found)
			{
				eligibilityCriterion.setLibrary(libraryPanels[libraryPanels.length - 1].getName());
				libraryPanels[libraryPanels.length - 1].add(eligibilityCriterion, -1);
				changedLibrary++;
			}
		}
		// Validate the changed GUI
		revalidate();
		return changedLibrary;
	}

	/**
	 * Update the last modified label
	 */
	private void updateLastModifiedLabel()
	{
		lastModifiedLabel.setText("Last modified: "
				+ ((lastModified <= 0) ? "n/a" : FORMATTER.format(new Date(lastModified))));
	}

	/**
	 * Version ID for serialization
	 */
	private static final long serialVersionUID = 816567774476436338L;

	/**
	 * Displays a library of eligibility criteria
	 */
	private class LibraryPanel extends JPanel
	{

		/**
		 * Create a titled column of criterion panels.
		 * 
		 * @param library
		 *            the library name used to look up the title
		 */
		private LibraryPanel(String library)
		{
			super(new WideTitledColumnLayoutManager());
			setName(library);
			JLabel label = new JLabel(library);
			label.setFont(label.getFont().deriveFont(Font.BOLD + Font.ITALIC, label.getFont().getSize() + 2));
			label.setName(library);
			add(label);
		}

		/**
		 * Adds a criterion to this library and listens for property events.
		 * 
		 * @param criterion
		 *            the criterion to add
		 * @param position
		 *            position to insert or -1 to add to end
		 */
		private void add(EligibilityCriterion eligibilityCriterion, int position)
		{
			add(new EligibilityCriterionPanel(eligibilityCriterion, position < 0 ? getComponentCount() : position),
					position);
			// Ensure that the non-empty library is displayed
			setVisible(true);
		}

		/**
		 * Remove all children from this container in preparation of adding
		 * different criteria
		 */
		private void removeCriterionPanels()
		{
			while (getComponentCount() > 1)
				remove(1);
			setVisible(false);
			revalidate();
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 2890459583339173355L;
	}

	/**
	 * A read-only representation of a eligibility criterion.
	 */
	private class EligibilityCriterionPanel extends JPanel
	{

		/**
		 * The eligibility criterion to represent
		 */
		private final EligibilityCriterion eligibilityCriterion;

		/**
		 * Holds the number of this criterion within its library parent
		 */
		private final JLabel label;

		/**
		 * Lays out the number to the left and the text, type, criterion and
		 * italicized qualifier in the center and registers a highlight
		 * listener.
		 * 
		 * @param eligibilityCriterion
		 *            the criterion to represent
		 * @param index
		 *            the number of the criterion within its parent library
		 */
		private EligibilityCriterionPanel(EligibilityCriterion eligibilityCriterion, int index)
		{
			super(new BorderLayout());
			this.eligibilityCriterion = eligibilityCriterion;
			if (eligibilityCriterion == null)
				throw new IllegalArgumentException("Must specify criterion");
			// The popup menu needs a mouse listener to work
			selector.register(this);
			// Index's label on the left; text, italicized qualifier and
			// criterion in center
			label = new JLabel();
			label.setVerticalAlignment(JLabel.TOP);
			setIndex(index);
			add(label, BorderLayout.WEST);
			// Only add the criterion's non-null elements
			JPanel panel = new JPanel(new WideTitledColumnLayoutManager());
			String string = Common.trimString(eligibilityCriterion.getText());
			if (string != null)
				panel.add(Common.createWordWrapTextArea(string));
			string = Common.trimString(eligibilityCriterion.getQualifier());
			if (string != null)
			{
				JTextArea qualifierTextArea = Common.createWordWrapTextArea(eligibilityCriterion.getQualifier());
				qualifierTextArea.setFont(getFont().deriveFont(java.awt.Font.ITALIC));
				panel.add(qualifierTextArea);
			}
			// Add bolded label for criterion type
			if (eligibilityCriterion.getCriterion() != null)
			{
				string = eligibilityCriterion.getType() == null ? null : eligibilityCriterion.getType().name();
				if (string != null)
				{
					JLabel label = new JLabel(Common.getString(Common.PREFIX_ACTION + string));
					label.setFont(label.getFont().deriveFont(Font.BOLD));
					panel.add(label);
				}
				panel.add(CriterionPanel.createCriterionPanel(eligibilityCriterion.getCriterion(), null, selector,
						null, userInterface));
			}
			add(panel, BorderLayout.CENTER);
			// TextAreas do not inherit their parents mouse-listener
			for (Component component : panel.getComponents())
				if (component instanceof JTextArea)
					component.addMouseListener(selector);
		}

		/**
		 * Returns the integer within this panel's label. This value is the same
		 * as the position of this panel within its parent library.
		 * 
		 * @return the integer within this panel's label
		 */
		private int getIndex()
		{
			return Integer.parseInt(label.getText().substring(0, label.getText().length() - 1));
		}

		/**
		 * Sets the number to the left of the criterion
		 * 
		 * @param index
		 *            number to set
		 */
		private void setIndex(int index)
		{
			label.setText(Integer.toString(index) + Common.SEPARATOR);
		}

		/**
		 * Sets the foreground of itself and all of its children
		 * 
		 * @param foreground
		 *            foreground color to set
		 */
		@Override
		public void setForeground(Color foreground)
		{
			super.setForeground(foreground);
			for (Component component : getComponents())
				component.setForeground(foreground);
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 8741095004990706556L;
	}

	/**
	 * Uses enter/exit and click mouse events to highlight a criterion and
	 * clicks on a menu item when the mouse is clicked
	 */
	private class EligibilityCriterionSelector implements MouseListener
	{

		/**
		 * Menu item to simulate a click when mouse clicked
		 */
		private final JMenuItem menuItemToClick;

		/**
		 * Most recently selected (entered) eligibility criterion or
		 * <code>null</code> if none selected so far. Once this field is set it
		 * is never <code>null</code> so that the popup knows which criterion to
		 * operate on.
		 */
		private EligibilityCriterionPanel lastSelection = null;

		/**
		 * Stores the menu item that will be clicked when the mouse is clicked
		 * 
		 * @param menuItemToClick
		 */
		EligibilityCriterionSelector(JMenuItem menuItemToClick)
		{
			this.menuItemToClick = menuItemToClick;
		}

		/**
		 * Forwards to <code>mouseReleased</code> for processing. Popup-menu
		 * activation may occur either when mouse is pressed or when it is
		 * released.
		 */
		public void mousePressed(MouseEvent mouseEvent)
		{
			mouseReleased(mouseEvent);
		}

		/**
		 * Change the popup menu's items and show the pop-up menu. The items
		 * that are changed are:
		 * <ul>
		 * <li>If the criterion cannot move down then disable DOWN
		 * <li>If the criterion cannot move up then disable UP
		 * <li>Update the library radio buttons for the criterion's library
		 * </ul>
		 * This method ignores events that are not popup menu events.
		 * 
		 * @param mouseEvent
		 *            the mouse-click event
		 */
		public void mouseReleased(MouseEvent mouseEvent)
		{
			// Ignore events that are not popup ones
			if (!mouseEvent.isPopupTrigger())
				return;
			// Update the library radio button menu items
			LibraryPanel libraryPanel = getLibraryPanel(lastSelection);
			for (Component libraryItem : libraryPopupMenu.getComponents())
				((JRadioButtonMenuItem) (libraryItem))
						.setSelected(libraryItem.getName().equals(libraryPanel.getName()));
			// Enable the up/down items
			upMenuItem.setEnabled(lastSelection.getIndex() > 1);
			downMenuItem.setEnabled(lastSelection.getIndex() < libraryPanel.getComponentCount() - 1);
			// Display the popup
			popupMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
		}

		/**
		 * Fires action event on menu item. This method ignores events that
		 * occur while the popup menu is showing
		 * 
		 * @param mouseEvent
		 *            the mouse-click event
		 */
		public void mouseClicked(MouseEvent mouseEvent)
		{
			// Ignore events while the popup menu is showing
			if (popupMenu.isVisible())
				return;
			menuItemToClick.doClick();
		}

		/**
		 * Highlight an eligibility criterion by coloring its border
		 * 
		 * @param mouseEvent
		 *            the mouse-enter event
		 */
		public void mouseEntered(MouseEvent mouseEvent)
		{
			Component component = mouseEvent.getComponent();
			while ((component != null) && !(component instanceof EligibilityCriterionPanel))
				component = component.getParent();
			lastSelection = (EligibilityCriterionPanel) (component);
			lastSelection.setBorder(Common.ROLLOVER);
		}

		/**
		 * Unhighlight an eligibility criterion by coloring its border to its
		 * background. This method <b>DOES NOT</b> clear the
		 * <code>lastSelection</code> field because if it did the popup would
		 * never know which criterion to operate on. (When the popup is entered
		 * then this method is called.)
		 * 
		 * @param mouseEvent
		 *            the mouse-exit event
		 */
		public void mouseExited(MouseEvent mouseEvent)
		{
			lastSelection.setBorder(Common.NONROLLOVER);
		}

		/**
		 * Add this listener to a panel and set the panel's border to
		 * unselected. Setting the border during registration will avoid
		 * resizing when the panel is first highlighted.
		 * 
		 * @param panel
		 */
		private void register(EligibilityCriterionPanel panel)
		{
			panel.addMouseListener(this);
			panel.setBorder(Common.NONROLLOVER);
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 7311635693878859031L;
	}
}

/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import org.quantumleaphealth.model.trial.Trial.Link;

import com.inet.jortho.SpellChecker;

/**
 * A panel that edits a trial's links. This class lays out input elements for a
 * label and a URL in a grid and provides methods to populate a list of links
 * and to update the GUI from a list.
 * 
 * @author Tom Bechtold
 * @version 2008-05-14
 */
public class TrialLinksPanel extends JPanel
{
	/**
	 * The number of links supported
	 */
	private static final int ROWS = 5;

	/**
	 * The input elements that hold the links' labels
	 */
	private final JTextField[] label = new JTextField[ROWS];

	/**
	 * The input elements that hold the links' URLs
	 */
	private final JTextField[] url = new JTextField[ROWS];

	/**
	 * Clears text fields when an action event occurs
	 */
	private static class TextFieldClearer implements ActionListener, Serializable
	{
		private final JTextField field1, field2;

		private TextFieldClearer(JTextField field1, JTextField field2)
		{
			this.field1 = field1;
			this.field2 = field2;
		}

		public void actionPerformed(ActionEvent actionEvent)
		{
			if (field1 != null)
				field1.setText(null);
			if (field2 != null)
				field2.setText(null);
		}

		private static final long serialVersionUID = 8371361692408073459L;
	}

	/**
	 * Version ID for serialization
	 */
	private static final long serialVersionUID = 2151035540709894634L;

	/**
	 * Displays each link GUI in a row underneath an instructions label
	 */
	public TrialLinksPanel()
	{
		super(new BorderLayout());
		JPanel tabPanel = new JPanel(new GridLayout(0, 1, 0, 3));
		add(tabPanel, BorderLayout.NORTH);
		tabPanel
				.add(new JLabel(
						"For each link enter its label and a URL, including the http:// prefix. Do not include the link to the cancer.gov record for this trial."));
		for (int index = 0; index < ROWS; index++)
		{
			// Add each row using border layout to stretch the url input across
			// available space
			label[index] = new JTextField(12);
			// BCT-305 add spell checker
			SpellChecker.register(label[index]);
			url[index] = new JTextField(50);
			// The button will clear both label and url fields
			JButton button = new JButton("Clear");
			button.addActionListener(new TextFieldClearer(label[index], url[index]));
			JPanel panel = new JPanel(new BorderLayout(5, 0));
			panel.add(label[index], BorderLayout.WEST);
			panel.add(url[index], BorderLayout.CENTER);
			panel.add(button, BorderLayout.EAST);
			tabPanel.add(panel);
		}
	}

	/**
	 * Updates a list of links with the information in the GUI. This method also
	 * updates the GUI with the resulting list. If either label or URL is
	 * missing, or if a URL is malformed then a warning message is displayed.
	 * 
	 * @return <code>true</code> if the update was successful;
	 *         <code>false</code> if a label/URL is missing or a URL is
	 *         malformed
	 * @param list
	 *            the list of links to update
	 */
	boolean update(List<Link> list)
	{
		// Validate parameter and URL's first
		if (list == null)
			throw new IllegalArgumentException("Must specify list of links to update");
		String[] labelText = new String[label.length];
		String[] urlText = new String[url.length];
		String badRows = "";
		String badLinks = "";
		for (int index = 0; index < labelText.length; index++)
		{
			labelText[index] = trimField(label[index]);
			urlText[index] = trimField(url[index]);
			if (((labelText[index] == null) && (urlText[index] != null))
					|| ((labelText[index] != null) && (urlText[index] == null)))
				badRows = (badRows.length() == 0) ? Integer.toString(index + 1) : badRows + ", "
						+ Integer.toString(index + 1);
			else if (urlText[index] != null)
				try
				{
					new URL(urlText[index]);
				}
				catch (MalformedURLException malformedURLException)
				{
					badLinks = (badLinks.length() == 0) ? Integer.toString(index + 1) : badLinks + ", "
							+ Integer.toString(index + 1);
				}
		}
		if (badRows.length() > 0)
		{
			JOptionPane.showMessageDialog(this, "Please correct the following row(s): " + badRows,
					"Some links are missing a label or URL", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		if (badLinks.length() > 0)
		{
			JOptionPane.showMessageDialog(this, "Please correct the following links(s): " + badLinks,
					"Some URLs are invalid", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		// Regenerate the list
		list.clear();
		for (int index = 0; index < labelText.length; index++)
			if (urlText[index] != null)
				list.add(new Link(labelText[index], urlText[index]));
		// Redisplay the GUI
		setList(list);
		return true;
	}

	/**
	 * Returns the trimmed text from a component or <code>null</code> if the
	 * string has no length.
	 * 
	 * @param jTextComponent
	 *            the component
	 * @return the component's trimmed text or <code>null</code> if the string
	 *         has no length
	 */
	private static String trimField(JTextComponent jTextComponent)
	{
		String value = jTextComponent.getText();
		if (value == null)
			return null;
		value = value.trim();
		return (value.length() == 0) ? null : value;
	}

	/**
	 * Updates the GUI with the trial's fields
	 * 
	 * @param list
	 *            the list to display
	 */
	void setList(List<Link> list)
	{
		if (list == null)
			throw new IllegalArgumentException("Must specify list of links to display");
		// Display items in the list that fit
		int count = 0;
		Iterator<Link> iterator = list.iterator();
		while ((count < label.length) && iterator.hasNext())
		{
			Link link = iterator.next();
			label[count].setText(link.getLabel());
			url[count].setText(link.getUrl());
			count++;
		}
		// Clear remaining rows
		while (count < label.length)
		{
			label[count].setText(null);
			url[count].setText(null);
			count++;
		}
		// If there are any links that were not displayed then display warning
		count = 0;
		while (iterator.hasNext())
		{
			count++;
			iterator.next();
		}
		if (count > 0)
			JOptionPane.showMessageDialog(this, "This tool only supports the display of " + label.length + " links; "
					+ count + " are not displayed and will not be stored", "Too many links to display",
					JOptionPane.WARNING_MESSAGE);
	}
}

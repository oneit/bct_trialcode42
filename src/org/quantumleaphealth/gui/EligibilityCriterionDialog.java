/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.quantumleaphealth.gui.CriterionPanel.AbstractCriterionPanel;
import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.Criteria;
import org.quantumleaphealth.model.trial.Criterion;
import org.quantumleaphealth.model.trial.EligibilityCriterion;
import org.quantumleaphealth.model.trial.Criteria.Operator;
import org.quantumleaphealth.model.trial.EligibilityCriterion.Type;

/**
 * Represents an eligibility criterion.
 * 
 * @author Tom Bechtold
 * @version 2008-06-07
 */
public class EligibilityCriterionDialog extends JDialog implements ActionListener
{

	/**
	 * The eligibility criterion to represent or <code>null</code> if the user
	 * canceled the dialog
	 */
	private EligibilityCriterion eligibilityCriterion = null;

	/**
	 * The working criteria.
	 */
	private ArrayList<Criterion> working;

	/**
	 * Contains the criterion's text
	 */
	private final JTextArea textTextArea;

	/**
	 * Contains the criterion's qualifier
	 */
	private final JTextArea qualifierTextArea;

	/**
	 * Contains the criterion's logic
	 */
	private final JPanel criteriaPanel;

	/**
	 * Contains the user interface to build criteria
	 */
	private final CriteriaUserInterface criteriaUserInterface;

	/**
	 * Contains the user interface to build criterion
	 */
	private final CriteriaUserInterfacePanel criteriaUserInterfacePanel;

	/**
	 * Controls the undo functionality
	 */
	private final JButton undo;

	/**
	 * Contains the criterion's type
	 */
	private final ButtonGroup typeButtonGroup;

	/**
	 * The criteria archive for undo functionality.
	 */
	private final LinkedList<ArrayList<Criterion>> criteriaArchive = new LinkedList<ArrayList<Criterion>>()
	{
		// Push deep-cloned list onto stack so we can independently change
		// original version
		public void addFirst(ArrayList<Criterion> criterionList)
		{
			if (criterionList == null)
				return;
			ArrayList<Criterion> clone = new ArrayList<Criterion>(criterionList.size());
			for (Criterion child : criterionList)
				try
				{
					clone.add((Criterion) child.clone());
				}
				catch (CloneNotSupportedException cloneNotSupportedException)
				{
					// Should never happen: add the original instead
					clone.add(child);
				}
			super.addFirst(clone);
			// Enable the undo button
			undo.setEnabled(true);
			return;
		}

		// Serial version UID for serialization
		private static final long serialVersionUID = -628927337722309306L;
	};

	/**
	 * Manages a list of selected panels sorted on when the panel was selected
	 */
	protected class SelectionManager extends ArrayList<CriterionPanel> implements PropertyChangeListener
	{

		/**
		 * Update list with selected/unselected panel. Fire property event with
		 * selected panel to listeners.
		 * 
		 * @param propertyChangeEvent
		 *            the selected/unselected event
		 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
		 */
		public void propertyChange(PropertyChangeEvent propertyChangeEvent)
		{
			// Ignore irrelevant events
			if (!(propertyChangeEvent.getSource() instanceof CriterionPanel))
				return;
			CriterionPanel criterionPanel = (CriterionPanel) (propertyChangeEvent.getSource());
			boolean selected = Boolean.TRUE.equals(propertyChangeEvent.getNewValue());
			// Update list and fire selected criterion event
			if (selected)
			{
				// The list must not have duplicate elements
				if (!contains(criterionPanel))
					add(criterionPanel);
				if (criterionPanel.getCriterion() instanceof AbstractCriterion)
					criteriaUserInterfacePanel.setCriterion((AbstractCriterion) criterionPanel.getCriterion());
			}
			else
				remove(criterionPanel);
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 3681049223181621788L;
	}

	/**
	 * Contains the currently selected panels
	 */
	private final SelectionManager selected = new SelectionManager();

	/**
	 * Represents the add functionality
	 */
	private static final String ADD = "add";

	/**
	 * Represents the delete functionality
	 */
	private static final String DELETE = "delete";

	/**
	 * Represents the invert functionality
	 */
	private static final String INVERT = "invert";

	/**
	 * Represents the undo functionality
	 */
	private static final String UNDO = "undo";

	/**
	 * Represents the OK functionality
	 */
	private static final String OK = "ok";

	/**
	 * Represents the cancel functionality
	 */
	private static final String CANCEL = "cancel";

	/**
	 * Create a modal dialog that represents an eligibility criterion. This
	 * constructor lays out the criterion's text and qualifier on the top, the
	 * attribute library on the left and the criterion panel on the right.
	 * Buttons on the bottom and window controls close the dialog.
	 * 
	 * @param owner
	 *            the owner of this dialog
	 * @param criteriaUserInterface
	 *            the user interface to support
	 */
	public EligibilityCriterionDialog(Frame owner, CriteriaUserInterface criteriaUserInterface)
	{
		// Create a BorderLayout modal dialog with the owner's title
		super(owner, owner == null ? null : owner.getTitle(), true);
		if (criteriaUserInterface == null)
			throw new IllegalArgumentException("user interface not specified");
		this.criteriaUserInterface = criteriaUserInterface;
		setLayout(new BorderLayout());

		// Criterion's text and qualifier are in North
		JPanel textPanel = new JPanel(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.anchor = GridBagConstraints.NORTHEAST;
		constraints.insets = new Insets(5, 5, 5, 5);
		JLabel label = new JLabel(Common.getString("label.text"));
		label.setHorizontalAlignment(JLabel.RIGHT);
		label.setVerticalAlignment(JLabel.TOP);
		label.setDisplayedMnemonic('T');
		textTextArea = new JTextArea(2, 0);
		textTextArea.setLineWrap(true);
		textTextArea.setWrapStyleWord(true);
		textTextArea.setBorder(BorderFactory.createLoweredBevelBorder());
		label.setLabelFor(textTextArea);
		textPanel.add(label, constraints);
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 1.0;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		textPanel.add(textTextArea, constraints);
		label = new JLabel(Common.getString("label.qualifier"));
		label.setHorizontalAlignment(JLabel.RIGHT);
		label.setVerticalAlignment(JLabel.TOP);
		label.setDisplayedMnemonic('Q');
		qualifierTextArea = new JTextArea(2, 0);
		qualifierTextArea.setLineWrap(true);
		qualifierTextArea.setWrapStyleWord(true);
		qualifierTextArea.setBorder(BorderFactory.createLoweredBevelBorder());
		qualifierTextArea.setFont(qualifierTextArea.getFont().deriveFont(Font.ITALIC));
		label.setLabelFor(qualifierTextArea);
		constraints.anchor = GridBagConstraints.NORTHEAST;
		constraints.weightx = 0.0;
		constraints.gridwidth = 1;
		textPanel.add(label, constraints);
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 1.0;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		textPanel.add(qualifierTextArea, constraints);
		add(textPanel, BorderLayout.NORTH);

		// Attribute library scrolls vertically in the West
		criteriaUserInterfacePanel = new CriteriaUserInterfacePanel(criteriaUserInterface);
		// Add vertical scrolling to the attribute library
		JScrollPane scrollPane = new JScrollPane(criteriaUserInterfacePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		// Set unit increment to scroll one button at a time
		scrollPane.getVerticalScrollBar().setUnitIncrement(Common.getButtonHeight());
		add(scrollPane, BorderLayout.WEST);

		// Criterion's criterion has buttons above criteria in Center
		Box actionPanel = new Box(BoxLayout.Y_AXIS);
		// Action buttons above link buttons
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(Common.createButton(ADD, this));
		buttonPanel.add(Common.createButton(DELETE, this));
		undo = Common.createButton(UNDO, this);
		undo.setEnabled(false);
		buttonPanel.add(undo);
		actionPanel.add(buttonPanel);
		buttonPanel = new JPanel();
		buttonPanel.add(new JLabel(Common.getString(Common.PREFIX_LABEL + "operator")));
		for (Criteria.Operator operator : Criteria.Operator.values())
			buttonPanel.add(Common.createButton(operator.name(), this));
		buttonPanel.add(Common.createButton(INVERT, this));
		actionPanel.add(buttonPanel);
		// Vertically scroll the criteria
		criteriaPanel = new JPanel(new WideTitledColumnLayoutManager());
		JPanel criterionPanel = new JPanel(new BorderLayout());
		criterionPanel.add(actionPanel, BorderLayout.NORTH);
		criterionPanel.add(new JScrollPane(criteriaPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER), BorderLayout.CENTER);
		add(criterionPanel, BorderLayout.CENTER);

		// Button panel contains OK/Cancel and types in South
		buttonPanel = new JPanel();
		buttonPanel.add(Common.createButton(OK, this));
		buttonPanel.add(Common.createButton(CANCEL, this));
		buttonPanel.add(new JLabel(Common.getString(Common.PREFIX_LABEL + "type")));
		typeButtonGroup = new ButtonGroup();
		for (EligibilityCriterion.Type type : EligibilityCriterion.Type.values())
		{
			JRadioButton radioButton = new JRadioButton(Common.getString(Common.PREFIX_ACTION + type.name()));
			// Set the button's action command to the enum name so we can query
			// the group's model
			radioButton.setName(type.name());
			radioButton.setActionCommand(type.name());
			buttonPanel.add(radioButton);
			typeButtonGroup.add(radioButton);
		}
		// Select the first button so one button will always be selected
		typeButtonGroup.getElements().nextElement().setSelected(true);
		add(buttonPanel, BorderLayout.SOUTH);

		// If the user cancels the window then clear the stored criterion
		addWindowListener(new WindowAdapter()
		{
			/**
			 * Clears the eligibility criterion
			 */
			public void windowClosing(java.awt.event.WindowEvent windowEvent)
			{
				eligibilityCriterion = null;
			}
		});

		// Pack the dialog, double its height and locate it next to its owner
		pack();
		setSize(getWidth() + 20, getHeight() * 2);
		setLocationRelativeTo(owner);
	}

	/**
	 * Cancels or saves the criterion or deletes and links selected panels or
	 * undoes the last add, delete or link operation. This method supports the
	 * following events:
	 * <ul>
	 * <li><code>CANCEL</code>
	 * <li><code>OK</code>
	 * <li><code>DELETE</code>
	 * <li><code>UNDO</code>
	 * <li>a value in <code>Criteria.Operator</code>
	 * </ul>
	 * 
	 * @param actionEvent
	 *            the button-pressing event
	 */
	public void actionPerformed(ActionEvent actionEvent)
	{

		// Cancel the dialog means clear the criterion
		if (CANCEL.equals(actionEvent.getActionCommand()))
		{
			eligibilityCriterion = null;
			setVisible(false);
			return;
		}

		// Verify criterion data and close the dialog
		if (OK.equals(actionEvent.getActionCommand()))
		{
			// Criterion text is mandatory
			if (Common.trimString(textTextArea.getText()) == null)
			{
				JOptionPane.showMessageDialog(this, "Please enter the criterion's text", "Please finish",
						JOptionPane.WARNING_MESSAGE);
				textTextArea.requestFocusInWindow();
				return;
			}
			// Criterion type is mandatory when there is at least one criterion
			if (!working.isEmpty() && EligibilityCriterion.Type.NOTMATCHED.name().equals(typeButtonGroup.getSelection().getActionCommand()))
			{
				JOptionPane.showMessageDialog(this, "Please choose the matching type", "Please finish",
						JOptionPane.WARNING_MESSAGE);
				typeButtonGroup.getElements().nextElement().requestFocusInWindow();
				return;
			}
			// Close the dialog so its owner can continue
			setVisible(false);
			return;
		}

		// Restore last archive and update button's enable
		if (UNDO.equals(actionEvent.getActionCommand()))
		{
			if (!criteriaArchive.isEmpty())
			{
				working = criteriaArchive.removeFirst();
				refresh();
			}
			undo.setEnabled(!criteriaArchive.isEmpty());
			return;
		}

		// Add a criterion from the attribute panel
		if (ADD.equals(actionEvent.getActionCommand()))
		{
			AbstractCriterion criterion = null;
			try
			{
				criterion = criteriaUserInterfacePanel.getCriterion();
			}
			catch (IllegalStateException illegalStateException)
			{
				// Do not have enough information to create a criterion; warn
				// user
				JOptionPane.showMessageDialog(this, illegalStateException.getMessage(),
						"Could not add a new criterion", JOptionPane.WARNING_MESSAGE);
				return;
			}
			if (criterion == null)
			{
				JOptionPane.showMessageDialog(this, "Please choose a library and an attribute", "Could not add",
						JOptionPane.WARNING_MESSAGE);
				return;
			}

			// If attributes match a single selection then edit selected
			// criterion
			if ((selected.size() == 1) && (selected.get(0) instanceof AbstractCriterionPanel)
					&& ((AbstractCriterion) (selected.get(0).getCriterion())).matchesSameCharacteristic(criterion))
			{
				// Archive working criteria and enable undo
				criteriaArchive.addFirst(working);
				// Update the selected panel and its parent criteria with the
				// new
				// criterion
				((AbstractCriterionPanel) (selected.get(0))).setCriterion(criterion);
				return;
			}

			// Otherwise add the new criterion to the end of the panel
			// Archive working criteria and enable undo/type
			criteriaArchive.addFirst(working);
			// Add the new criterion to the criteria panel
			criteriaPanel.add(CriterionPanel.createCriterionPanel(criterion, working, null, selected,
					criteriaUserInterface));
			criteriaPanel.revalidate();
			// Add the new criterion to the eligibility criteria
			working.add(criterion);
			// If this is the first criterion then enable the matching buttons
			if (working.size() == 1)
			{
				Enumeration<AbstractButton> typeButtons = typeButtonGroup.getElements();
				while (typeButtons.hasMoreElements())
				{
					AbstractButton button = typeButtons.nextElement();
					button.setEnabled(!button.getName().equals(EligibilityCriterion.Type.NOTMATCHED.name()));
				}
			}
			return;
		}

		// Delete all selected panels
		if (DELETE.equals(actionEvent.getActionCommand()) && (selected.size() > 0))
		{
			// Archive working criteria and enable the undo button
			criteriaArchive.addFirst(working);
			// Remove each panel's criterion from its parent criteria
			removeSelected();
			// Collapse any small criteria and refresh the display
			collapse(working);
			refresh();
			return;
		}

		// Invert all selected panels
		if (INVERT.equals(actionEvent.getActionCommand()) && (selected.size() > 0))
		{
			// Archive working criteria and enable the undo button
			criteriaArchive.addFirst(working);
			// Invert each criterion and refresh the display
			for (CriterionPanel criterionPanel : selected)
				criterionPanel.getCriterion().invert();
			refresh();
			return;
		}

		// Determine whether a link button was pressed
		Operator link = null;
		for (Operator operator : Operator.values())
			if (actionEvent.getActionCommand().equals(operator.toString()))
				link = operator;
		if (link != null)
			// IF/THEN requires exactly two arguments; AND, OR require at least
			// two
			if ((selected.size() > 1) && ((selected.size() == 2) || (link != Operator.IF)))
			{
				// Archive working set
				criteriaArchive.addFirst(working);
				// Insertion point for new criteria is where earliest selected
				// criterion currently is
				List<Criterion> insertionParent = selected.get(0).parent;
				int insertionIndex = insertionParent.indexOf(selected.get(0).getCriterion());
				// Remove all selected criteria from their parents
				removeSelected();
				// Populate a new criteria with the (orphaned) selected
				// criteria
				Criteria criteria = new Criteria();
				criteria.setOperator(link);
				for (CriterionPanel criterionPanel : selected)
					criteria.getChildren().add(criterionPanel.getCriterion());
				// If insertion point is out of bounds then add rather than
				// insert
				if ((insertionIndex >= 0) && (insertionIndex < insertionParent.size()))
					insertionParent.add(insertionIndex, criteria);
				else
					insertionParent.add(criteria);
				// Collapse any criteria that are now small after the deletion
				// and refresh the display
				collapse(working);
				refresh();
				return;
			}

		// If no successful operation then warn the user
		getToolkit().beep();
	}

	/**
	 * Remove each selected panel's criterion from its parent criteria.
	 */
	private void removeSelected()
	{
		for (CriterionPanel criterionPanel : selected)
			if (!criterionPanel.parent.remove(criterionPanel.getCriterion()))
				throw new IllegalStateException("Criterion " + criterionPanel.getCriterion()
						+ " could not be removed from " + criterionPanel.parent);
	}

	/**
	 * Recursively collapse child criteria that have less than two children.
	 * When a criteria is encountered, perform the following steps:
	 * <ol>
	 * <li>Recursively collapse its children
	 * <li>If a criteria now has no children then remove it.
	 * <li>If instead the criteria now has only one child then replace the
	 * criteria with its child.
	 * </ol>
	 * 
	 * @param children
	 *            the list of criterion to collapse
	 */
	private void collapse(List<Criterion> children)
	{
		if (children == null)
			return;
		int index = 0;
		while (index < children.size())
		{
			// Remove empty child (should never happen)
			if (children.get(index) == null)
			{
				children.remove(index);
				continue;
			}
			// Ignore non-criteria children
			if (!(children.get(index) instanceof Criteria))
			{
				index++;
				continue;
			}
			// Step 1: Collapse its children recursively
			Criteria criteria = (Criteria) (children.get(index));
			collapse(criteria.getChildren());
			// Step 2: If no children remaining then remove it
			if (criteria.getCount() == 0)
			{
				children.remove(index);
				continue;
			}
			// Step 3: If one child left then replace itself with its child
			if (criteria.getCount() == 1)
				children.set(index, criteria.getChildren().get(0));
			index++;
		}
	}

	/**
	 * Returns the displayed eligibility criterion
	 * 
	 * @return the displayed eligibility criterion
	 */
	protected EligibilityCriterion getEligibilityCriterion()
	{
		// If eligibility criterion is null then the user canceled the dialog
		if (eligibilityCriterion == null)
			return null;
		eligibilityCriterion.setText(Common.trimString(textTextArea.getText()));
		eligibilityCriterion.setQualifier(Common.trimString(qualifierTextArea.getText()));
		// Convert list into criterion with AND operator
		if (working.isEmpty())
			eligibilityCriterion.setCriterion(null);
		else if (working.size() == 1)
			eligibilityCriterion.setCriterion(working.get(0));
		else
		{
			Criteria criteria = new Criteria();
			criteria.setOperator(Operator.AND);
			criteria.setChildren(working);
			eligibilityCriterion.setCriterion(criteria);
		}
		// If no criterion then override the type with NOTMATCHED
		eligibilityCriterion.setType(working.isEmpty() ? EligibilityCriterion.Type.NOTMATCHED : EligibilityCriterion.Type.valueOf(typeButtonGroup.getSelection()
				.getActionCommand()));
		return eligibilityCriterion;
	}

	/**
	 * Sets the eligibility criteria to display and edit. This method clones the
	 * parameter so that any changes can be undone.
	 * 
	 * @param eligibilityCriterion
	 *            the eligibility criterion to set or <code>null</code> to set a
	 *            new criterion
	 */
	protected void setEligibilityCriterion(EligibilityCriterion eligibilityCriterion)
	{
		// Create a new object or clone the passed one
		eligibilityCriterion = (eligibilityCriterion == null) ? new EligibilityCriterion()
				: (EligibilityCriterion) eligibilityCriterion.clone();
		this.eligibilityCriterion = eligibilityCriterion;
		// Set the text values and type
		textTextArea.setText(eligibilityCriterion.getText());
		// Select the text area for easier initial editing
		textTextArea.selectAll();
		qualifierTextArea.setText(eligibilityCriterion.getQualifier());
		// Set the button that matches the type's name
		String typeName = eligibilityCriterion.getType().name();
		Enumeration<AbstractButton> enumeration = typeButtonGroup.getElements();
		while (enumeration.hasMoreElements())
		{
			AbstractButton button = enumeration.nextElement();
			if (typeName.equals(button.getName()))
			{
				button.setSelected(true);
				break;
			}
		}

		// Convert the criterion into a list and clear the archive stack
		Criterion criterion = this.eligibilityCriterion.getCriterion();
		if ((criterion == null) || (criterion instanceof Criterion))
		{
			working = new ArrayList<Criterion>(1);
			if (criterion != null)
				working.add(criterion);
		}
		else if (criterion instanceof Criteria)
			working = new ArrayList<Criterion>(((Criteria) (criterion)).getChildren());
		// Refresh the display, stack and undo button
		refresh();
		criteriaArchive.clear();
		undo.setEnabled(false);
		// Reset attribute library GUI
		criteriaUserInterfacePanel.setCriterion(null);
	}

	/**
	 * Displays the current set of eligibility criteria and enables the type
	 * buttons.
	 */
	private void refresh()
	{
		// Clear the selected list and rebuild the panel
		selected.clear();
		criteriaPanel.removeAll();
		for (Criterion criterion : working)
			criteriaPanel.add(CriterionPanel.createCriterionPanel(criterion, working, null, selected,
					criteriaUserInterface));
		// Refresh panel; must call revalidate AND repaint for it to work!
		criteriaPanel.revalidate();
		criteriaPanel.repaint();
		// Enable the type buttons if at least one criterion; otherwise select
		// the NOTMATCHED button
		boolean typeButtonEnable = !working.isEmpty();
		Enumeration<AbstractButton> typeButtons = typeButtonGroup.getElements();
		while (typeButtons.hasMoreElements())
		{
			AbstractButton typeButton = typeButtons.nextElement();
			typeButton.setEnabled(typeButtonEnable);
			if (!typeButtonEnable && typeButton.getName().equals(EligibilityCriterion.Type.NOTMATCHED.name()))
				typeButton.setSelected(true);
		}
	}

	/**
	 * Sets visibility and then requests focus to <code>textTextArea</code> if
	 * visible
	 */
	@Override
	public void setVisible(boolean b)
	{
		super.setVisible(b);
		if (b)
			textTextArea.requestFocusInWindow();
	}

	/**
	 * Version ID for serialization
	 */
	private static final long serialVersionUID = 816567774476436338L;
}

/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * Limits a text document to a set number of characters.
 * 
 * @author Tom Bechtold
 * @version 2008-04-04
 */
class MaximumLengthDocumentFilter extends DocumentFilter
{

	/**
	 * The maximum number of characters allowed in the document.
	 */
	private final int limit;

	/**
	 * Stores the limit.
	 */
	public MaximumLengthDocumentFilter(int limit)
	{
		super();
		this.limit = limit;
	}

	/**
	 * Validates the length and replaces a section of text with another string.
	 * 
	 * @param filterBypass
	 *            FilterBypass that can be used to mutate Document
	 * @param offset
	 *            location in Document
	 * @param replacedLength
	 *            length of text to delete
	 * @param newText
	 *            text to insert, null indicates no text to insert
	 * @param attributeSet
	 *            attributeSet indicating attributes of inserted text, null is
	 *            legal.
	 * @exception BadLocationException
	 *                if the length of the resulting string is more than
	 *                <code>limit</code>, or the given insert position is not a
	 *                valid position within the document
	 * @see javax.swing.text.DocumentFilter#replace(javax.swing.text.DocumentFilter.FilterBypass,
	 *      int, int, java.lang.String, javax.swing.text.AttributeSet)
	 */
	@Override
	public void replace(FilterBypass filterBypass, int offset, int replacedLength, String newText,
			AttributeSet attributeSet) throws BadLocationException
	{
		int textLength = (newText == null) ? 0 : newText.length();
		int exceeds = filterBypass.getDocument().getLength() - replacedLength + textLength - limit;
		if (exceeds > 0)
			throw new BadLocationException("Text is too long (" + exceeds
					+ " too many characters) to fit in database; maximum length is " + limit, offset);
		super.replace(filterBypass, offset, replacedLength, newText, attributeSet);
	}

	/**
	 * Validates the length and inserts a new string.
	 * 
	 * @param filterBypass
	 *            FilterBypass that can be used to mutate Document
	 * @param offset
	 *            the offset into the document to insert the content >= 0. All
	 *            positions that track change at or after the given location
	 *            will move.
	 * @param newText
	 *            the string to insert
	 * @param attributeSet
	 *            the attributes to associate with the inserted content. This
	 *            may be null if there are no attributes.
	 * @exception BadLocationException
	 *                if the length of the resulting string is more than
	 *                <code>limit</code>, or the given insert position is not a
	 *                valid position within the document
	 * @see javax.swing.text.DocumentFilter#insertString(javax.swing.text.DocumentFilter.FilterBypass,
	 *      int, java.lang.String, javax.swing.text.AttributeSet)
	 */
	@Override
	public void insertString(FilterBypass filterBypass, int offset, String newText, AttributeSet attributeSet)
			throws BadLocationException
	{
		// Insert the text with replacing existing text
		replace(filterBypass, offset, 0, newText, attributeSet);
	}
}

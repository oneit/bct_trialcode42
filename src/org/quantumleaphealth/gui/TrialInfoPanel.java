/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.net.URL;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.text.AbstractDocument;
import javax.swing.text.JTextComponent;

import org.quantumleaphealth.model.trial.Trial;

import com.inet.jortho.FileUserDictionary;
import com.inet.jortho.SpellChecker;

/**
 * A panel that edits a trial's information. This class lays out certain trial
 * fields in a two-column grid and provides methods to populate a trial from the
 * GUI and to update the GUI from a trial. The class supports maximum text field
 * lengths by using a document filter.
 * 
 * @author Tom Bechtold
 * @version 2009-03-19
 */
public class TrialInfoPanel extends JPanel
{

	static
	{
		URL theURL = TrialInfoPanel.class.getResource("/dictionary_en.ortho");
		SpellChecker.registerDictionaries(theURL, "en", "");
		SpellChecker.setUserDictionaryProvider(new FileUserDictionary());
	}
	/**
	 * Contains the trial's title
	 */
	private final JTextComponent title;

	/**
	 * Contains the trial's primary sponsor
	 */
	private final JTextComponent sponsor;

	/**
	 * Contains the trial's purpose
	 */
	private final JTextComponent purpose;

	/**
	 * Contains the trial's treatment plan
	 */
	private final JTextComponent treatmentPlan;
	/**
	 * Contains the trial's purpose
	 */
	private final JTextComponent trialpurpose;

	/**
	 * Contains the trial's treatment plan
	 */
	private final JTextComponent audience;
	/**
	 * Contains the trial's purpose
	 */
	private final JTextComponent studymaterial;

	/**
	 * Contains the trial's procedures
	 */
	private final JTextComponent procedures;

	/**
	 * Contains the trial's burden
	 */
	private final JTextComponent burden;

	/**
	 * Contains the trial's duration
	 */
	private final JTextComponent duration;

	/**
	 * Contains the trial's followup
	 */
	private final JTextComponent followup;

	/**
	 * Gridbag constraints for the text component column. The non-default values
	 * are:
	 * <ul>
	 * <li><code>gridwidth</code>: <code>GridBagConstraints.REMAINDER</code> so
	 * it is last column in its row</li>
	 * <li><code>weightx</code>: <code>1.0</code> so any extra horizontal space
	 * is used by this column</li>
	 * <li><code>anchor</code>: <code>GridBagConstraints.NORTHWEST</code> so it
	 * is anchored to the top corner of its label</li>
	 * <li><code>fill</code>: <code>GridBagConstraints.HORIZONTAL</code> so it
	 * takes up all available room horizontally</li>
	 * <li><code>insets</code>: equal external padding on all sides</li>
	 * <ul>
	 */
	private static final GridBagConstraints CONSTRAINTS = new GridBagConstraints(GridBagConstraints.RELATIVE,
			GridBagConstraints.RELATIVE, GridBagConstraints.REMAINDER, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
			GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);

	/**
	 * Version ID for serialization
	 */
	private static final long serialVersionUID = 2653373064978379281L;

	/**
	 * Displays a list of sites in a table underneath an input area
	 */
	public TrialInfoPanel()
	{
		// Lay out the labels and text components in a two-column grid
		super(new GridBagLayout());
		title = addRow(3, 255, Common.getString("info.title"), false, true);
		sponsor = addRow(1, 50, Common.getString("info.sponsor"), false, false);
		purpose = addRow(5, 1100, Common.getString("info.purpose"), false, true);
		treatmentPlan = addRow(5, 0, Common.getString("info.treatmentPlan"), true, true);
		// Add help row for seam-formatted text
		JTextField formatting = (JTextField) (addRow(1, 0, "(Formatting)", false, false));
		formatting
				.setText("_bolded_ *italicized* =bulleted #numbered ^superscript^ [link=>http://site] |<| |>| ALT-NumLock: ©=0169 ®=0174 ™=0153");
		formatting.setEditable(false);
		formatting.setBorder(null);
		trialpurpose = addRow(4, 1100, Common.getString("info.trialpurpose"), false, true);
		audience = addRow(3, 1100, Common.getString("info.audience"), false, true);
		studymaterial = addRow(5, 0, Common.getString("info.studymaterial"), true, true);
		JTextField formatting2 = (JTextField) (addRow(1, 0, "(Formatting)", false, false));
		formatting2
				.setText("_bolded_ *italicized* =bulleted #numbered ^superscript^ [link=>http://site] |<| |>| ALT-NumLock: ©=0169 ®=0174 ™=0153");
		formatting2.setEditable(false);
		formatting2.setBorder(null);
		procedures = addRow(3, 255, Common.getString("info.procedures"), true, true);
		burden = addRow(1, 127, Common.getString("info.burden"), false, true);
		duration = addRow(1, 50, Common.getString("info.duration"), false, false);
		followup = addRow(1, 255, Common.getString("info.followup"), false, true);
	}

	private JLabel createLabel(String label)
	{

		JLabel jLabel = new JLabel(label);
		jLabel.setHorizontalAlignment(JLabel.RIGHT);
		jLabel.setVerticalAlignment(JLabel.TOP);
		jLabel.setDisplayedMnemonic(label.charAt(0));

		return jLabel;
	}

	/**
	 * All JTextFields and JTextAreas will be scrollable by default. JScrollPane
	 * ensures the scrollers only show when needed.
	 * */
	private JComponent createTextComponent(int rows, int limit, boolean spellingEnabled)
	{

		// Create and add text component with constraints
		JComponent thisComponent = null;

		JTextComponent jTextComponent = null;
		if (rows <= 1)
			jTextComponent = new JTextField();
		else
		{
			JTextArea jTextArea = new JTextArea(rows, 0);
			jTextArea.setLineWrap(true);
			jTextArea.setWrapStyleWord(true);

			jTextComponent = jTextArea;
		}

		if (limit > 0)
			((AbstractDocument) (jTextComponent.getDocument()))
					.setDocumentFilter(new MaximumLengthDocumentFilter(limit));

		/* BCT-305 Add spellcheck capability if required */
		if (spellingEnabled)
		{
			SpellChecker.register(jTextComponent);
		}

		if (jTextComponent instanceof JTextArea)
		{
			thisComponent = new JScrollPane(jTextComponent);
			thisComponent.setBorder(BorderFactory.createLoweredBevelBorder());
		}
		else
		{
			thisComponent = jTextComponent;
		}

		return thisComponent;
	}

	/**
	 * Create and add a row to a <code>GridBagLayout</code> containing a text
	 * component and its label. This method creates a label, aligns it to the
	 * top-right corner and associates it with a new text component. If
	 * <code>rows</code> is more than one then the text component is a
	 * <code>jTextArea</code> that wraps on words. Otherwise it is a
	 * <code>JTextField</code>. This class also associates a document filter to
	 * the component to limit the number of characters allowed.
	 * 
	 * @param rows
	 *            if more than one, the number of rows in a
	 *            <code>jTextArea</code>
	 * @param limit
	 *            the maximum text length for the text or 0 for no limit
	 * @param label
	 *            the text of the accompanying label
	 * @param stretchVertically
	 *            whether or not to set this row's <code>weighty</code> to 1.0.
	 * @param spellingEnabled
	 *            a flag to indicate if this field should be registered with
	 *            SpellChecker (BCT-305)
	 * @return the new text component
	 */
	private JTextComponent addRow(int rows, int limit, String label, boolean stretchVertically, boolean spellingEnabled)
	{
		// Adjust consraints if stretching
		JTextComponent textComponent = null;
		GridBagConstraints rowConstraints = CONSTRAINTS;
		if (stretchVertically)
		{
			rowConstraints = (GridBagConstraints) CONSTRAINTS.clone();
			rowConstraints.weighty = 1.0;
		}
		// Add label first with no constraints, mnemonic of first character
		JLabel jLabel = createLabel(label);
		add(jLabel);

		JComponent comp = createTextComponent(rows, limit, spellingEnabled);
		comp.setToolTipText(label);

		if (comp instanceof JScrollPane)
		{
			textComponent = (JTextComponent) ((JViewport) comp.getComponent(0)).getComponent(0);
		}
		else
		{
			textComponent = (JTextComponent) comp;
		}

		jLabel.setLabelFor(comp);
		add(comp, rowConstraints);

		return textComponent;
	}

	/**
	 * Updates a trial with the information in the GUI. This method sets empty
	 * fields (e.g., trimmed text has zero length) to <code>null</code>.
	 * 
	 * @param trial
	 *            the trial to update
	 */
	void updateTrial(Trial trial)
	{
		// Validate parameter
		if (trial == null)
			throw new IllegalArgumentException("Must specify trial to update");
		// Populate each trial field from GUI
		trial.setTitle(trimField(title));
		trial.setSponsor(trimField(sponsor));
		trial.setPurpose(trimField(purpose));
		trial.setBurden(trimField(burden));
		trial.setTreatmentPlan(trimField(treatmentPlan));
		trial.setTrialpurpose(trimField(trialpurpose));
		trial.setAudience(trimField(audience));
		trial.setStudymaterial(trimField(studymaterial));
		trial.setDuration(trimField(duration));
		trial.setProcedures(trimField(procedures));
		trial.setFollowup(trimField(followup));
	}

	/**
	 * Returns the trimmed text from a component or <code>null</code> if the
	 * string has no length.
	 * 
	 * @param jTextComponent
	 *            the component
	 * @return the component's trimmed text or <code>null</code> if the string
	 *         has no length
	 */
	private static String trimField(JTextComponent jTextComponent)
	{
		String value = jTextComponent.getText();
		if (value == null)
			return null;
		
		value = value.replaceAll("�","");
		value = value.trim();
		return (value.length() == 0) ? null : value;
	}
	
	private static String trimString(String value)
	{
		if (value == null)
			return null;
		
		value = value.replaceAll("�","");
		value = value.trim();
		return (value.length() == 0) ? null : value;
	}

	/**
	 * Updates the GUI with the trial's fields and maximum string lengths.
	 * 
	 * @param trial
	 *            the trial to represent in the GUI
	 * @param maximumStringLength
	 *            the maximum string lengths of the trial's fields or
	 *            <code>null</code> for no maximum lengths
	 */
	void setTrial(Trial trial, Map<String, Integer> maximumStringLengths)
	{
		// If trial is not provided then blank each field
		if (trial == null)
			trial = new Trial();
		// BCT-302 auto add trial primary ID to end of title
		String titleString = trial.getTitle();
		
		//BCT-1191 Removing the NCT number from the title
		if ((titleString != null)  && titleString.matches(".*[(][^)]+[)]$") && !titleString.endsWith(trial.getPrimaryID().trim() + ")"))
		{
			JOptionPane.showMessageDialog(this, "This title may be incorrect.  It looks like it already ends with a primary ID but the primary ID does not match the primary ID of this trial.", "Bad Title Warning", JOptionPane.WARNING_MESSAGE);
		}
		
		if ((titleString != null) && !titleString.endsWith(trial.getPrimaryID().trim() + ")"))
		{
			titleString = titleString + " (" + trial.getPrimaryID().trim() + ")";
		}
		/*if (titleString != null)
		{
			Pattern replace = Pattern.compile("\\(NCT\\d{1,8}\\)");
			Matcher matcher = replace.matcher(titleString);
			titleString = matcher.replaceAll("");
		}*/
        
		setTextComponent(title, titleString, maximumStringLengths, "title");
		setTextComponent(sponsor, trial.getSponsor(), maximumStringLengths, "sponsor");
		setTextComponent(purpose, trimString(trial.getPurpose()), maximumStringLengths, "purpose");
		setTextComponent(burden, trimString(trial.getBurden()), maximumStringLengths, "burden");
		setTextComponent(treatmentPlan, trimString(trial.getTreatmentPlan()), maximumStringLengths, "treatmentPlan");
		setTextComponent(trialpurpose, trimString(trial.getTrialpurpose()), maximumStringLengths, "trialpurpose");
		setTextComponent(audience, trimString(trial.getAudience()), maximumStringLengths, "audience");
		setTextComponent(studymaterial, trimString(trial.getStudymaterial()), maximumStringLengths, "studymaterial");
		setTextComponent(duration, trial.getDuration(), maximumStringLengths, "duration");
		setTextComponent(procedures, trial.getProcedures(), maximumStringLengths, "procedures");
		setTextComponent(followup, trial.getFollowup(), maximumStringLengths, "followup");
	}

	/**
	 * Stores a text value into a text component and (optionally) sets the
	 * maximum length
	 * 
	 * @param jTextComponent
	 *            the text component
	 * @param text
	 *            the text to set
	 * @param maximumStringLengths
	 *            the maximum string length for each field (lower-case) or
	 *            <code>null</code> for not applicable
	 * @param fieldName
	 *            the name of the field
	 * @throws IllegalArgumentException
	 *             if the component is not provided
	 */
	static final void setTextComponent(JTextComponent jTextComponent, String text,
			Map<String, Integer> maximumStringLengths, String fieldName) throws IllegalArgumentException
	{
		// Validate component
		if (jTextComponent == null)
			throw new IllegalArgumentException("component not specified");
		jTextComponent.setText(text);
		// Set the document filter if specified
		Integer maximumLength = (maximumStringLengths == null) || (fieldName == null) ? null : maximumStringLengths
				.get(fieldName.toLowerCase());
		((AbstractDocument) (jTextComponent.getDocument())).setDocumentFilter((maximumLength == null)
				|| (maximumLength.intValue() <= 0) ? null : new MaximumLengthDocumentFilter(maximumLength.intValue()));
	}
}

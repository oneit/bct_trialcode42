/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import org.quantumleaphealth.model.trial.AbstractCriterion;

/**
 * Contains input elements to represent an abstract criterion.
 * 
 * @author Tom Bechtold
 * @version 2008-02-22
 */
public interface AbstractCriterionUserInterface
{
	/**
	 * Returns the localized name of this user interface.
	 * 
	 * @return the localized name of this user interface
	 */
	public String getLabel();

	/**
	 * Sets the user interface active or inactive
	 * 
	 * @param active
	 *            <code>true</code> to activate, <code>false</code> to
	 *            deactivate
	 */
	public void setActive(boolean active);

	/**
	 * Returns a new criterion
	 * 
	 * @return a new criterion or <code>null</code> if this component is not
	 *         active
	 * @throws IllegalStateException
	 *             if the interface does not have enough information to create a
	 *             criterion
	 */
	public AbstractCriterion getCriterion() throws IllegalStateException;

	/**
	 * Returns whether an abstract criterion is appropriate for this interface.
	 * 
	 * @return whether an abstract criterion is appropriate for this interface
	 * @param abstractCriterion
	 *            the abstract criterion to test
	 */
	public boolean isCompatible(AbstractCriterion abstractCriterion);

	/**
	 * Sets an abstract criterion's values into the interface. If the abstract
	 * criterion is not compatible then do nothing.
	 * 
	 * @param abstractCriterion
	 *            the abstract criterion to set
	 * @see #isCompatible(AbstractCriterion)
	 */
	public void setCriterion(AbstractCriterion abstractCriterion);
}

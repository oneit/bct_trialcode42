/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.ItemSelectable;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import org.quantumleaphealth.screen.CriterionProfile;
import org.quantumleaphealth.screen.EligibilityProfile;
import org.quantumleaphealth.screen.Matchable.CharacteristicResult;
import org.quantumleaphealth.screen.Matchable.Result;

/**
 * Displays eligibility profiles for a particular trial. This component lays out
 * a combo-box to select a profile and a grid to display the profile's criteria.
 * This class registers <code>ItemEvent</code> listeners to respond to selecting
 * a criterion. The event's item is the index of the criterion within the
 * profile.
 * 
 * @author Tom Bechtold
 * @version 2008-10-28
 */
public class EligibilityProfilePanel extends JPanel implements ItemSelectable
{
	/**
	 * Logs error messages
	 */
	private static final Logger LOGGER = Logger.getLogger(EligibilityProfilePanel.class.getName());
	/**
	 * Illuminates results
	 */
	private static final Map<Result, Color> RESULT_COLORS = new HashMap<Result, Color>(5, 10.f);
	static
	{
		RESULT_COLORS.put(Result.PASS, Color.GREEN);
		RESULT_COLORS.put(Result.PASS_SUBJECTIVE, Color.YELLOW);
		RESULT_COLORS.put(Result.FAIL_SUBJECTIVE, Color.ORANGE);
		RESULT_COLORS.put(Result.FAIL, Color.PINK);
		RESULT_COLORS.put(Result.AVATAR_SUBJECTIVE, Color.CYAN);
	}
	/**
	 * Border of un-rollover component
	 */
	private static final Border NONROLLOVER = BorderFactory.createLineBorder(SystemColor.control);
	/**
	 * Border of rollover component
	 */
	private static final Border ROLLOVER = BorderFactory.createLineBorder(SystemColor.textInactiveText);

	/**
	 * Describes an eligibility profile.
	 */
	private static class DescriptiveEligibilityProfile implements Serializable
	{
		/**
		 * The eligibility profile to describe, guaranteed to be non-
		 * <code>null</code>
		 */
		private final EligibilityProfile eligibilityProfile;

		/**
		 * Validates and stores the instance variable.
		 * 
		 * @param eligibilityProfile
		 *            the eligibility profile to describe
		 * @throws IllegalArgumentException
		 *             if the parameter is <code>null</code>
		 */
		public DescriptiveEligibilityProfile(EligibilityProfile eligibilityProfile) throws IllegalArgumentException
		{
			if (eligibilityProfile == null)
				throw new IllegalArgumentException("profile not specified");
			this.eligibilityProfile = eligibilityProfile;
		}

		/**
		 * @return the profile's description (or <code>&quot;n/a&quot;</code> if
		 *         <code>null</code>) prepended by whether or not it matches (
		 *         <code>&quot;*&quot;</code>)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			StringBuilder builder = new StringBuilder(1);
			builder.append(eligibilityProfile.isMatchDefinitive() ? '*' : ' ');
			builder.append(eligibilityProfile.getDescription() == null ? "n/a" : eligibilityProfile.getDescription());
			return builder.toString();
		}

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = 2918132158236431143L;
	}

	/**
	 * Displays the text, qualifier and characteristic results of a criterion's
	 * matching profile.
	 */
	private class CriterionProfilePanel extends JPanel
	{
		/**
		 * Displays the criterion text
		 */
		private final JTextArea textArea;
		/**
		 * Displays the characteristic results
		 */
		private final Box characteristicResultsPanel;

		/**
		 * Lays out the criterion's text to the left above its optional
		 * qualifier and the characteristic results to the right.
		 * 
		 * @param criterionProfile
		 *            the profile to display
		 * @param index
		 *            the 0-based index of this criterion within the eligibility
		 *            profile
		 * @throws IllegalArgumentException
		 *             if the parameter is <code>null</code>
		 */
		public CriterionProfilePanel(CriterionProfile criterionProfile, final int index)
				throws IllegalArgumentException
		{
			// Lay out vertically-expandable criterion text without qualifier
			// above characteristic results and unrollover border
			super(new BorderLayout(0, 0));
			setBorder(NONROLLOVER);
			if (criterionProfile == null)
				throw new IllegalArgumentException("criterion profile not specified");
			if (index < 0)
				throw new IllegalArgumentException("index cannot be negative " + index);
			// Display the type and text in an expandable area
			String text = criterionProfile.getType() == null ? "n/a" : criterionProfile.getType().toString();
			if (criterionProfile.getText() != null)
				text += ": " + criterionProfile.getText().trim();
			textArea = new JTextArea(text);
			textArea.setEditable(false);
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
			add(textArea, BorderLayout.PAGE_START);
			characteristicResultsPanel = new Box(BoxLayout.PAGE_AXIS);
			add(characteristicResultsPanel, BorderLayout.PAGE_END);
			// Bold textarea's font
			characteristicResultsPanel.setFont(textArea.getFont().deriveFont(Font.BOLD));
			// Display the results
			setCriterionProfile(criterionProfile);

			// Highlight itself if hovered over and send item-selected event if
			// clicked
			addMouseListener(new MouseAdapter()
			{
				@Override
				public void mouseEntered(MouseEvent mouseEvent)
				{
					setBorder(ROLLOVER);
				}

				@Override
				public void mouseExited(MouseEvent mouseEvent)
				{
					setBorder(NONROLLOVER);
				}

				@Override
				public void mouseClicked(MouseEvent mouseEvent)
				{
					fireItemStateChanged(index);
				}
			});
		}

		/**
		 * Displays each characteristic result in the result box.
		 * 
		 * @param characteristicResults
		 *            the list of characteristic results or <code>null</code>
		 *            for none
		 */
		private void setCriterionProfile(CriterionProfile criterionProfile)
		{
			// Reset backgrounds and results
			textArea.setBackground(SystemColor.control);
			characteristicResultsPanel.removeAll();
			if (criterionProfile == null)
				return;
			// Set the text's background to the criterion's matching result
			if (RESULT_COLORS.containsKey(criterionProfile.getResult()))
				textArea.setBackground(RESULT_COLORS.get(criterionProfile.getResult()));
			// Display each relevant characteristic and background cooresponding
			// to its result
			if (criterionProfile.getCharacteristicResults() != null)
				for (CharacteristicResult characteristicResult : criterionProfile.getCharacteristicResults())
					if (characteristicResult != null)
					{
						// TODO: Change string from toString() to
						// getCharacteristic once background colors are working
						JLabel resultLabel = new JLabel(characteristicResult.toString());
						if (RESULT_COLORS.containsKey(characteristicResult.getResult()))
							resultLabel.setBackground(RESULT_COLORS.get(characteristicResult.getResult()));
						characteristicResultsPanel.add(resultLabel);
					}
			// BCT-311 include a space after the characteristics so next
			// criterion will be separated from this one
			JLabel resultLabel = new JLabel(" ");
			characteristicResultsPanel.add(resultLabel);
		}

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = -4285197874893303301L;
	}

	/**
	 * Displays match results
	 */
	private final JLabel resultsLabel;
	/**
	 * Selects an eligibility profile.
	 */
	private final JComboBox profileComboBox;
	/**
	 * Item to display in combo-box if profiles not available
	 */
	private final String unavailable;
	/**
	 * Holds the criteria profiles for the selected eligibility profile. This
	 * container must <strong>ONLY</strong> hold instances of
	 * <code>CriterionProfilePanel</code>.
	 */
	private final Box grid;
	/**
	 * The index of the currently selected criterion as the only element or
	 * <code>null</code> if none selected.
	 */
	private Integer[] selectedIndex;

	/**
	 * Lays out the combo-box above the scrolled grid of criterion panels.
	 * 
	 * @param unavailable
	 *            displayed in combo-box when profiles are unavailable
	 */
	EligibilityProfilePanel(String unavailable)
	{
		// Combo-box is above scrolled criterion panels
		super(new BorderLayout(0, 10));
		this.unavailable = (unavailable != null) ? unavailable
				: "To see eligibility profiles, load a trial from a database";
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 0));
		JLabel label = new JLabel("Matches:");
		panel.add(label);
		resultsLabel = new JLabel("-");
		panel.add(resultsLabel);
		profileComboBox = new JComboBox();
		profileComboBox.addItem(this.unavailable);
		panel.add(profileComboBox);
		add(panel, BorderLayout.PAGE_START);
		grid = new Box(BoxLayout.PAGE_AXIS);
		// BCT-309 - add horizontal scroll bar
		JScrollPane scrollPane = new JScrollPane(grid, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.getVerticalScrollBar().setUnitIncrement(label.getFont().getSize() + 2);
		add(scrollPane, BorderLayout.CENTER);

		// Create holder of criteria that is updated when the user chooses a
		// different profile
		profileComboBox.addItemListener(new ItemListener()
		{
			/**
			 * Display the characteristic results of the chosen eligibility
			 * profile within the criterion profile panels.
			 * 
			 * @param itemEvent
			 *            the item event selecting an eligibility profile
			 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
			 */
			public void itemStateChanged(ItemEvent itemEvent)
			{
				// We only care about EligibilityProfile selection events when
				// the panels are available
				if ((grid.getComponentCount() == 0) || (itemEvent == null)
						|| (itemEvent.getStateChange() != ItemEvent.SELECTED) || (itemEvent.getItem() == null)
						|| !(itemEvent.getItem() instanceof DescriptiveEligibilityProfile))
					return;
				LOGGER.finer("Choosing profile " + itemEvent.getItem());
				// Validate the number of criteria equals the number of panels
				List<CriterionProfile> criteria = ((DescriptiveEligibilityProfile) (itemEvent.getItem())).eligibilityProfile
						.getCriteria();
				int count = criteria == null ? 0 : criteria.size();
				if (count != grid.getComponentCount())
				{
					LOGGER.warning("Mismatch in " + itemEvent.getItem() + " between " + count + " criteria and "
							+ grid.getComponentCount() + " criterion panels");
					return;
				}
				if (count == 0)
					return;
				// Update each criterion profile panel for its new
				// characteristic results
				Iterator<CriterionProfile> criteriaIterator = criteria.iterator();
				for (Component component : grid.getComponents())
					if ((component instanceof CriterionProfilePanel) && criteriaIterator.hasNext())
						((CriterionProfilePanel) (component)).setCriterionProfile(criteriaIterator.next());
				// Refresh the display
				validate();
			}
		});
	}

	/**
	 * Stores and displays a trial's eligibility profiles. This method uses the
	 * first element in the list as a reference to validate the subsequent
	 * elements for compatibility. If any subsequent element is not compatible
	 * (e.g., it does not reflect the same number and text of criteria as the
	 * reference element) then the offending element is transferred from the
	 * parameter list to the returned list.
	 * <p>
	 * If no profiles provided then the GUI is reset.
	 * 
	 * @param profiles
	 *            the trial's eligibility profiles which will be trimmed of
	 *            non-compatible elements or <code>null</code> for none
	 * @return the non-compatible elements or <code>null</code> if all profile
	 *         elements are compatible.
	 */
	List<EligibilityProfile> setProfiles(Iterator<EligibilityProfile> profiles)
	{
		// Reset GUI
		selectedIndex = null;
		profileComboBox.removeAllItems();
		grid.removeAll();
		validate();
		if ((profiles == null) || !profiles.hasNext())
		{
			profileComboBox.addItem(unavailable);
			return null;
		}
		EligibilityProfile referenceProfile = profiles.next();
		if (referenceProfile == null)
			return null;
		// Display the first (reference) profile in the combo-box and criteria
		// panels
		profileComboBox.addItem(new DescriptiveEligibilityProfile(referenceProfile));
		int criteriaCount = 0;
		if (referenceProfile.getCriteria() != null)
		{
			for (CriterionProfile criterionProfile : referenceProfile.getCriteria())
				if (criterionProfile != null)
					grid.add(new CriterionProfilePanel(criterionProfile, criteriaCount++));
		}

		// Add compatible subsequent entries to the combo-box
		List<EligibilityProfile> incompatible = new LinkedList<EligibilityProfile>();
		int compatibleCount = 0;
		int matchDefinitiveCount = 0;
		while (profiles.hasNext())
		{
			EligibilityProfile eligibilityProfile = profiles.next();
			// Validate the number of criteria
			if (eligibilityProfile == null)
				continue;
			if ((referenceProfile.getCriteria() == null) ? ((eligibilityProfile.getCriteria() != null) && (eligibilityProfile
					.getCriteria().size() > 0))
					: ((eligibilityProfile.getCriteria() == null) || (criteriaCount != eligibilityProfile.getCriteria()
							.size())))
			{
				incompatible.add(eligibilityProfile);
				profiles.remove();
				continue;
			}
			// Validate that each criterion is the same as in the reference
			// (type and text)
			Iterator<CriterionProfile> referenceCriteria = referenceProfile.getCriteria().iterator();
			Iterator<CriterionProfile> eligibilityCriteria = eligibilityProfile.getCriteria().iterator();
			boolean compatible = true;
			while (referenceCriteria.hasNext())
			{
				if (!eligibilityCriteria.hasNext())
				{
					compatible = false;
					break;
				}
				CriterionProfile referenceCriterionProfile = referenceCriteria.next();
				CriterionProfile eligibilityCriterionProfile = eligibilityCriteria.next();
				if ((referenceCriterionProfile.getText() != eligibilityCriterionProfile.getText())
						|| (referenceCriterionProfile.getType() != eligibilityCriterionProfile.getType()))
				{
					compatible = false;
					break;
				}
			}
			if (!compatible)
			{
				incompatible.add(eligibilityProfile);
				profiles.remove();
				continue;
			}
			// Add compatible choice to combo-box
			profileComboBox.addItem(new DescriptiveEligibilityProfile(eligibilityProfile));
			compatibleCount++;
			if (eligibilityProfile.isMatchDefinitive())
				matchDefinitiveCount++;
		}

		// Display the matching results, scroll to the top and return any
		// incompatible entries
		resultsLabel.setText(compatibleCount == 0 ? "-" : (Integer.toString(matchDefinitiveCount) + '/' + Integer
				.toString(compatibleCount)));
		validate();
		// BCT-309 force Eligibility Profiles scroll area to initially display
		// at the top
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				grid.scrollRectToVisible(new Rectangle(1, 1, 1, 1));
			}
		});
		return (incompatible.size() == 0) ? null : incompatible;
	}

	/**
	 * Adds an item listener.
	 * 
	 * @param itemListener
	 *            listens for criterion-selected events
	 * @see java.awt.ItemSelectable#addItemListener(java.awt.event.ItemListener)
	 */
	public void addItemListener(ItemListener itemListener)
	{
		listenerList.add(ItemListener.class, itemListener);
	}

	/**
	 * Removes an item listener.
	 * 
	 * @param itemListener
	 *            listens for criterion-selected events
	 * @see java.awt.ItemSelectable#removeItemListener(java.awt.event.ItemListener)
	 */
	public void removeItemListener(ItemListener itemListener)
	{
		listenerList.remove(ItemListener.class, itemListener);
	}

	/**
	 * @return the index of the selected criterion as the first element or
	 *         <code>null</code> if none selected
	 * @see java.awt.ItemSelectable#getSelectedObjects()
	 */
	public Object[] getSelectedObjects()
	{
		return selectedIndex;
	}

	/**
	 * Notifies all listeners of the selected criterion that have registered
	 * interest for notification on this event type.
	 * 
	 * @param index
	 *            the 0-based index of the selected criterion or <code>0</code>
	 *            to deselect all
	 */
	private void fireItemStateChanged(int index)
	{
		// Send deselection event for old selection
		if (selectedIndex != null)
		{
			// Do nothing if selection is not changing
			if ((selectedIndex.length == 1) && (selectedIndex[0] != null) && (selectedIndex[0].intValue() == index))
				return;
			// Update cached selection before firing the event so listeners do
			// not get confused
			Integer[] deselectedIndex = selectedIndex;
			selectedIndex = null;
			fireItemStateChanged(deselectedIndex[0], false);
		}
		// If clearing selection then return
		if (index < 0)
			return;
		// Fire select event
		selectedIndex = new Integer[] { Integer.valueOf(index) };
		fireItemStateChanged(selectedIndex[0], true);
	}

	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type. Copied from <code>JComboBox</code>.
	 * 
	 * @param item
	 *            an array where the first element is the 0-based index of the
	 *            criterion that is changing
	 * @param selected
	 *            whether or not the criterion was selected or deselected
	 * @see JComboBox
	 */
	private void fireItemStateChanged(Object item, boolean selected)
	{
		ItemEvent itemEvent = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, item, selected ? ItemEvent.SELECTED
				: ItemEvent.DESELECTED);
		// Notify listeners; copied from JComboBox.fireItemStateChanged()
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int index = listeners.length - 2; index >= 0; index -= 2)
			if (listeners[index] == ItemListener.class)
				((ItemListener) listeners[index + 1]).itemStateChanged(itemEvent);
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 294521849970773091L;
}

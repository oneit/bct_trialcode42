/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.io.Serializable;
import java.util.List;

import org.quantumleaphealth.model.trial.AbstractCriterion;

/**
 * The list of libraries and the user interface components contained in each
 * library.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public interface CriteriaUserInterface extends Serializable
{
	/**
	 * Returns the list of library names in order of display
	 * 
	 * @return the list of library names in order of display
	 */
	public List<String> getLibraryNames();

	/**
	 * Returns the user interface components contained in a library
	 * 
	 * @param libraryName
	 *            the library's name
	 * @return the user interface components contained in a library or
	 *         <code>null</code> if library does not exist
	 */
	public List<AbstractCriterionUserInterface> getLibraryUserInterface(String libraryName);

	/**
	 * Returns the name of a criterion's library
	 * 
	 * @param criterion
	 *            the criterion to describe
	 * @return the name of a criterion's library or <code>null</code> if the
	 *         criterion is not part of this interface's libraries
	 */
	public String getLibraryName(AbstractCriterion criterion);

	/**
	 * Returns a localized description of the criterion
	 * 
	 * @param criterion
	 *            the criterion to describe
	 * @return a localized description of the criterion
	 */
	public String getDescription(AbstractCriterion criterion);
}

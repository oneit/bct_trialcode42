/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.plaf.FileChooserUI;
import javax.swing.plaf.basic.BasicFileChooserUI;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.model.patient.Therapy;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.YearMonth;
import org.quantumleaphealth.model.trial.EligibilityCriteria;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.Trial_Category;
import org.quantumleaphealth.model.trial.Trial_CategoryPK;
import org.quantumleaphealth.model.trial.Trial.Registry;
import org.quantumleaphealth.model.trial.Trial_Mutation;
import org.quantumleaphealth.model.trial.Trial_MutationPK;
import org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.DeclarativeMatchingEngine;
import org.quantumleaphealth.screen.EligibilityProfile;
import org.quantumleaphealth.screen.StageMatchable;
import org.quantumleaphealth.xml.DecodingException;
import org.quantumleaphealth.xml.DecodingExceptionListener;

/**
 * Contains input fields for a trial's data.
 * 
 * @author Tom Bechtold
 * @version 2008-10-28
 */
public class TrialFrame extends JFrame implements ActionListener
{

	/**
	 * The user interface used to build criteria
	 */
	private final CriteriaUserInterface userInterface;

	/**
	 * The trial that is currently represented. This field is guaranteed by the
	 * constructor's call of <code>setTrial()</code> to be non-<code>null</code>
	 * .
	 */
	private Trial trial;
	/**
	 * The patients whose eligibility profiles are currently displayed.
	 */
	private List<UserPatient> patients;

	/**
	 * Optional URL of logo
	 */
	private final URL logoURL;
	/**
	 * Contains the trial's registry and identifier
	 */
	private final JLabel identifier;
	/**
	 * Contains whether or not the trial is open
	 */
	private final JCheckBox open;
	/**
	 * Contains whether or not the trial is open
	 */
	private final JCheckBox listed;
	
	/**
	 * If checked, then update the post date.
	 */
	private final JCheckBox newCheckBox;
	
	/**
	 * Dynamic label indicating wether trial has already been posted.
	 */
	private JLabel newCheckBoxLabel;
	
	/**
	 * Contains the trial's primary identifier
	 */
	private final JTextField primaryIdentifier;
	/**
	 * Contains the trial's pdq identifier
	 */
	private final JTextField pdqIdentifier;
	/**
	 * type Label, so that it can be resized to the larger secondary label.
	 */
	private final JLabel typeLabel;
	/**
	 * Contains the trial's type
	 */
	private final JComboBox type;
	/**
	 * Contains the trial's phase
	 */
	private final JComboBox phase;
	/**
	 * secondary type Label
	 */
	private final JLabel secondaryTypeLabel;
	
	/**
	 * Dual List Box of available and chosen secondary categories.
	 */
	private final DualListBox secondaryType;
	
	/**
	 * secondary type Label
	 */
	private final JLabel mutationLabel;
	
	/**
	 * Dual List Box of available and chosen secondary categories.
	 */
	private final DualListBox mutation;
	
	/**
	 * Contains the trial's data source
	 */
	private final JTextField dataSource;
	/**
	 * Contains the trial's name
	 */
	private final JTextField name;
	/**
	 * Contains the trial's summary information
	 */
	private final TrialInfoPanel trialInfoPanel;
	/**
	 * Contains the trial's link information
	 */
	private final TrialLinksPanel trialLinksPanel;
	/**
	 * Contains the trial's eligibility criteria
	 */
	private final EligibilityCriteriaPanel eligibilityCriteriaPanel;
	/**
	 * Contains the trial's eligibility profiles
	 */
	private final EligibilityProfilePanel eligibilityProfilePanel;
	/**
	 * Chooses a file to open or save
	 */
	private final JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView());
	/**
	 * Chooses a server to open
	 */
	private final RecordChooser recordChooser;
	/**
	 * Indicator if trial data is loaded from a file or not
	 */
	private boolean isDataFromFile;

	/**
	 * Represents the action of creating a new trial
	 */
	private static final String NEW = "new";
	/**
	 * Represents the action of opening a trial from a file
	 */
	private static final String OPEN = "open";
	/**
	 * Represents the action of saving a trial to a file
	 */
	private static final String SAVE = "save";
	/**
	 * Represents the action of loading a trial from a database
	 */
	private static final String DOWNLOAD = "download";
	/**
	 * Represents the action of storing a trial to a database
	 */
	private static final String UPLOAD = "upload";
	/**
	 * Represents the action of exporting a trial to a PDF file
	 */
	private static final String EXPORT = "export";
	/**
	 * Represents the action of reloading patients
	 */
	private static final String RELOAD = "reload";
	/**
	 * Represents the action of closing the main window
	 */
	private static final String EXIT = "exit";

	/**
	 * The string that represents and empty trial identifier
	 * 
	 * @see #identifier
	 */
	private static final String EMPTY_IDENTIFIER = Common.getString("label.identifier");

	/**
	 * Extension for XML (Extensible Markup Language) files
	 */
	private static final String XML = "xml";
	/**
	 * Extension for PDF (Portable Document Format) files
	 */
	private static final String PDF = "pdf";

	/**
	 * Relative path to file containing window icon
	 */
	private static final String ICON = "images/icon.png";
	/**
	 * Relative path to file containing logo image
	 */
	private static final String LOGO = "images/logo_bct.png";

	/**
	 * Identifies a dynamic date syntax
	 */
	private static final String DYNAMICDATE_TAG = "dynamicdates:";
	/**
	 * Conjunction between two dynamic dates
	 */
	private static final String DYNAMICDATE_CONJUNCTION = " ";
	/**
	 * Identifies a dynamic date for diagnosis
	 */
	private static final char DYNAMICDATE_DIAGNOSIS = 'd';
	/**
	 * Identifies a dynamic date for procedure
	 */
	private static final char DYNAMICDATE_PROCEDURE = 'p';
	/**
	 * Identifies a dynamic date for therapy
	 */
	private static final char DYNAMICDATE_THERAPY = 't';
	/**
	 * Separator between two numbers
	 */
	private static final String DYNAMICDATE_SEPARATOR = "&";
	/**
	 * Assignment operator for date length
	 */
	private static final String DYNAMICDATE_ASSIGNMENT = "=";
	/**
	 * Regular expression syntax for dynamic date operations. This syntax
	 * assumes the diagnosis date does not specify a characteristic code and the
	 * procedures and therapies must have at least one code. Examples of
	 * matching operations:
	 * <ul>
	 * <li><code>d=30</code>: set diagnosis date to 30 days ago</li>
	 * <li><code>p1034=25</code>: set starting date to 25 days ago for each
	 * procedure whose kind, type or location is code <tt>1034</tt></li>
	 * <li><code>t1034=70&35</code>: set starting date to 70 days ago and
	 * completion date to 35 days ago for each regimen whose agent or setting is
	 * code <tt>1034</tt></li>
	 * <li><code>t987&456=90&22</code>: set starting date to 90 days ago and
	 * completion date to 22 days ago for each regimen whose agent or setting
	 * refers to both codes <tt>987</tt> and <tt>456</tt></li>
	 * </ul>
	 */
	private static final String DYNAMICDATE_SYNTAX = "\\A(" + DYNAMICDATE_DIAGNOSIS + "|((" + DYNAMICDATE_PROCEDURE
			+ '|' + DYNAMICDATE_THERAPY + ")\\d+(\\&\\d+)*))=\\d+(\\&\\d+)?\\z";
	/**
	 * Number of milliseconds in a day
	 */
	private static final long MILLIS_IN_DAY = 1000l * 60l * 60l * 24l;

	/**
	 * This is an array of category tuples.  The tuple is necessary as the category/type is dependent on the order
	 * the type appears in the Trial.Type enum, but this order will be changed once the category list is sorted.
	 */
	private static final CategoryDataTuple[] sortedCategoryTuples;
	static
	{
		List<CategoryDataTuple> categories = new ArrayList<CategoryDataTuple>();
		
		for (Trial.Type t : Trial.Type.values())
		{
			if ( !t.equals(Trial.Type.OTHER) && !t.equals(Trial.Type.LYMPHEDEMA) && !t.equals(Trial.Type.RESPONSE_TO_TREATMENT) && !t.equals(Trial.Type.DIAGNOSIS) && !t.equals(Trial.Type.TREATMENT_BIOLOGICAL_EARLY_STAGE) && !t.equals(Trial.Type.TREATMENT_BIOLOGICAL_ADVANCED_DISEASE)) //BCT-579
			{	
				categories.add(new CategoryDataTuple((short) t.ordinal(),  Common.getString("label.type." + t.name())));
			}	
		}
        Collections.sort(categories);
		categories.add(0, new CategoryDataTuple((short) Trial.Type.OTHER.ordinal(),  Common.getString("label.type." + Trial.Type.OTHER.name())));
		sortedCategoryTuples = new CategoryDataTuple[ categories.size() ];
		categories.toArray( sortedCategoryTuples );
	}
	
	private String getNewCheckBoxLabel()
	{
		if (trial == null || trial.getPostDate() == null)
		{
			return "Click to post.";
		}
		else
		{	
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy 'at' HH:mm:ss");
			return "Posted " + sdf.format(trial.getPostDate());
		}	
	}
	
	/**
	 * Lays out input fields for primary identifier, name, attributes and
	 * criteria and constructs File menu.
	 * 
	 * @param userInterface
	 *            the user interface to build criteria
	 */
	TrialFrame(CriteriaUserInterface userInterface)
	{
		super();
		this.userInterface = userInterface;
		// Find the last modified date for the frame's title from the owning jar
		// file
		URL url = getClass().getResource(getClass().getSimpleName() + ".class");
		if ((url != null) && (url.getProtocol() != null) && ("jar".equals(url.getProtocol())))
		{
			String resourceName = url.toExternalForm();
			try
			{
				URLConnection connection = new URL(resourceName.substring(0, resourceName.indexOf('!') + 1)
						+ "/META-INF/MANIFEST.MF").openConnection();
				setTitle("BreastCancerTrials.org "
						+ DateFormat.getDateInstance(DateFormat.LONG).format(new Date(connection.getLastModified())));
			}
			catch (Throwable throwable)
			{
				setTitle("BreastCancerTrials.org");
			}
		}
		else
			setTitle("BreastCancerTrials.org");

		// Layout identifier/name on NORTH with labels and horizontally filled
		JPanel namePanel = new JPanel(new GridBagLayout());
		
		// First Row: primary identifier that takes up excess space
		identifier = new JLabel(EMPTY_IDENTIFIER);
		identifier.setHorizontalAlignment(JLabel.RIGHT);
		identifier.setVerticalAlignment(JLabel.TOP);
		primaryIdentifier = new JTextField(15);
		identifier.setLabelFor(primaryIdentifier);
		namePanel.add(identifier, new GridBagConstraints( 0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));
		namePanel.add(primaryIdentifier, new GridBagConstraints( 1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));

		// First Row: open (read-only), listed and primary category
		open = new JCheckBox(Common.getString("label.open"));
		open.setEnabled(false);
		open.setHorizontalAlignment(JLabel.RIGHT);
		open.setVerticalAlignment(JLabel.TOP);
		open.setDisplayedMnemonicIndex(0);
		namePanel.add(open, new GridBagConstraints( 2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 0, 0, 0, 0), 0, 0));
		JLabel listedLabel = new JLabel(Common.getString("label.listed"));
		listedLabel.setHorizontalAlignment(JLabel.RIGHT);
		listedLabel.setVerticalAlignment(JLabel.BOTTOM);

		listed = new JCheckBox(Common.getString("label.listed"));
		listed.setHorizontalAlignment(JLabel.RIGHT);
		listed.setVerticalAlignment(JLabel.TOP);
		namePanel.add(listed, new GridBagConstraints( 3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 0, 0, 0, 0), 0, 0));
		String primaryCategory = String.format("%-" + Common.getString("label.secondary.type").length() + "s", Common
				.getString("label.type"));
		typeLabel = new JLabel(primaryCategory);
		typeLabel.setHorizontalAlignment(JLabel.RIGHT);
		typeLabel.setVerticalAlignment(JLabel.BOTTOM);
		typeLabel.setDisplayedMnemonic(typeLabel.getText().charAt(0));
		type = new JComboBox();
		for (CategoryDataTuple tuple: sortedCategoryTuples)
		{
			type.addItem(tuple);
		}
		typeLabel.setLabelFor(type);
		namePanel.add(typeLabel, new GridBagConstraints( 4, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 0, 0, 0, 0), 0, 0));
		namePanel.add(type, new GridBagConstraints( 5, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 0, 0, 0, 0), 0, 0));
		
		JLabel label = new JLabel(Common.getString("label.phase"));
		label.setHorizontalAlignment(JLabel.RIGHT);
		label.setVerticalAlignment(JLabel.TOP);
		label.setDisplayedMnemonic(label.getText().charAt(0));
		phase = new JComboBox();
		for (Trial.Phase p : Trial.Phase.values())
			phase.addItem(Common.getString("label.phase." + p.name()));
		label.setLabelFor(phase);
		namePanel.add(label, new GridBagConstraints( 6, 0, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets( 0, 0, 0, 0), 0, 0));
		namePanel.add(phase, new GridBagConstraints( 7, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets( 0, 0, 0, 10), 0, 0));
		
		// New Second Row for PDQ ID
		pdqIdentifier = new JTextField(15);
		pdqIdentifier.setEditable(false);
		namePanel.add(new JLabel("PDQ ID:"), new GridBagConstraints( 0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));
		namePanel.add(pdqIdentifier, new GridBagConstraints( 1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));
		//namePanel.add(new JLabel(""), new GridBagConstraints( 2, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));
		newCheckBox = new JCheckBox(getNewCheckBoxLabel());
		newCheckBox.setSelected(false);
		newCheckBoxLabel = new JLabel();
		
		//newCheckBox = new JCheckBox(getNewCheckBoxLabel());
		//newCheckBox.setHorizontalAlignment(JLabel.RIGHT);
		//newCheckBox.setVerticalAlignment(JLabel.TOP);
		//newCheckBox.setSelected(false);
		
		namePanel.add(newCheckBox, new GridBagConstraints( 2, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));
		namePanel.add(newCheckBoxLabel, new GridBagConstraints( 3, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));

		// Third row:  New checkbox and New informational label
		
		secondaryTypeLabel = new JLabel(Common.getString("label.secondary.type"));
		secondaryTypeLabel.setDisplayedMnemonic(label.getText().charAt(0));
		secondaryType = new DualListBox("Categories");
		List<CategoryDataTuple> secondaryList = new ArrayList<CategoryDataTuple>();
		for (Trial.Type t: Trial.Type.values() )
		{
			if ( !t.equals(Trial.Type.DIAGNOSIS) && !t.equals(Trial.Type.LYMPHEDEMA) && !t.equals(Trial.Type.RESPONSE_TO_TREATMENT) && !t.equals(Trial.Type.TREATMENT_BIOLOGICAL_EARLY_STAGE) && !t.equals(Trial.Type.TREATMENT_BIOLOGICAL_ADVANCED_DISEASE))
			{	
				secondaryList.add( new CategoryDataTuple((short) t.ordinal(),  Common.getString("label.type." + t.name())));
			}	
		}
		secondaryType.addSourceElements( secondaryList.toArray() );
		namePanel.add(secondaryType, new GridBagConstraints( 5, 1, 3, 3, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets( 0, 0, 0, 10), 0, 0));

		mutationLabel = new JLabel(Common.getString("label.mutation"));
		mutationLabel.setDisplayedMnemonic(label.getText().charAt(0));
		mutation = new DualListBox("Mutations");
		List<MutationDataTuple> mutationList = new ArrayList<MutationDataTuple>();
		for (Trial.Mutation m: Trial.Mutation.values() )
		{
			mutationList.add( new MutationDataTuple((short) m.ordinal(),  m.getMutationDisplayName()));
		}
		mutation.addSourceElements( mutationList.toArray() );
		namePanel.add(mutation, new GridBagConstraints( 1, 3, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets( 0, 0, 0, 5), 0, 0));

		// Fourth Row with secondary category.
		logoURL = getClass().getResource(LOGO);
		//namePanel.add(new JLabel(new ImageIcon(logoURL)), new GridBagConstraints( 0, 3, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));
		namePanel.add(mutationLabel, new GridBagConstraints( 0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 0, 0, 0, 0), 0, 0));
		namePanel.add(secondaryTypeLabel, new GridBagConstraints( 4, 1, 1, 3, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 0, 0, 0, 0), 0, 0));


		// Fifth row: data source and name
		label = new JLabel(Common.getString("label.name"));
		label.setHorizontalAlignment(JLabel.RIGHT);
		label.setVerticalAlignment(JLabel.TOP);
		label.setDisplayedMnemonicIndex(label.getText().indexOf(' ') + 1);
		namePanel.add(label, new GridBagConstraints( 0, 4, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets( 5, 5, 5, 5), 0, 0));
		
		name = new JTextField( 20 );
		label.setLabelFor(name);
		namePanel.add(name, new GridBagConstraints( 1, 4, 4, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets( 0, 0, 0, 0), 0, 0));
		dataSource = new JTextField();
		dataSource.setDisabledTextColor(dataSource.getForeground());
		dataSource.setEnabled(false);
		namePanel.add(dataSource, new GridBagConstraints( 5, 4, 1, 2, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets( 0, 10, 0, 0), 0, 0));
		
		// Lay out identifier/name next to logo on top and tabbed panels below
		JPanel northPanel = new JPanel(new BorderLayout());
		northPanel.add(namePanel, BorderLayout.CENTER);
		add(northPanel, BorderLayout.NORTH);

		// Tab order: info, eligibility, links, eligibility profiles
		JTabbedPane tabbedPane = new JTabbedPane();
		trialInfoPanel = new TrialInfoPanel();
		tabbedPane.add(Common.getString("tab.summary"), trialInfoPanel);
		eligibilityCriteriaPanel = new EligibilityCriteriaPanel(userInterface);
		tabbedPane.add(Common.getString("tab.eligibilityCriteria"), eligibilityCriteriaPanel);
		trialLinksPanel = new TrialLinksPanel();
		tabbedPane.add(Common.getString("tab.links"), trialLinksPanel);
		eligibilityProfilePanel = new EligibilityProfilePanel(null);
		tabbedPane.add(Common.getString("tab.profiles"), eligibilityProfilePanel);
		// Set default opening tab to eligibility criteria
		tabbedPane.setSelectedIndex(1);
		add(tabbedPane, BorderLayout.CENTER);

		// Construct the Trial Chooser to load trials from a database
		recordChooser = RecordChooser.isLoaded() ? new RecordChooser(this) : null;

		// Add menu items including shortcuts
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu(Common.getString(Common.PREFIX_ACTION + "file"));
		menu.setMnemonic(KeyEvent.VK_F);
		menu.setName(menu.getText());
		menu.setActionCommand(menu.getText());
		menu.add(createMenuItem(NEW, KeyEvent.VK_N));
		menu.addSeparator();
		menu.add(createMenuItem(OPEN, KeyEvent.VK_O));
		// BCT-317 - reorder menu items
		if (recordChooser != null)
		{
			menu.add(createMenuItem(DOWNLOAD, KeyEvent.VK_L));
		}
		menu.addSeparator();
		menu.add(createMenuItem(SAVE, KeyEvent.VK_S));
		if (recordChooser != null)
		{
			menu.add(createMenuItem(UPLOAD, KeyEvent.VK_U));
		}
		menu.addSeparator();
		menu.add(createMenuItem(EXPORT, KeyEvent.VK_P));
		if (recordChooser != null)
		{
			menu.addSeparator();
			menu.add(createMenuItem(RELOAD, KeyEvent.VK_R));
		}
		menu.addSeparator();
		menu.add(createMenuItem(EXIT, KeyEvent.VK_X));
		menuBar.add(menu);
		setJMenuBar(menuBar);

		// Construct the File Chooser with the Accept All and custom (extension)
		// filters
		fileChooser.setDialogTitle(getTitle());
		fileChooser.setAcceptAllFileFilterUsed(true);
		fileChooser.addChoosableFileFilter(new ExtensionFileFilter());

		// Display a blank trial's data
		setTrial(null);

		// Set window icon and closing operation
		URL iconURL = getClass().getResource(ICON);
		setIconImage(new ImageIcon(iconURL).getImage());
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		// Pack, center in screen and show
		pack();

		this.validate();


		Dimension screenSize = getToolkit().getScreenSize();
		setLocation((screenSize.width - getSize().width) / 2, (screenSize.height - getSize().height) / 2);
		setVisible(true);
	}

	/**
	 * Create a menu item and attach this object as listener
	 * 
	 * @param key
	 *            look up localized text
	 * @param mnemonic
	 *            mnemonic and CTRL-mnemonic for accelerator
	 * @return a new menu item
	 */
	private JMenuItem createMenuItem(String key, int mnemonic)
	{
		JMenuItem item = new JMenuItem(Common.getString(Common.PREFIX_ACTION + key), mnemonic);
		item.setAccelerator(KeyStroke.getKeyStroke(mnemonic, KeyEvent.CTRL_DOWN_MASK));
		item.setName(key);
		item.setActionCommand(key);
		item.addActionListener(this);
		return item;
	}

	/**
	 * Filters files for an extension. The matching suffix consists of
	 * <code>Common.SEPARATOR</code> plus a string.
	 */
	private class ExtensionFileFilter extends FileFilter
	{
		/**
		 * The file extension to find - does not include the separator
		 */
		private String extension;
		/**
		 * The file extension to match to - includes the separator
		 */
		private String match;

		/**
		 * Sets the extension to find. This method prepends
		 * <code>Common.SEPARATOR</code>.
		 * 
		 * @param extension
		 *            the extension without a separator
		 */
		private void setExtension(String extension)
		{
			this.extension = extension;
			this.match = (extension == null) ? null : Common.SEPARATOR + extension;
		}

		/**
		 * Accept all directories and those files that end with the matching
		 * extension
		 * 
		 * @param file
		 *            the file to test
		 * @return if <code>file</code> is a directory or ends with
		 *         <code>match</code>
		 */
		@Override
		public boolean accept(File file)
		{
			return (file != null) && (file.isDirectory() || (match == null) || (file.getName().endsWith(match)));
		}

		/**
		 * Returns a description of this filter. This method looks up the text
		 * in the resource bundle using the extension - without the separator.
		 * 
		 * @return a localized description of this filter
		 */
		@Override
		public String getDescription()
		{
			return (extension == null) ? null : Common.getString(extension);
		}
	}

	/**
	 * Respond to menu events to create new, open, save, export and exit. The
	 * following events are supported:
	 * <ul>
	 * <li><code>NEW</code>: the GUI fields are reset to blank</li>
	 * <li><code>OPEN</code>: an XML file is loaded into the GUI fields</li>
	 * <li><code>SAVE</code>: the GUI fields are saved to an XML file</li>
	 * <li><code>EXPORT</code>: the GUI fields are exported to a PDF file</li>
	 * <li><code>EXIT</code>: the application's frame is closed and disposed</li>
	 * </ul>
	 * 
	 * @param actionEvent
	 *            the menu-item event
	 */
	public void actionPerformed(ActionEvent actionEvent)
	{

		if (actionEvent.getActionCommand().equals(NEW))
		{
			// Display blank trial
			setTrial(null);
			// To clear chooser's file, we cannot call setSelectedFile(null):
			// JDK bug #4893572
			FileChooserUI fileChooserUI = fileChooser.getUI();
			if (fileChooserUI instanceof BasicFileChooserUI)
				((BasicFileChooserUI) (fileChooserUI)).setFileName(null);
			return;
		}

		if (actionEvent.getActionCommand().equals(OPEN))
		{
			// Reset chooser and prompt user for XML file to load into GUI
			ensureFileExtension(XML);
			setFilterExtension(XML);
			if ((fileChooser.showOpenDialog(this) != JFileChooser.APPROVE_OPTION)
					|| (fileChooser.getSelectedFile() == null))
				return;
			try
			{
				// Open and display new trial
				isDataFromFile = true;
				setTrial((Trial) DecodingExceptionListener.decode(new BufferedInputStream(new FileInputStream(
						fileChooser.getSelectedFile()))));
				return;
			}
			catch (FileNotFoundException fileNotFoundException)
			{
				// FileNotFoundException means file could not be found or opened
				JOptionPane.showMessageDialog(this, "Could not open file " + fileChooser.getSelectedFile(),
						"Could not open file", JOptionPane.ERROR_MESSAGE);
			}
			catch (DecodingException decodingException)
			{
				// DecodingException means trial could not be decoded from XML
				String message = "Could not read trial from " + fileChooser.getSelectedFile();
				if (decodingException.getCause() != null)
					message += "; found " + decodingException.getCount() + " errors; first error: "
							+ decodingException.getCause().getMessage();
				JOptionPane.showMessageDialog(this, message, "Could not read trial", JOptionPane.ERROR_MESSAGE);
			}
			catch (ClassCastException classCastException)
			{
				// ClassCastException means the read object is not a Trial
				JOptionPane.showMessageDialog(this, "Could not load trial from " + fileChooser.getSelectedFile()
						+ "; message: " + classCastException.getMessage(), "Could not load trial",
						JOptionPane.ERROR_MESSAGE);
				classCastException.printStackTrace();
			}
		}

		if (actionEvent.getActionCommand().equals(DOWNLOAD))
		{
			// Prompt user for trial from database
			Trial loadedTrial = recordChooser.load();
			
			if (loadedTrial != null)
			{
				// Fetch patients if not already done so validation will show up
				if (patients == null)
					loadPatients();
				isDataFromFile = false;
				setTrial(loadedTrial);
			}
			return;
		}

		if (actionEvent.getActionCommand().equals(UPLOAD))
		{
			updateTrial();
			if ((trial.getId() == null)) // || (trial.getId().longValue() <= 0))
			{
				JOptionPane.showMessageDialog(this,
						"This trial data was not loaded from a database and therefore may not be stored in a database",
						"Trial database identifier missing", JOptionPane.WARNING_MESSAGE);
				return;
			}
			if (trial.getPrimaryID() == null)
			{
				JOptionPane.showMessageDialog(this, "Please enter a primary id for this trial", "Primary ID missing",
						JOptionPane.WARNING_MESSAGE);
				primaryIdentifier.requestFocusInWindow();
				return;
			}
			// Prompt user for database to store in
			recordChooser.store(trial);
			return;
		}

		if (actionEvent.getActionCommand().equals(SAVE))
		{
			// Prompt for XML file and store trial fields into instance variable
			if (!saveTrial(XML))
				return;
			try
			{
				// Write trial into XML file
				XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(fileChooser
						.getSelectedFile())));
				encoder.writeObject(trial);
				encoder.close();
				return;
			}
			catch (FileNotFoundException fileNotFoundException)
			{
				JOptionPane.showMessageDialog(this, "Could not save file " + fileChooser.getSelectedFile(),
						"Could not save trial in file", JOptionPane.ERROR_MESSAGE);
			}
		}

		if (actionEvent.getActionCommand().equals(EXPORT))
		{
			// Prompt for PDF file and store trial fields into instance variable
			if (!saveTrial(PDF))
				return;
			OutputStream outputStream = null;
			try
			{
				// Write trial into PDF file
				outputStream = new BufferedOutputStream(new FileOutputStream(fileChooser.getSelectedFile()));
				EligibilityCriteriaExport.write(trial, outputStream, userInterface, logoURL);
				return;
			}
			catch (FileNotFoundException fileNotFoundException)
			{
				JOptionPane.showMessageDialog(this, "Could not export file " + fileChooser.getSelectedFile(),
						"Could not export trial in file", JOptionPane.ERROR_MESSAGE);
				fileNotFoundException.printStackTrace();
			}
			catch (IOException exception)
			{
				JOptionPane.showMessageDialog(this, exception, "Could not export trial", JOptionPane.ERROR_MESSAGE);
				exception.printStackTrace();
			}
			finally
			{
				// Always close stream no matter what
				try
				{
					outputStream.close();
				}
				catch (Exception exception)
				{
				}
			}
		}

		if (actionEvent.getActionCommand().equals(RELOAD))
		{
			// Refresh patient list and eligibility profiles
			loadPatients();
			displayEligibilityProfiles();
			return;
		}

		if (actionEvent.getActionCommand().equals(EXIT))
		{
			// Disposing window should exit application
			setVisible(false);
			dispose();
			return;
		}

		// If didn't accomplish anything then warn user
		getToolkit().beep();
	}

	/**
	 * Sets the <code>fileChooser</code>'s file extension. This method updates
	 * each of the <code>fileChooser</code>'s filters for the extension and sets
	 * the current filter to the last one.
	 * 
	 * @param extension
	 *            the extension to set in the filter(s)
	 */
	private void setFilterExtension(String extension)
	{
		ExtensionFileFilter currentFilter = null;
		for (FileFilter fileFilter : fileChooser.getChoosableFileFilters())
			if (fileFilter instanceof ExtensionFileFilter)
			{
				currentFilter = (ExtensionFileFilter) (fileFilter);
				currentFilter.setExtension(extension);
			}
		if (currentFilter != null)
			fileChooser.setFileFilter(currentFilter);
	}

	/**
	 * Prompts the user for a file to write the trial information that is in the
	 * GUI fields. This method ensures that the <code>fileChooser</code>'s file
	 * contains the appropriate extension.
	 * 
	 * @param extension
	 *            the extension that the file must have
	 * @return whether or not the <code>trial</code> field was updated
	 */
	private boolean saveTrial(String extension)
	{
		// Prompt the user for the file name using the extension
		ensureFileExtension(extension);
		setFilterExtension(extension);
		// BCT-314 auto populate with current trial primary ID when saving
		File previouslySelectedFile = fileChooser.getSelectedFile();
		File saveAsFile = null;
		if (previouslySelectedFile == null)
		{
			saveAsFile = new File(trial.getPrimaryID());
		}
		else
		{
			String filePath = previouslySelectedFile.getParent();
			saveAsFile = new File(filePath + "/" + trial.getPrimaryID());
		}
		fileChooser.setSelectedFile(saveAsFile);

		if ((fileChooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION)
				|| (fileChooser.getSelectedFile() == null))
			return false;
		// Ensure file has correct extension and confirm overwriting any
		// existing file
		ensureFileExtension(extension);
		if (fileChooser.getSelectedFile().canWrite()
				&& (JOptionPane.showConfirmDialog(this, "Overwrite existing file?", "Overwrite file?",
						JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) != JOptionPane.YES_OPTION))
			return false;
		// Populate trial from GUI fields
		updateTrial();
		return true;
	}

	private Trial_Category createCategory( short category )
	{
		Trial_Category trialCategory = new Trial_Category();
		Trial_CategoryPK trialCategoryPK = new Trial_CategoryPK();
		trialCategoryPK.setCategory( category );
		trialCategoryPK.setTrialid( trial.getId() );
		trialCategory.setTrial_CategoryPK(trialCategoryPK);
		trialCategory.setCreated( new Date() );
		
		return trialCategory;
	}
	
	private Trial_Mutation createMutation( short mutation )
	{
		Trial_Mutation trialMutation = new Trial_Mutation();
		Trial_MutationPK trialMutationPK = new Trial_MutationPK();
		trialMutationPK.setMutation( mutation );
		trialMutationPK.setTrialid( trial.getId() );
		trialMutation.setTrial_MutationPK(trialMutationPK);
		trialMutation.setCreated( new Date() );
		
		return trialMutation;
	}
	
	/**
	 * Update instance variable with data from GUI fields. This method verifies
	 * that the <code>type</code> and <code>phase</code> fields have been
	 * selected.
	 */
	private void updateTrial()
	{
		// Type and phase cannot be set to null
		if (type.getSelectedIndex() < 0)
		{
			JOptionPane.showMessageDialog(this, "Please choose a trial type first", "Cannot save trial",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		if (phase.getSelectedIndex() < 0)
		{
			JOptionPane.showMessageDialog(this, "Please choose a phase first", "Cannot save trial",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		// Do not update identifier, open or registry
		trial.setListed(listed.isSelected());
		trial
				.setPrimaryID(primaryIdentifier.getText().trim().length() == 0 ? null : primaryIdentifier.getText()
						.trim());
		trial.setName(name.getText().trim().length() == 0 ? null : name.getText().trim());

		trial.setType(Trial.Type.values()[sortedCategoryTuples[type.getSelectedIndex()].getCategory()]);

		Set<Trial_Category> trialCategories = new HashSet<Trial_Category>();
        trialCategories.add( createCategory( sortedCategoryTuples[type.getSelectedIndex()].getCategory() ));
		
        for ( Object selectedCategory: secondaryType.getDestListModel().model )
        {
            trialCategories.add( createCategory( ((CategoryDataTuple)  selectedCategory).getCategory()));
        }
		trial.setTrial_Categories(trialCategories);
		
		Set<Trial_Mutation> trialMutations = new HashSet<Trial_Mutation>();
		
        for ( Object selectedMutation: mutation.getDestListModel().model )
        {
        	trialMutations.add( createMutation( ((MutationDataTuple) selectedMutation).getMutation()));
        }
		trial.setTrial_Mutations(trialMutations);
		
		trial.setPhase(Trial.Phase.values()[phase.getSelectedIndex()]);
		trialInfoPanel.updateTrial(trial);
		trialLinksPanel.update(trial.getLink());
		EligibilityCriteria eligibilityCriteria = trial.getEligibilityCriteria();
		eligibilityCriteria.clear();
		EligibilityCriteria newEligibilityCriteria = eligibilityCriteriaPanel.getEligibilityCriteria();
		eligibilityCriteria.addAll(newEligibilityCriteria);
		eligibilityCriteria.setLastModified(newEligibilityCriteria.getLastModified());
		eligibilityCriteria.setSourceEligibilityCriteria(newEligibilityCriteria.getSourceEligibilityCriteria());
		// BCT-388 - update eligibilityCriteriaLastModified
		trial.setEligibilityCriteriaLastModified(new Date(eligibilityCriteria.getLastModified()));
		
		Date now = new Date(); // for last modified and post date fields
		trial.setLastModified(now);
		
		// Set PostDate.  This is the ONLY place in the BCT application suite that this should be done.
		if (newCheckBox.isSelected())
		{	
			trial.setPostDate(now);
		}
	}

	/**
	 * Sets the instance variable and displays the trial's data within the user
	 * interface.
	 * 
	 * @param newTrial
	 *            the trial to set
	 */
	private void setTrial(Trial newTrial)
	{
		// Ensure that trial is non-null
		trial = (newTrial != null) ? newTrial : new Trial();
		Map<String, Integer> stringWidths = null;
		
		if ( recordChooser != null )
		{	
			stringWidths = recordChooser.getStringWidths();
			recordChooser.setResizable( true );
		}
		
		// Registry is guaranteed to be non-null
		String idText = Registry.UNASSIGNED.equals(trial.getRegistry()) ? null : trial.getRegistry().toString();
		if (trial.getId() != null)
			idText = (idText == null) ? '#' + trial.getId().toString() : idText + '#' + trial.getId().toString();
		identifier.setText(idText == null ? EMPTY_IDENTIFIER : (idText + ':'));
		
		if (trial.getPdqID() != null)
		{
			pdqIdentifier.setText(trial.getPdqID());
		} else
			pdqIdentifier.setText("");
		open.setSelected(trial.isOpen());
		listed.setSelected(trial.isListed());
		newCheckBox.setSelected(false);  // the new check box never starts checked.
		newCheckBox.setText(getNewCheckBoxLabel());
		//newCheckBoxLabel.setText(); // reset text
		// Name and primary identifier may have maximum field widths
		TrialInfoPanel.setTextComponent(name, trial.getName(), stringWidths, "name");
		TrialInfoPanel.setTextComponent(primaryIdentifier, trial.getPrimaryID(), stringWidths, "primaryID");
		
		// Trial type and phase are guaranteed to be non-null
		for ( int ndx = 0; ndx < sortedCategoryTuples.length; ndx++)
		{
			if ( sortedCategoryTuples[ndx].getCategory() == trial.getType().ordinal() )
			{
				type.setSelectedIndex(ndx);
			}
		}

		if ( trial.getTrial_Categories() != null )
		{
			secondaryType.clearDestinationListModel();
			secondaryType.clearSourceListModel();
			
			ArrayList<Short> otherCategoriesSelectedIndices = new ArrayList<Short>();
			for ( Trial_Category trialCategory: trial.getTrial_Categories() )
			{
				otherCategoriesSelectedIndices.add( trialCategory.getTrial_CategoryPK().getCategory() );
			}
			
			ArrayList<CategoryDataTuple> destinationValues = new ArrayList<CategoryDataTuple>();
			ArrayList<CategoryDataTuple> availableValues = new ArrayList<CategoryDataTuple>();
			
			for (Trial.Type t: Trial.Type.values() )
			{
				// don't display the primary type in the secondary category list.  It's confusing.
				if ( trial.getType().ordinal() != t.ordinal() && !t.equals(Trial.Type.LYMPHEDEMA) && !t.equals(Trial.Type.RESPONSE_TO_TREATMENT) && !t.equals(Trial.Type.DIAGNOSIS)&& !t.equals(Trial.Type.TREATMENT_BIOLOGICAL_EARLY_STAGE) && !t.equals(Trial.Type.TREATMENT_BIOLOGICAL_ADVANCED_DISEASE))
				{
					String category = Common.getString("label.type." + t.name());
					if ( otherCategoriesSelectedIndices.contains( (short) t.ordinal() ) )
					{
						destinationValues.add( new CategoryDataTuple( (short) t.ordinal(),  category ) );
					}
					else
					{
						availableValues.add( new CategoryDataTuple( (short) t.ordinal(),  category ) );
					}
				}
			}
			secondaryType.addDestinationElements(destinationValues.toArray());
			secondaryType.addSourceElements(availableValues.toArray());					
		}
		
		if ( trial.getTrial_Mutations() != null )
		{
			mutation.clearDestinationListModel();
			mutation.clearSourceListModel();
			
			ArrayList<Short> otherMutationsSelectedIndices = new ArrayList<Short>();
			for ( Trial_Mutation trialMutation: trial.getTrial_Mutations() )
			{
				otherMutationsSelectedIndices.add( trialMutation.getTrial_MutationPK().getMutation() );
			}
			
			ArrayList<MutationDataTuple> destinationValues = new ArrayList<MutationDataTuple>();
			ArrayList<MutationDataTuple> availableValues = new ArrayList<MutationDataTuple>();
			
			for (Trial.Mutation m: Trial.Mutation.values() )
			{
			// don't display the primary type in the secondary mutation list.  It's confusing.
				//String mutation = Common.getString("label.mutation." + m.name());
				if ( otherMutationsSelectedIndices.contains( (short) m.ordinal()) )
				{
					destinationValues.add(new MutationDataTuple((short) m.ordinal(), m.getMutationDisplayName()));
				}
				else
				{
					availableValues.add( new MutationDataTuple((short) m.ordinal(), m.getMutationDisplayName()));
				}
			}
			mutation.addDestinationElements(destinationValues.toArray());
			mutation.addSourceElements(availableValues.toArray());					
		}
		
		phase.setSelectedIndex(trial.getPhase().ordinal());
		// BCT-304 set where the trial was loaded from
		String sourceText = null;
		if (idText == null)
		{
			sourceText = "n/a";
		}
		else
		{
			if (isDataFromFile)
				sourceText = Common.getString("action.file");
			else
				sourceText = (String) recordChooser.getServerComboBox().getSelectedItem();
		}
		dataSource.setText(sourceText);
		// Set UI elements in other components; record chooser has maximum
		// string widths
		trialInfoPanel.setTrial(trial, stringWidths);
		trialLinksPanel.setList(trial.getLink());
		eligibilityCriteriaPanel.setEligibilityCriteria(trial.getEligibilityCriteria());
		displayEligibilityProfiles();
	}

	/**
	 * Ensure that the chooser's file has the correct extension.
	 * 
	 * @param extension
	 *            the extension that the file must have
	 */
	private void ensureFileExtension(String extension)
	{
		// Ignore directories
		if ((fileChooser.getSelectedFile() == null) || fileChooser.getSelectedFile().isDirectory()
				|| fileChooser.getSelectedFile().getName().endsWith(Common.SEPARATOR + extension))
			return;
		String name = fileChooser.getSelectedFile().getAbsolutePath();
		// Replace any invalid extension
		int separator = name.lastIndexOf(Common.SEPARATOR);
		if (separator >= 0)
			name = name.substring(0, separator);
		fileChooser.setSelectedFile(new File(name + Common.SEPARATOR + extension));
	}

	/**
	 * Load patient data from the database and update dynamic dates
	 */
	private void loadPatients()
	{
		// Load the patient data that matches the principal criterion and
		// credentials
		// TODO: prompt for patient principal and credentials
		try
		{
			patients = recordChooser.getPatients(null, null);
		}
		catch (SQLException sqlException)
		{
			JOptionPane.showMessageDialog(this, sqlException, "Could not load patients", JOptionPane.ERROR_MESSAGE);
			sqlException.printStackTrace();
			patients = null;
			return;
		}

		// Parse treatment notes for dynamic dates
		if ((patients == null) || patients.isEmpty())
			return;
		LinkedList<String> badOperations = new LinkedList<String>();
		for (UserPatient patient : patients)
		{
			PatientHistory patientHistory = (patient == null) ? null : patient.getPatientHistory();
			String treatmentDescription = (patientHistory == null) ? null : patientHistory
					.getString(BreastCancerCharacteristicCodes.CANCER_TREATMENT);
			if ((treatmentDescription == null) || (treatmentDescription.trim().length() == 0))
				continue;
			for (String line : treatmentDescription.split("\n"))
			{
				// If line does not start with the dynamic-date tag then igmore
				// it
				if (line == null)
					continue;
				line = line.toLowerCase().trim();
				if ((line.length() == 0) || !line.startsWith(DYNAMICDATE_TAG))
					continue;
				// Operate on each date specified
				operationLoop: for (String operation : line.substring(DYNAMICDATE_TAG.length()).split(
						DYNAMICDATE_CONJUNCTION))
				{
					if (operation == null)
						continue;
					operation = operation.trim();
					if (operation.length() == 0)
						continue;
					// Record and skip bad syntax
					if (!operation.matches(DYNAMICDATE_SYNTAX))
					{
						badOperations.add(patient.getPrincipal() + ": \"" + operation + '"');
						continue;
					}

					// Parse number of days to offset from today
					String[] sides = operation.split(DYNAMICDATE_ASSIGNMENT);
					// Set diagnosis year (but not month) if specified
					if ((sides[0].length() == 1) && (sides[0].charAt(0) == DYNAMICDATE_DIAGNOSIS))
					{
						YearMonth yearMonth = new YearMonth();
						try
						{
							yearMonth.setTime(System.currentTimeMillis()
									- (Long.parseLong(sides[1].trim()) * MILLIS_IN_DAY));
							yearMonth.setMonth((byte) (0));
						}
						catch (NumberFormatException numberFormatException)
						{
							badOperations.add(patient.getPrincipal() + ": \"" + operation + "\" ("
									+ numberFormatException.getMessage() + ')');
							continue;
						}
						patientHistory.getYearMonthHistory().put(BreastCancerCharacteristicCodes.DIAGNOSISBREAST,
								yearMonth);
						continue;
					}

					// Verify that operation is for procedure or therapy
					if ((sides[0].trim().length() == 0)
							|| ((sides[0].trim().charAt(0) != DYNAMICDATE_PROCEDURE) && (sides[0].trim().charAt(0) != DYNAMICDATE_THERAPY)))
					{
						badOperations.add(patient.getPrincipal() + ": \"" + operation + '"');
						continue;
					}
					// Parse started and (optional) completed dates
					YearMonth started = new YearMonth();
					YearMonth completed = null;
					String[] periods = sides[1].trim().split(DYNAMICDATE_SEPARATOR);
					try
					{
						started.setTime(System.currentTimeMillis()
								- (Long.parseLong(periods[0].trim()) * MILLIS_IN_DAY));
						if ((periods.length > 1) && (periods[1] != null) && (periods[1].trim().length() > 0))
						{
							completed = new YearMonth();
							completed.setTime(System.currentTimeMillis()
									- (Long.parseLong(periods[1].trim()) * MILLIS_IN_DAY));
						}
					}
					catch (NumberFormatException numberFormatException)
					{
						badOperations.add(patient.getPrincipal() + ": \"" + operation + "\" ("
								+ numberFormatException.getMessage() + ')');
						continue;
					}

					// Parse procedure's/therapy's codes
					String[] codeStrings = sides[0].substring(1).split(DYNAMICDATE_SEPARATOR);
					int[] codes = new int[codeStrings.length];
					for (int index = 0; index < codeStrings.length; index++)
						try
						{
							codes[index] = Integer.parseInt(codeStrings[index]);
						}
						catch (NumberFormatException numberFormatException)
						{
							badOperations.add(patient.getPrincipal() + ": \"" + operation + "\" ("
									+ numberFormatException.getMessage() + ')');
							continue operationLoop;
						}

					// Apply date(s) to each procedure whose kind, type and/or
					// location match all specified
					if (sides[0].charAt(0) == DYNAMICDATE_PROCEDURE)
					{
						procedureLoop: for (Procedure procedure : patientHistory.getProcedures())
							if (procedure != null)
							{
								for (int criterionIndex = 0; criterionIndex < codes.length; criterionIndex++)
									if (((procedure.getKind() == null) || (codes[criterionIndex] != procedure.getKind()
											.getCode()))
											&& ((procedure.getType() == null) || (codes[criterionIndex] != procedure
													.getType().getCode()))
											&& ((procedure.getLocation() == null) || (codes[criterionIndex] != procedure
													.getLocation().getCode())))
										continue procedureLoop;
								// Set dates
								procedure.setStarted(started);
								if (completed != null)
									procedure.setCompleted(completed);
							}
						continue;
					}

					// Apply date(s) to each therapy whose codes match all
					// specified
					therapyLoop: for (Therapy therapy : patientHistory.getTherapies())
						if (therapy != null)
						{
							for (int criterionIndex = 0; criterionIndex < codes.length; criterionIndex++)
							{
								// Look for specified code in both setting and
								// agents; incomplete and status are not matched
								boolean found = (therapy.getSetting() != null)
										&& (therapy.getSetting().getCode() == codes[criterionIndex]);
								if (!found)
									for (CharacteristicCode agent : therapy.getAgents())
										if ((agent != null) && (agent.getCode() == codes[criterionIndex]))
										{
											found = true;
											break;
										}
								if (!found)
									continue therapyLoop;
							}
							// Set dates
							therapy.setStarted(started);
							if (completed != null)
								therapy.setCompleted(completed);
						}
				}
			}
		}

		// Display bad operations to user
		Iterator<String> iterator = badOperations.iterator();
		if (iterator.hasNext())
		{
			String newline = System.getProperty("line.separator");
			String message = "Skipped the following invalid dynamic date(s):" + newline + iterator.next();
			while (iterator.hasNext())
				message += newline + iterator.next();
			JOptionPane.showMessageDialog(this, message, "Skipping invalid dynamic dates", JOptionPane.WARNING_MESSAGE);
		}
	}

	/**
	 * Update eligibility profiles with trial and patient data
	 */
	private void displayEligibilityProfiles()
	{
		if ((trial == null) || (trial.getId() == null) || (patients == null) || patients.isEmpty())
			return;
		// Get the eligibility profiles from a new matching engine
		LinkedList<Trial> list = new LinkedList<Trial>();
		list.add(trial);
		DeclarativeMatchingEngine matchingEngine = new DeclarativeMatchingEngine(list);
		LinkedList<EligibilityProfile> profiles = new LinkedList<EligibilityProfile>();
		for (UserPatient patient : patients)
		{
			// Postpend patient's description with diagnosis string
			// characteristic
			PatientHistory patientHistory = patient.getPatientHistory();
			patientHistory.setUserType(patient.getUsertype());
			
			String profileDescription = patientHistory.getString(BreastCancerCharacteristicCodes.CANCER_DIAGNOSIS);
			if (profileDescription == null)
				profileDescription = patient.getPrincipal();
			else
				profileDescription = patient.getPrincipal() + ": " + profileDescription;

			if (patient.isAvatarProfile())
			{
				for (CharacteristicCode stage: patient.getAvatarHistory().getAvatarStageCharacteristics())
				{
					// set up a descriptive for this avatar, this stage
					String currentAvatarProfileDescription = profileDescription + " (" + StageMatchable.StageDescriptive.get(stage) + ")";
					
					// inject the stage for this Eligibility Profile
					patientHistory.injectStageHistory(stage);
					
					EligibilityProfile avatarEligibilityProfile = matchingEngine.getEligibilityProfile(trial.getId(), patientHistory,
							currentAvatarProfileDescription);
					if (avatarEligibilityProfile != null)
					{	
						profiles.add(avatarEligibilityProfile);
					}	
				}
			}
			else
			{	
				EligibilityProfile eligibilityProfile = matchingEngine.getEligibilityProfile(trial.getId(), patientHistory,	profileDescription);
				if (eligibilityProfile != null)
				{	
					profiles.add(eligibilityProfile);
				}
			}	
		}
		eligibilityProfilePanel.setProfiles(profiles.iterator());
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -2074649087521120923L;
}

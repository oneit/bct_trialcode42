/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;

/**
 * Arrange's a container's components in one column with particular width and
 * indentation features. Width calculations take all components -- including
 * invisible ones -- into account so that when invisible components are made
 * visible the container's width is not resized. The first component ("title")
 * and -- optionally -- the other components are layed out as wide as the
 * column. An optional indentation applies a left offset to either the first
 * component or to all of the others. Note: This layout manager does not cache
 * any size information because it is cached in <code>Component</code>.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public class WideTitledColumnLayoutManager implements LayoutManager
{

	/**
	 * Left inset. If negative it applies only to the first component; if
	 * positive it applies to all components except the first.
	 */
	private final int indent;

	/**
	 * Whether to layout each component at the container's width or just the
	 * first ("title") component.
	 */
	private final boolean allComponentsHaveColumnWidth;

	/**
	 * Configures the column with no indentation and layout all components at
	 * the container's width.
	 */
	public WideTitledColumnLayoutManager()
	{
		this(true, 0);
	}

	/**
	 * Configures the column with indentation where only the first "title"
	 * component is layed out at the container's width.
	 * 
	 * @param indent
	 *            if negative apply a left inset only to the first component; if
	 *            positive apply a left inset to all components except the
	 *            first.
	 */
	public WideTitledColumnLayoutManager(int indent)
	{
		this(false, indent);
	}

	/**
	 * Configures the column with indentation and whether to apply the full
	 * width to all components
	 * 
	 * @param allComponentsHaveColumnWidth
	 *            if <code>true</code> then lay out all components at the
	 *            container's width; if <code>false</code> then lay out the
	 *            first "title" component at container's width and the other
	 *            components at their preferred width
	 * @param indent
	 *            if negative apply a left inset only to the first component; if
	 *            positive apply a left inset to all components except the
	 *            first.
	 */
	public WideTitledColumnLayoutManager(boolean allComponentsHaveColumnWidth, int indent)
	{
		this.allComponentsHaveColumnWidth = allComponentsHaveColumnWidth;
		this.indent = indent;
	}

	/**
	 * Places all components in a column using the container's width and
	 * optional indentation. If <code>allComponentsHaveColumnWidth</code> is
	 * <code>true</code> then all of the visible components have the container's
	 * width; otherwise only the first "title" component has the container
	 * width. If <code>indent</code> is negative then it is applied to the first
	 * "title" component instead of the other components.
	 * 
	 * @see java.awt.LayoutManager#layoutContainer(java.awt.Container)
	 */
	public void layoutContainer(Container container)
	{
		// If container is empty then do nothing
		int count = (container == null) ? 0 : container.getComponentCount();
		if (count == 0)
			return;
		// Lay out title component across entire width; apply indent if negative
		Insets insets = container.getInsets();
		int x = insets.left;
		int columnWidth = container.getWidth() - (insets.left + insets.right);
		int y = insets.top;
		Component component = container.getComponent(0);
		Dimension preferredSize = component.getPreferredSize();
		component.setLocation(indent < 0 ? (x - indent) : x, y);
		component.setSize(indent < 0 ? columnWidth + indent : columnWidth, preferredSize.height);
		y += preferredSize.height;

		// Lay out each visible component underneath
		if (indent > 0)
		{
			x += indent;
			columnWidth -= indent;
		}
		for (int index = 1; index < count; index++)
		{
			component = container.getComponent(index);
			if (!component.isVisible())
				continue;
			component.setLocation(x, y);
			// Use either the preferred width or column width
			preferredSize = component.getPreferredSize();
			component.setSize(allComponentsHaveColumnWidth ? new Dimension(columnWidth, preferredSize.height)
					: preferredSize);
			y += preferredSize.height;
		}
	}

	/**
	 * Returns the column's widest width (including insets and indentation)
	 * taking all components -- including invisible ones -- into account, and
	 * sums the total height of the visible components. If <code>indent</code>
	 * is negative then it is applied to the first "title" component instead of
	 * the other components.
	 * 
	 * @return the widest width and the sum of the visible heights
	 * @param container
	 *            the container to get the size for
	 * @param preferred
	 *            if <code>true</code> then use components' preferred sizes; if
	 *            <code>false</code> then use minimum size
	 * @see java.awt.LayoutManager#minimumLayoutSize(java.awt.Container)
	 */
	private Dimension layoutSize(Container container, boolean preferred)
	{

		// If container is empty then return insets
		if (container == null)
			return new Dimension(0, 0);
		int count = container.getComponentCount();
		Insets insets = container.getInsets();
		if (count == 0)
			return new Dimension(insets.left + insets.right, insets.top + insets.bottom);

		// For non-title components, find widest component and sum the visible
		// heights
		Dimension size = preferred ? container.getComponent(0).getPreferredSize() : container.getComponent(0)
				.getMinimumSize();
		int nonTitleWidth = 0;
		for (int index = 1; index < count; index++)
		{
			Dimension componentSize = preferred ? container.getComponent(index).getPreferredSize() : container
					.getComponent(index).getMinimumSize();
			nonTitleWidth = Math.max(nonTitleWidth, componentSize.width);
			if (container.getComponent(index).isVisible())
				size.height += componentSize.height;
		}

		// Apply negative indent to title's width instead of nonTitle width and
		// apply indents
		size.width = insets.left
				+ insets.right
				+ (indent < 0 ? Math.max(size.width - indent, nonTitleWidth) : Math.max(size.width, nonTitleWidth
						+ indent));
		size.height += insets.top + insets.bottom;
		return size;
	}

	/**
	 * Returns the column's widest preferred width (including insets and
	 * indentation) taking all components -- including invisible ones -- into
	 * account, and sums the total preferred height of the visible components.
	 * If <code>indent</code> is negative then it is applied to the first
	 * "title" component instead of the other components.
	 * 
	 * @return the widest preferred width and the sum of the visible preferred
	 *         heights
	 * @param container
	 *            the container to get the preferred size for
	 * @see java.awt.LayoutManager#preferredLayoutSize(java.awt.Container)
	 */
	public Dimension preferredLayoutSize(Container container)
	{
		// Returns overloaded method
		return layoutSize(container, true);
	}

	/**
	 * Returns the column's widest minimum width (including insets and
	 * indentation) taking all components -- including invisible ones -- into
	 * account, and sums the total minimum height of the visible components. If
	 * <code>indent</code> is negative then it is applied to the first "title"
	 * component instead of the other components.
	 * 
	 * @return the widest minimum width and the sum of the visible minimum
	 *         heights
	 * @param container
	 *            the container to get the minimum size for
	 * @see java.awt.LayoutManager#minimumLayoutSize(java.awt.Container)
	 */
	public Dimension minimumLayoutSize(Container container)
	{
		// Returns overloaded method
		return layoutSize(container, false);
	}

	/**
	 * Not implemented as this manager treats all components (except the first)
	 * equally.
	 * 
	 * @param name
	 *            (not used)
	 * @param component
	 *            (not used)
	 * @see java.awt.LayoutManager#addLayoutComponent(java.lang.String,
	 *      java.awt.Component)
	 */
	public void addLayoutComponent(String name, Component component)
	{
	}

	/**
	 * Not implemented as this manager treats all components (except the first)
	 * equally.
	 * 
	 * @param component
	 *            (not used)
	 * @see java.awt.LayoutManager#removeLayoutComponent(java.awt.Component)
	 */
	public void removeLayoutComponent(Component component)
	{
	}
}

/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.Criteria;
import org.quantumleaphealth.model.trial.Criterion;
import org.quantumleaphealth.model.trial.Criteria.Operator;

/**
 * A read-only panel that represents a criterion or set of criteria.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
abstract class CriterionPanel extends JPanel
{

	/**
	 * The criterion/criteria that this panel represents
	 */
	protected Criterion criterion;

	/**
	 * The list that contains the <code>criterion</code>
	 */
	protected final List<Criterion> parent;

	/**
	 * Listens to this component when it is rolled over and clicked
	 */
	protected final MouseListener listener;

	/**
	 * Gap between label and panel components
	 */
	private static final int GAP = 5;

	/**
	 * Creates a BorderLayout panel with and optionally listens for mouse
	 * events.
	 * 
	 * @param criterion
	 *            the logic that this panel represents
	 * @param parent
	 *            contains the <code>criterion</code> or <code>null</code> if no
	 *            parent
	 * @param listener
	 *            listener for this component or <code>null</code> if this
	 *            component should create its own listener
	 */
	private CriterionPanel(Criterion criterion, List<Criterion> parent, MouseListener listener)
	{
		super(new BorderLayout(GAP, 0));
		this.criterion = criterion;
		this.parent = parent;
		// Create its own highlighter not provided
		if (listener == null)
			this.listener = new Highlighter(this);
		else
		{
			this.listener = listener;
			addMouseListener(listener);
		}
	}

	/**
	 * Returns this panel's criterion. Note that this method is redundant since
	 * <code>criterion</code> is <code>protected</code>.
	 * 
	 * @return this panel's criterion
	 */
	protected Criterion getCriterion()
	{
		return criterion;
	}

	/**
	 * Sets the foreground of itself and all of its children
	 * 
	 * @param foreground
	 *            foreground color to set
	 */
	@Override
	public void setForeground(Color foreground)
	{
		super.setForeground(foreground);
		for (Component component : getComponents())
			component.setForeground(foreground);
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 3079525659027234227L;

	/**
	 * Represents a criterion with a text area
	 */
	static class AbstractCriterionPanel extends CriterionPanel
	{
		/**
		 * Holds the description of the criterion
		 */
		private final JTextArea description;
		/**
		 * Looks up the description of the criterion
		 */
		private final CriteriaUserInterface userInterface;

		/**
		 * Set the criterion and lays out its comparator and requirements in a
		 * text pane.
		 * 
		 * @param abstractCriterion
		 *            the criterion that this panel represents
		 * @param parent
		 *            contains the <code>criterion</code> or <code>null</code>
		 *            if no parent
		 * @param listener
		 *            listener for this component or <code>null</code> if this
		 *            component should create its own listener
		 * @param userInterface
		 *            looks up criterion descriptions
		 */
		private AbstractCriterionPanel(AbstractCriterion abstractCriterion, List<Criterion> parent,
				MouseListener listener, CriteriaUserInterface userInterface)
		{
			super(abstractCriterion, parent, listener);
			this.userInterface = userInterface;
			if (abstractCriterion == null)
				throw new IllegalArgumentException("criterion not specified");
			// Text area looks like a word-wrapping label
			description = Common.createWordWrapTextArea(userInterface.getDescription(abstractCriterion));
			// TextArea is opaque to parent's mouse events, so add a listener to
			// it
			description.addMouseListener(this.listener);
			add(description, BorderLayout.CENTER);
		}

		/**
		 * Updates the <code>criterion</code> that this panel represents. This
		 * stores the new criterion both in the panel and in its
		 * <code>Criteria</code> parent.
		 * 
		 * @param abstractCriterion
		 *            the criterion to update
		 */
		protected void setCriterion(AbstractCriterion abstractCriterion)
		{
			// Store the new criterion first in the parent and then in panel
			parent.set(parent.indexOf(criterion), abstractCriterion);
			this.criterion = abstractCriterion;
			description.setText(userInterface.getDescription(abstractCriterion));
			revalidate();
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 7176656224586173599L;
	}

	/**
	 * Represents a criteria with a wide column of its children
	 */
	static class CriteriaPanel extends CriterionPanel
	{

		/**
		 * Set the criteria and lays out its children connected with its
		 * operator in a wide column
		 * 
		 * @param criteria
		 *            the criteria that this panel represents
		 * @param parent
		 *            contains the <code>criteria</code> or <code>null</code> if
		 *            no parent
		 * @param listener
		 *            listener for this component or <code>null</code> if this
		 *            component should create its own listener
		 * @param propertyChangeListener
		 *            optional listener for selection events
		 * @param userInterface
		 *            looks up criterion description
		 */
		private CriteriaPanel(Criteria criteria, List<Criterion> parent, MouseListener listener,
				PropertyChangeListener propertyChangeListener, CriteriaUserInterface userInterface)
		{
			// Superclass sets up BorderLayout
			super(criteria, parent, listener);
			// Ensure enough children and get first child
			if ((criteria == null) || (criteria.getOperator() == null) || (criteria.getCount() == 0))
				throw new IllegalArgumentException("Operator or children not specified");
			if ((criteria.getCount() < 2) || ((criteria.getCount() > 2) && (criteria.getOperator() == Operator.IF)))
				throw new IllegalArgumentException("Operator " + criteria.getOperator() + " not compatible with "
						+ criteria.getCount() + " size");
			CriterionPanel child = createCriterionPanel(criteria.getChildren().get(0), criteria.getChildren(),
					listener, propertyChangeListener, userInterface);
			child.addPropertyChangeListener(Highlighter.SELECTED, propertyChangeListener);

			// Stretch children across entire opaque column width
			// TODO: Add special border when deep nesting (parent != null)
			JPanel panel = new JPanel(new WideTitledColumnLayoutManager());
			panel.setOpaque(false);
			add(panel, BorderLayout.CENTER);

			// Each IF/THEN child is placed next to a IF/THEN label on a row
			// stretched across the column's width
			if (criteria.getOperator() == Operator.IF)
			{
				// Add first child with "IF" label
				JPanel row = new JPanel(new BorderLayout(GAP, 0));
				row.setOpaque(false);
				JLabel label = new JLabel(Common.getString(Common.PREFIX_OPERATOR + Operator.IF.toString()
						+ Common.SEPARATOR + Common.SUFFIX_IF));
				label.setVerticalAlignment(JLabel.TOP);
				row.add(label, BorderLayout.WEST);
				row.add(child, BorderLayout.CENTER);
				panel.add(row);
				// Add second child with "THEN" label
				child = createCriterionPanel(criteria.getChildren().get(1), criteria.getChildren(), listener,
						propertyChangeListener, userInterface);
				child.addPropertyChangeListener(Highlighter.SELECTED, propertyChangeListener);
				row = new JPanel(new BorderLayout(GAP, 0));
				row.setOpaque(false);
				label = new JLabel(Common.getString(Common.PREFIX_OPERATOR + Operator.IF.toString() + Common.SEPARATOR
						+ Common.SUFFIX_THEN));
				label.setVerticalAlignment(JLabel.TOP);
				row.add(label, BorderLayout.WEST);
				row.add(child, BorderLayout.CENTER);
				panel.add(row);
				return;
			}

			// For other criteria operators, add children separated by a
			// conjunction
			panel.add(child);
			String conjunction = Common.getString(Common.PREFIX_OPERATOR + criteria.getOperator().toString());
			for (int index = 1; index < criteria.getCount(); index++)
			{
				panel.add(new JLabel(conjunction));
				child = createCriterionPanel(criteria.getChildren().get(index), criteria.getChildren(), listener,
						propertyChangeListener, userInterface);
				child.addPropertyChangeListener(Highlighter.SELECTED, propertyChangeListener);
				panel.add(child);
			}
		}

		/**
		 * Sets the foreground of itself and its child labels
		 * 
		 * @param foreground
		 *            foreground color to set
		 */
		@Override
		public void setForeground(Color foreground)
		{
			super.setForeground(foreground);
			if (getComponentCount() == 0)
				return;
			// The last component added is the panel of children
			JPanel panel = (JPanel) (getComponent(getComponentCount() - 1));
			switch (((Criteria) (getCriterion())).getOperator())
			{
			case IF:
				// Set IF and THEN labels
				((JPanel) (panel.getComponent(0))).getComponent(0).setForeground(foreground);
				((JPanel) (panel.getComponent(1))).getComponent(0).setForeground(foreground);
				break;
			default:
				// Set conjunction labels
				for (int index = 1; index < panel.getComponentCount(); index += 2)
					panel.getComponent(index).setForeground(foreground);
			}
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 2584443846623718508L;
	}

	/**
	 * Returns the appropriate subclass after adding the optional listener
	 * 
	 * @return a <code>AbstractCriterionUserInterface</code> or
	 *         <code>CriteriaPanel</code>
	 * @param criterion
	 *            the criterion that this panel represents
	 * @param parent
	 *            contains the <code>criterion</code> or <code>null</code> if no
	 *            parent
	 * @param listener
	 *            listener for this component or <code>null</code> if this
	 *            component should create its own listener
	 * @param propertyChangeListener
	 *            optional listener for selection events
	 * @param userInterface
	 *            looks up criterion descriptions
	 */
	static CriterionPanel createCriterionPanel(Criterion criterion, List<Criterion> parent, MouseListener listener,
			PropertyChangeListener propertyChangeListener, CriteriaUserInterface userInterface)
	{
		CriterionPanel criterionPanel = null;
		if (criterion instanceof AbstractCriterion)
			criterionPanel = new AbstractCriterionPanel((AbstractCriterion) (criterion), parent, listener,
					userInterface);
		else if (criterion instanceof Criteria)
			criterionPanel = new CriteriaPanel((Criteria) (criterion), parent, listener, propertyChangeListener,
					userInterface);
		else
			throw new IllegalArgumentException("Cannot support criterion " + criterion);
		criterionPanel.addPropertyChangeListener(Highlighter.SELECTED, propertyChangeListener);
		return criterionPanel;
	}

	/**
	 * Uses enter/exit and click mouse events to highlight a Swing component.
	 */
	private class Highlighter extends MouseAdapter
	{

		/**
		 * Component that this object is highlighting
		 */
		private final JComponent component;

		/**
		 * Whether this component is currently selected
		 */
		private boolean selected = false;

		/**
		 * When the last click event occurred for this component or
		 * <code>0</code> if never clicked.
		 */
		private long when = 0;

		/**
		 * Name of selected property
		 */
		protected static final String SELECTED = "SELECTED";

		/**
		 * Sets the default border and listens for mouse events
		 * 
		 * @param component
		 */
		Highlighter(JComponent component)
		{
			this.component = component;
			component.setBorder(Common.NONROLLOVER);
			component.addMouseListener(this);
		}

		/**
		 * Toggles the selection of the component by changing its background
		 * color and recording selection time. This method fires a
		 * <code>boolean</code> property change event to listeners.
		 * 
		 * @param mouseEvent
		 *            the mouse-click event
		 */
		@Override
		public void mouseClicked(MouseEvent mouseEvent)
		{
			// Ignore duplicate events that occur when the component's child is
			// also
			// registered with highlighter
			if (mouseEvent.getWhen() == when)
				return;
			selected = !selected;
			component.setForeground(selected ? Common.FOREGROUND_SELECTED : Common.FOREGROUND);
			component.setBackground(selected ? Common.BACKGROUND_SELECTED : Common.BACKGROUND);
			component.firePropertyChange(SELECTED, !selected, selected);
		}

		/**
		 * Highlight the component by coloring its border
		 * 
		 * @param mouseEvent
		 *            the mouse-enter event
		 */
		@Override
		public void mouseEntered(MouseEvent mouseEvent)
		{
			component.setBorder(Common.ROLLOVER);
		}

		/**
		 * Unhighlight the component by coloring its border to its background
		 * 
		 * @param mouseEvent
		 *            the mouse-exit event
		 */
		@Override
		public void mouseExited(MouseEvent mouseEvent)
		{
			component.setBorder(Common.NONROLLOVER);
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 7311635693878859031L;
	}
}
package org.quantumleaphealth.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

/**
 * Data tuple for the categories displayed under Secondary Categories. The short
 * is necessary to preserve the actual internal category id regardless of where
 * in the available or selected lists the entry may appear.
 * 
 * @author wgweis
 * 
 */
class CategoryDataTuple implements Comparable<CategoryDataTuple>
{
	private short category;
	private String categoryLabel;

	public CategoryDataTuple(short category, String categoryLabel)
	{
		this.setCategory(category);
		this.setCategoryLabel(categoryLabel);
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(short category)
	{
		this.category = category;
	}

	/**
	 * @return the category
	 */
	public short getCategory()
	{
		return category;
	}

	/**
	 * @param categoryLabel
	 *            the categoryLabel to set
	 */
	public void setCategoryLabel(String categoryLabel)
	{
		this.categoryLabel = categoryLabel;
	}

	/**
	 * @return the categoryLabel
	 */
	public String getCategoryLabel()
	{
		return categoryLabel;
	}

	public String toString()
	{
		return categoryLabel;
	}

	@Override
	public int compareTo(CategoryDataTuple other)
	{
		return this.getCategoryLabel().compareTo(
				((CategoryDataTuple) other).getCategoryLabel());
	}
}

class MutationDataTuple implements Comparable<MutationDataTuple>
{
	private Short mutation;
	private String mutationLabel;

	public MutationDataTuple(Short mutation, String mutationLabel)
	{
		this.setMutation(mutation);
		this.setMutationLabel(mutationLabel);
	}

	/**
	 * @param mutation
	 *            the mutation to set
	 */
	public void setMutation(Short mutation)
	{
		this.mutation = mutation;
	}

	/**
	 * @return the mutation
	 */
	public Short getMutation()
	{
		return mutation;
	}

	/**
	 * @param mutationLabel
	 *            the mutationLabel to set
	 */
	public void setMutationLabel(String mutationLabel)
	{
		this.mutationLabel = mutationLabel;
	}

	/**
	 * @return the mutationLabel
	 */
	public String getMutationLabel()
	{
		return mutationLabel;
	}

	public String toString()
	{
		return mutationLabel;
	}

	@Override
	public int compareTo(MutationDataTuple other)
	{
		return this.getMutationLabel().compareTo(
				((MutationDataTuple) other).getMutationLabel());
	}
}

/**
 * the remaining two classes in this source file were taken from
 * http://www.java2s.com/Tutorial/Java/0240__Swing/DualListBoxSample.htm
 * 
 * I added getters for the source and destination models to allow access to the
 * existing lists backing these models.
 * 
 * @author wgweis
 * 
 */
class SortedListModel extends AbstractListModel
{
	private static final long serialVersionUID = 1475669753628767199L;

	SortedSet<Object> model;

	public SortedListModel()
	{
		model = new TreeSet<Object>();
	}

	public int getSize()
	{
		return model.size();
	}

	public Object getElementAt(int index)
	{
		return model.toArray()[index];
	}

	public void add(Object element)
	{
		if (model.add(element))
		{
			fireContentsChanged(this, 0, getSize());
		}
	}

	public void addAll(Object elements[])
	{
		Collection<Object> c = Arrays.asList(elements);
		model.addAll(c);
		fireContentsChanged(this, 0, getSize());
	}

	public void clear()
	{
		model.clear();
		fireContentsChanged(this, 0, getSize());
	}

	public boolean contains(Object element)
	{
		return model.contains(element);
	}

	public Object firstElement()
	{
		return model.first();
	}

	public Iterator iterator()
	{
		return model.iterator();
	}

	public Object lastElement()
	{
		return model.last();
	}

	public boolean removeElement(Object element)
	{
		boolean removed = model.remove(element);
		if (removed)
		{
			fireContentsChanged(this, 0, getSize());
		}
		return removed;
	}
}

public class DualListBox extends JPanel
{
	private static final long serialVersionUID = 1281989918765748700L;

	private JList sourceList;

	private SortedListModel sourceListModel;

	private JList destList;

	private SortedListModel destListModel;

	private JButton addButton;

	private JButton removeButton;

	public DualListBox()
	{
		initScreen();
	}
	
	public DualListBox(String title)
	{
		initScreen(title);
	}

	/**
	 * @return the sourceListModel
	 */
	public SortedListModel getSourceListModel()
	{
		return sourceListModel;
	}

	/**
	 * @return the destListModel
	 */
	public SortedListModel getDestListModel()
	{
		return destListModel;
	}

	public void clearSourceListModel()
	{
		sourceListModel.clear();
	}

	public void clearDestinationListModel()
	{
		destListModel.clear();
	}

	public void addSourceElements(ListModel newValue)
	{
		fillListModel(sourceListModel, newValue);
	}

	public void setSourceElements(ListModel newValue)
	{
		clearSourceListModel();
		addSourceElements(newValue);
	}

	public void addDestinationElements(ListModel newValue)
	{
		fillListModel(destListModel, newValue);
	}

	private void fillListModel(SortedListModel model, ListModel newValues)
	{
		int size = newValues.getSize();
		for (int i = 0; i < size; i++)
		{
			model.add(newValues.getElementAt(i));
		}
	}

	public void addSourceElements(Object newValue[])
	{
		fillListModel(sourceListModel, newValue);
	}

	public void setSourceElements(Object newValue[])
	{
		clearSourceListModel();
		addSourceElements(newValue);
	}

	public void addDestinationElements(Object newValue[])
	{
		fillListModel(destListModel, newValue);
	}

	private void fillListModel(SortedListModel model, Object newValues[])
	{
		model.addAll(newValues);
	}

	private void clearSourceSelected()
	{
		Object selected[] = sourceList.getSelectedValues();
		for (int i = selected.length - 1; i >= 0; --i)
		{
			sourceListModel.removeElement(selected[i]);
		}
		sourceList.getSelectionModel().clearSelection();
	}

	private void clearDestinationSelected()
	{
		Object selected[] = destList.getSelectedValues();
		for (int i = selected.length - 1; i >= 0; --i)
		{
			destListModel.removeElement(selected[i]);
		}
		destList.getSelectionModel().clearSelection();
	}

	private void initScreen()
	{
		setLayout(new GridLayout(0, 2));
		sourceListModel = new SortedListModel();
		sourceList = new JList(sourceListModel);

		addButton = new JButton(">>");
		addButton.addActionListener(new AddListener());
		removeButton = new JButton("<<");
		removeButton.addActionListener(new RemoveListener());

		destListModel = new SortedListModel();
		destList = new JList(destListModel);

		JPanel leftPanel = new JPanel(new BorderLayout());
		leftPanel.add(new JLabel("Available Categories:"), BorderLayout.NORTH);
		leftPanel.add(new JScrollPane(sourceList), BorderLayout.CENTER);
		leftPanel.add(addButton, BorderLayout.SOUTH);

		JPanel rightPanel = new JPanel(new BorderLayout());

		rightPanel.add(new JLabel("Selected Categories:"), BorderLayout.NORTH);
		rightPanel.add(new JScrollPane(destList), BorderLayout.CENTER);
		rightPanel.add(removeButton, BorderLayout.SOUTH);

		add(leftPanel);
		add(rightPanel);
	}
	
	private void initScreen(String title)
	{
		setLayout(new GridLayout(0, 2));
		sourceListModel = new SortedListModel();
		sourceList = new JList(sourceListModel);

		addButton = new JButton(">>");
		addButton.addActionListener(new AddListener());
		removeButton = new JButton("<<");
		removeButton.addActionListener(new RemoveListener());

		destListModel = new SortedListModel();
		destList = new JList(destListModel);
		
		sourceList.setVisibleRowCount(3);
		destList.setVisibleRowCount(3);
		//sourceList.setFixedCellWidth(100);
		JPanel leftPanel = new JPanel(new BorderLayout());
		leftPanel.add(new JLabel("Available "+title+":"), BorderLayout.NORTH);
		leftPanel.add(new JScrollPane(sourceList), BorderLayout.CENTER);
		leftPanel.add(addButton, BorderLayout.SOUTH);

		JPanel rightPanel = new JPanel(new BorderLayout());

		rightPanel.add(new JLabel("Selected "+title+":"), BorderLayout.NORTH);
		rightPanel.add(new JScrollPane(destList), BorderLayout.CENTER);
		rightPanel.add(removeButton, BorderLayout.SOUTH);

		add(leftPanel);
		add(rightPanel);
	}

	private class AddListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Object selected[] = sourceList.getSelectedValues();
			addDestinationElements(selected);
			clearSourceSelected();
		}
	}

	private class RemoveListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Object selected[] = destList.getSelectedValues();
			addSourceElements(selected);
			clearDestinationSelected();
		}
	}

	/**
	 * For Testing purposes only
	 * @param args NA
	 */
	public static void main(String args[])
	{
		JFrame frame = new JFrame("Dual List Box Tester");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		DualListBox dual = new DualListBox();
		String[] sourceElementLabels = { "One", "Two", "Three", "Four", "Five",
				"Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve",
				"Thirteen", "Fourteen" };
		List<CategoryDataTuple> sourceElements = new ArrayList<CategoryDataTuple>();
		for (int i = 0; i < sourceElementLabels.length; i++)
		{
			sourceElements.add(new CategoryDataTuple((short) i,
					sourceElementLabels[i]));
		}

		dual.addSourceElements(sourceElements.toArray());
		frame.add(dual, BorderLayout.CENTER);
		frame.setSize(400, 300);
		frame.setVisible(true);
	}
}
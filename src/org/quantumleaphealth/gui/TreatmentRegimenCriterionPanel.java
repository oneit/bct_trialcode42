/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.TreatmentRegimenCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.COMPLETED;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CONCURRENT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MONTH;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MOSTRECENT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PROGRESSING;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.SETTING;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.WEEK;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.YEAR;

/**
 * Displays a treatment regimen criterion.
 * 
 * @author Tom Bechtold
 * @version 2008-06-24
 */
class TreatmentRegimenCriterionPanel extends JPanel implements AbstractCriterionUserInterface
{

	/**
	 * Contains comparators
	 */
	private final JComboBox comparatorComboBox = new JComboBox();
	/**
	 * Contains the count
	 */
	private final JComboBox countComboBox = new JComboBox();
	/**
	 * Contains the prior/concurrent comparator
	 */
	private final JComboBox currentComboBox = new JComboBox();
	/**
	 * Contains recent malignancy comparator
	 */
	private final JCheckBox recentMalignancyCheckBox;
	/**
	 * Contains progression required comparator
	 */
	private final JCheckBox progressionRequiredCheckBox;
	/**
	 * Contains settings comparator
	 */
	private final AssociativeCriterionUserInterface settingsPanel;
	/**
	 * Contains agents and their classes
	 */
	private final AssociativeCriterionUserInterface agentsPanel;
	/**
	 * Contains the threshold number or <code>null</code> if the threshold does
	 * not apply
	 */
	private final JTextField threshold;
	/**
	 * Contains the threshold unit of measure or <code>null</code> if the
	 * threshold does not apply
	 */
	private final JComboBox thresholdUnitOfMeasure;

	/**
	 * Maximum number for <code>count</code> field
	 */
	private static final int MAXIMUM_COUNT = 4;

	/**
	 * Adds the three combobox comparators to one row, the threshold text field
	 * and combobox to one row, the two checkboxes to another row, the settings
	 * checkbox group to another row and classes/agents to the bottom section.
	 * 
	 * @param parent
	 *            the characteristic representing the type of therapy
	 * @param settings
	 *            the settings for which the therapy type is given
	 * @param agents
	 *            the list of agents (drugs)
	 * @param classes
	 *            the aggregators of the agents
	 * @param isThresholdTime
	 *            whether a threshold specified using units of time applies
	 * @throws IllegalArgumentException
	 *             if <code>parent</code> is <code>null</code>
	 */
	public TreatmentRegimenCriterionPanel(CharacteristicCode parent, CharacteristicCode[] settings,
			CharacteristicCode[] agents, Map<CharacteristicCode, CharacteristicCode[]> classes, boolean isThresholdTime)
			throws IllegalArgumentException
	{
		// Lay out panel as a vertical box
		super(null);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		if (parent == null)
			throw new IllegalArgumentException("must specify parent characteristic");
		setName(Common.getString(parent.toString()));

		// Comparator combobox supports no, any, minimum, maximum
		comparatorComboBox.addItem("No");
		comparatorComboBox.addItem("Any");
		comparatorComboBox.addItem("No more than");
		comparatorComboBox.addItem("At least");
		// Count combobox goes up from 0 inclusive
		for (int index = 0; index <= MAXIMUM_COUNT; index++)
			countComboBox.addItem(Integer.toString(index));
		// Initially disable count
		comparatorComboBox.addItemListener(new ItemListener()
		{
			/**
			 * Selecting comparator combo focuses on count; any/none disables it
			 * 
			 * @param itemEvent
			 *            the event when the comparator combobox is selected
			 */
			public void itemStateChanged(ItemEvent itemEvent)
			{
				if (comparatorComboBox.getSelectedIndex() < 2)
				{
					countComboBox.setSelectedIndex(0);
					countComboBox.setEnabled(false);
				}
				else
				{
					countComboBox.setEnabled(true);
					countComboBox.requestFocusInWindow();
				}
			}
		});
		// Current combobox supports both prior and concurrent fields
		currentComboBox.addItem("prior or concurrent");
		currentComboBox.addItem("prior");
		currentComboBox.addItem("concurrent");
		// Place the comparator combobox, count combobox, label and progression
		// on first row
		JPanel row = new JPanel(new FlowLayout(FlowLayout.LEADING));
		row.setAlignmentX(LEFT_ALIGNMENT);
		row.add(comparatorComboBox);
		row.add(countComboBox);
		row.add(currentComboBox);
		add(row);

		// Place the threshold text field and combo box on the (optional) second
		// row
		threshold = isThresholdTime ? new JTextField(4) : null;
		thresholdUnitOfMeasure = isThresholdTime ? new JComboBox() : null;
		if (isThresholdTime)
		{
			row = new JPanel(new FlowLayout(FlowLayout.LEADING));
			row.setAlignmentX(LEFT_ALIGNMENT);
			row.add(new JLabel("of at least"));
			row.add(threshold);
			thresholdUnitOfMeasure.addItem("weeks");
			thresholdUnitOfMeasure.addItem("months");
			thresholdUnitOfMeasure.addItem("years");
			row.add(thresholdUnitOfMeasure);
			add(row);
		}

		// Place the recentMalignancy and progressionRequired check boxes on the
		// third row
		row = new JPanel(new FlowLayout(FlowLayout.LEADING));
		row.setAlignmentX(LEFT_ALIGNMENT);
		recentMalignancyCheckBox = new JCheckBox("for recent malignancy");
		row.add(recentMalignancyCheckBox);
		progressionRequiredCheckBox = new JCheckBox("with progression");
		row.add(progressionRequiredCheckBox);
		add(row);

		// Place the settings choices and agents below
		settingsPanel = new AssociativeCriterionUserInterface(SETTING, settings, null, true, false);
		settingsPanel.setAlignmentX(LEFT_ALIGNMENT);
		add(settingsPanel);
		agentsPanel = new AssociativeCriterionUserInterface(parent, agents, classes, true, true);
		agentsPanel.setAlignmentX(LEFT_ALIGNMENT);
		add(agentsPanel);
	}

	/**
	 * Set this component's visibility and clear the input fields
	 * 
	 * @param active
	 *            <code>true</code> to activate, <code>false</code> to
	 *            deactivate
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#setActive(boolean)
	 */
	public void setActive(boolean active)
	{
		// Activation focuses on current combobox; deactivation resets all
		// fields
		agentsPanel.setActive(active);
		settingsPanel.setActive(active);
		setVisible(active);
		if (active)
			comparatorComboBox.requestFocusInWindow();
		else
			resetFields();
	}

	/**
	 * Resets all input fields. This method sets each combo box to its first
	 * choice and disables the count combo box since comparator is "No".
	 */
	private void resetFields()
	{
		comparatorComboBox.setSelectedIndex(0);
		countComboBox.setSelectedIndex(0);
		countComboBox.setEnabled(false);
		currentComboBox.setSelectedIndex(0);
		if (threshold != null)
		{
			threshold.setText(null);
			thresholdUnitOfMeasure.setSelectedIndex(0);
		}
		recentMalignancyCheckBox.setSelected(false);
		progressionRequiredCheckBox.setSelected(false);
	}

	/**
	 * Returns a new criterion that contains the comparator and requirement from
	 * the panel's input elements. Special cases:
	 * <ul>
	 * <li>"any": minimum of 1</li>
	 * <li>"none": maximum of 0</li>
	 * </ul>
	 * 
	 * @return a new criterion
	 * @throws IllegalStateException
	 *             if there is not enough information to create a criterion
	 */
	public AbstractCriterion getCriterion() throws IllegalStateException
	{
		// If the location panel does not return anything then return null
		AbstractCriterion abstractAgents = agentsPanel.getCriterion();
		AbstractCriterion abstractSettings = settingsPanel.getCriterion();
		if (!isVisible() || (abstractAgents == null) || !(abstractAgents instanceof AssociativeCriterion)
				|| (abstractSettings == null) || !(abstractSettings instanceof AssociativeCriterion))
			return null;
		// Validate the type; minimum of zero does not make sense
		if ((comparatorComboBox.getSelectedIndex() == 3) && (countComboBox.getSelectedIndex() == 0))
			throw new IllegalStateException("Minimum of zero regimens does not make sense");

		// Copy the fields from the agents and settings subpanels
		TreatmentRegimenCriterion criterion = new TreatmentRegimenCriterion();
		AssociativeCriterion agents = (AssociativeCriterion) abstractAgents;
		criterion.setParent(agents.getParent());
		criterion.setRequirement(agents.getRequirement());
		criterion.setSetOperation(agents.getSetOperation());
		criterion.setSettings(((AssociativeCriterion) (abstractSettings)).getRequirement());

		// Minimum/maximum (invert) and limit is found in the first two combo
		// boxes
		int index = comparatorComboBox.getSelectedIndex();
		// Invert first because inverting changes the count
		if ((index == 0) || (index == 2))
			criterion.invert();
		if ((index == 2) || (index == 3))
			criterion.setCount(countComboBox.getSelectedIndex());
		else if (index == 1)
			criterion.setCount(1);

		// Build the qualifiers from the third combo box and check boxes
		List<CharacteristicCode> qualifiers = new LinkedList<CharacteristicCode>();
		index = currentComboBox.getSelectedIndex();
		if (index == 1)
			qualifiers.add(COMPLETED);
		if (index == 2)
			qualifiers.add(CONCURRENT);
		if (recentMalignancyCheckBox.isSelected())
			qualifiers.add(MOSTRECENT);
		if (progressionRequiredCheckBox.isSelected())
			qualifiers.add(PROGRESSING);
		if (qualifiers.size() != 0)
			criterion.setQualifiers(qualifiers.toArray(new CharacteristicCode[qualifiers.size()]));

		// Parse threshold if specified
		String thresholdString = (threshold == null) ? null : threshold.getText();
		if ((thresholdString != null) && (thresholdString.trim().length() > 0))
			try
			{
				int thresholdNumber = Integer.parseInt(thresholdString);
				if (thresholdNumber < 0)
					throw new NumberFormatException();
				else if (thresholdNumber > 0)
				{
					criterion.setThreshold(thresholdNumber);
					criterion.setThresholdUnitOfMeasure(thresholdUnitOfMeasure.getSelectedIndex() == 0 ? WEEK
							: (thresholdUnitOfMeasure.getSelectedIndex() == 1 ? MONTH : YEAR));
				}
			}
			catch (NumberFormatException numberFormatException)
			{
				// If cannot parse the field highlight it, wrap the exception
				// and rethrow
				threshold.setSelectionStart(0);
				threshold.setSelectionEnd(thresholdString.length());
				threshold.requestFocusInWindow();
				throw new IllegalStateException("Cannot set threshold to " + thresholdString, numberFormatException);
			}
		return criterion;
	}

	/**
	 * Sets the panel's input elements to match a criterion. Special cases:
	 * <ul>
	 * <li>"any": minimum of 1</li>
	 * <li>"none": maximum of 0</li>
	 * </ul>
	 * 
	 * @param abstractCriterion
	 *            the criterion to set
	 * @throws IllegalArgumentException
	 *             if the count does not fit in the combobox
	 */
	public void setCriterion(AbstractCriterion abstractCriterion)
	{
		// Do nothing if incompatible
		if (!isCompatible(abstractCriterion))
			return;
		// Reset the other fields and set the locations first
		resetFields();
		agentsPanel.setCriterion(abstractCriterion);

		// Prior and concurrent share a combobox
		TreatmentRegimenCriterion criterion = (TreatmentRegimenCriterion) abstractCriterion;
		if (criterion.getQualifiers() != null)
			for (CharacteristicCode qualifier : criterion.getQualifiers())
				if (COMPLETED.equals(qualifier))
					currentComboBox.setSelectedIndex(1);
				else if (CONCURRENT.equals(qualifier))
					currentComboBox.setSelectedIndex(2);
				else if (MOSTRECENT.equals(qualifier))
					recentMalignancyCheckBox.setSelected(true);
				else if (PROGRESSING.equals(qualifier))
					progressionRequiredCheckBox.setSelected(true);

		// Comparator and count are interdependent; first verify that the count
		// can fit
		if ((criterion.getCount() < 0) || (criterion.getCount() >= countComboBox.getItemCount()))
			throw new IllegalArgumentException("Regimen count " + criterion.getCount() + " out of range ["
					+ countComboBox.getItemCount() + ']');
		if (criterion.isInvert())
		{
			if (criterion.getCount() == 0)
				// "No" means maximum of zero
				countComboBox.setEnabled(false);
			else
			{
				comparatorComboBox.setSelectedIndex(2);
				countComboBox.setSelectedIndex(criterion.getCount());
			}
		}
		else
		{
			if (criterion.getCount() <= 1)
			{
				// "Any" means minimum of one
				comparatorComboBox.setSelectedIndex(1);
				countComboBox.setEnabled(false);
			}
			else
				comparatorComboBox.setSelectedIndex(3);
			countComboBox.setSelectedIndex(criterion.getCount());
		}

		// Set settings in the subpanel
		AssociativeCriterion settings = new AssociativeCriterion();
		settings.setParent(SETTING);
		settings.setRequirement(criterion.getSettings());
		settingsPanel.setCriterion(settings);

		// Set the threshold if specified
		CharacteristicCode unit = criterion.getThresholdUnitOfMeasure();
		if ((threshold != null) && (criterion.getThreshold() > 0) && (unit != null))
		{
			threshold.setText(Integer.toString(criterion.getThreshold()));
			if (MONTH.equals(unit))
				thresholdUnitOfMeasure.setSelectedIndex(1);
			else if (YEAR.equals(unit))
				thresholdUnitOfMeasure.setSelectedIndex(2);
		}

		// Finally, set this panel active
		setActive(true);
	}

	/**
	 * Returns the panel's name.
	 * 
	 * @return the panel's name.
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#getLabel()
	 */
	public String getLabel()
	{
		return getName();
	}

	/**
	 * Returns whether a criterion is a treatment regimen criterion.
	 * 
	 * @param abstractCriterion
	 *            the criterion to test
	 * @return whether a criterion is a treatment regimen criterion
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#isCompatible(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	public boolean isCompatible(AbstractCriterion abstractCriterion)
	{
		return (abstractCriterion != null) && (abstractCriterion instanceof TreatmentRegimenCriterion)
				&& agentsPanel.parent.equals(((TreatmentRegimenCriterion) (abstractCriterion)).getParent());
	}

	/**
	 * Version uid for serialization
	 */
	private static final long serialVersionUID = 9199886837915610447L;
}

/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

import java.awt.FlowLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Displays a treatment surgery criterion using an array of checkboxes for
 * surgical procedures. Location and malignancy checkboxes and optional maximum
 * time elapsed is also displayed.
 * 
 * @author Tom Bechtold
 * @version 2008-06-24
 */
class SurgeryCriterionPanel extends JPanel implements AbstractCriterionUserInterface
{
	/**
	 * Holds the surgery types
	 */
	protected final AssociativeCriterionUserInterface typePanel;
	/**
	 * Contains bilateral location comparator
	 */
	protected final JCheckBox locationBilateral;
	/**
	 * Contains recent malignancy comparator
	 */
	protected final JCheckBox recentMalignancy;
	/**
	 * Contains maximum elapsed time comparator
	 */
	protected final JTextField maximumElapsed = new JTextField(4);
	/**
	 * Contains elapsed months comparator
	 */
	protected final JComboBox elapsedMonths;

	private static final CharacteristicCode[] SURGERY_SITE_SET = { SURGERY_BREAST_CONSERVING, BREAST_REEXCISION,
			EXCISIONAL_BIOPSY, MASTECTOMY_UNILATERAL, MASTECTOMY_BILATERAL, SENTINEL_NODE_BIOPSY,
			AXILLARY_NODE_DISSECTION, OOPHORECTOMY, HYSTERECTOMY, SURGERY_BRAIN, SURGERY_SPINAL_CORD, SURGERY_BONE,
			SURGERY_LIVER, SURGERY_LUNG, SURGERY_LYMPH_NODE };

	/**
	 * Adds the type and locations on one row, the bilateral and recent check
	 * boxes on the second row and maximum elapsed time underneath.
	 * 
	 */
	public SurgeryCriterionPanel()
	{
		// Lay out panel as a vertical box
		super(null);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setName(Common.getString(SURGERY.toString()));
		// Add types to the first row using the aggregators
		typePanel = new AssociativeCriterionUserInterface(SURGERY, SURGERY_SITE_SET, SURGERY_AGGREGATORS, true, false);
		typePanel.setAlignmentX(LEFT_ALIGNMENT);
		add(typePanel);
		// Add bilateral location and malignancy underneath
		locationBilateral = new JCheckBox("Bilateral");
		recentMalignancy = new JCheckBox("Recent malignancy");
		JPanel row = new JPanel(new FlowLayout(FlowLayout.LEFT));
		row.setAlignmentX(LEFT_ALIGNMENT);
		//row.add(locationBilateral);
		row.add(recentMalignancy);
		add(row);
		// Add maximum elapsed elements in its own left-justified row
		row = new JPanel(new FlowLayout(FlowLayout.LEFT));
		row.setAlignmentX(LEFT_ALIGNMENT);
		row.add(new JLabel("Maximum time between surgery and enrollment:"));
		row.add(maximumElapsed);
		String[] elapsedMonthsLabels = { "weeks", "months" };
		elapsedMonths = new JComboBox(elapsedMonthsLabels);
		row.add(elapsedMonths);
		add(row);
	}

	/**
	 * Set this component's visibility and clear the input fields
	 * 
	 * @param active
	 *            <code>true</code> to activate, <code>false</code> to
	 *            deactivate
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#setActive(boolean)
	 */
	public void setActive(boolean active)
	{
		// Activation focuses on first component in typePanel; deactivation
		// resets all fields
		typePanel.setActive(active);
		setVisible(active);
		if (!active)
			resetFields();
	}

	/**
	 * Resets all input fields. This method enables all components; it deselects
	 * check boxes and sets the combo box to its first choice.
	 */
	private void resetFields()
	{
		locationBilateral.setSelected(false);
		recentMalignancy.setSelected(false);
		maximumElapsed.setText(null);
		elapsedMonths.setSelectedIndex(0);
	}

	/**
	 * Returns a new criterion that contains the comparator and requirement from
	 * the panel's input elements
	 * 
	 * @return a new criterion
	 * @throws IllegalStateException
	 *             if no procedures were chosen
	 */
	public AbstractCriterion getCriterion() throws IllegalStateException
	{
		// If the type panel does not return anything then return null
		AbstractCriterion abstractCriterion = typePanel.getCriterion();
		if (!isVisible() || (abstractCriterion == null) || !(abstractCriterion instanceof AssociativeCriterion))
			return null;
		// Create the object using the fields from the types object; invert is
		// not available
		TreatmentProcedureCriterion criterion = new TreatmentProcedureCriterion();
		AssociativeCriterion associativeCriterion = (AssociativeCriterion) abstractCriterion;
		criterion.setParent(associativeCriterion.getParent());
		criterion.setRequirement(associativeCriterion.getRequirement());
		criterion.setSetOperation(associativeCriterion.getSetOperation());
		// Build the qualifiers from the check boxes and maximum fields
		List<CharacteristicCode> qualifiers = new LinkedList<CharacteristicCode>();
		if (locationBilateral.isSelected())
			qualifiers.add(BILATERAL_SYNCHRONOUS);
		if (recentMalignancy.isSelected())
			qualifiers.add(MOSTRECENT);
		if (qualifiers.size() != 0)
			criterion.setQualifiers(qualifiers.toArray(new CharacteristicCode[qualifiers.size()]));
		// Parse string into maximum elapsed time
		String elapsed = maximumElapsed.getText();
		if ((elapsed != null) && (elapsed.trim().length() > 0))
			try
			{
				int maximumElapsedTime = Integer.parseInt(elapsed);
				if (maximumElapsedTime < 0)
				{
					maximumElapsed.setSelectionStart(0);
					maximumElapsed.setSelectionEnd(elapsed.length());
					maximumElapsed.requestFocusInWindow();
					throw new IllegalStateException("Cannot set elapsed time to " + elapsed);
				}
				criterion.setMaximumElapsedTime(maximumElapsedTime);
				criterion.setMaximumElapsedTimeUnits(elapsedMonths.getSelectedIndex() == 0 ? WEEK : MONTH);
			}
			catch (NumberFormatException numberFormatException)
			{
				// If cannot parse the elapsed field, wrap the exception and
				// rethrow
				maximumElapsed.setSelectionStart(0);
				maximumElapsed.setSelectionEnd(elapsed.length());
				maximumElapsed.requestFocusInWindow();
				throw new IllegalStateException("Cannot set elapsed time to " + elapsed, numberFormatException);
			}
		return criterion;
	}

	/**
	 * Sets a criterion's values into the interface. This method calls
	 * <code>isCompatible()</code> first. If successful then the component
	 * activates itself. If not successful then the component deactivates
	 * itself.
	 * 
	 * @return <code>true</code> if the criterion was set, <code>false</code> if
	 *         the criterion is not appropriate for this interface
	 * @param abstractCriterion
	 *            the criterion to set
	 * @see #isCompatible(AbstractCriterion)
	 */
	public void setCriterion(AbstractCriterion abstractCriterion)
	{
		// Do nothing if incompatible
		if (!isCompatible(abstractCriterion))
			return;
		// Reset the other fields and set the types first
		typePanel.setCriterion(abstractCriterion);
		resetFields();

		// Set each check boxes depending upon whether qualifiers reference it
		TreatmentProcedureCriterion criterion = (TreatmentProcedureCriterion) (abstractCriterion);
		CharacteristicCode[] qualifiers = criterion.getQualifiers();
		if (qualifiers != null)
			for (CharacteristicCode qualifier : qualifiers)
				if (BILATERAL_SYNCHRONOUS.equals(qualifier))
					locationBilateral.setSelected(true);
				else if (MOSTRECENT.equals(qualifier))
					recentMalignancy.setSelected(true);
		// Set maximum elapsed
		maximumElapsed.setText(criterion.getMaximumElapsedTime() <= 0 ? null : Integer.toString(criterion
				.getMaximumElapsedTime()));
		elapsedMonths.setSelectedIndex(MONTH.equals(criterion.getMaximumElapsedTimeUnits()) ? 1 : 0);

		// Finally, set this panel active
		setActive(true);
	}

	/**
	 * Returns the panel's name.
	 * 
	 * @return the panel's name.
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#getLabel()
	 */
	public String getLabel()
	{
		return getName();
	}

	/**
	 * Returns whether a criterion is appropriate for this interface.
	 * 
	 * @param abstractCriterion
	 *            the criterion to test
	 * @return whether a criterion is appropriate for this interface
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#isCompatible(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	public boolean isCompatible(AbstractCriterion abstractCriterion)
	{
		return (abstractCriterion != null) && (abstractCriterion instanceof TreatmentProcedureCriterion)
				&& SURGERY.equals(((TreatmentProcedureCriterion) (abstractCriterion)).getParent());
	}

	/**
	 * Version uid for serialization
	 */
	private static final long serialVersionUID = -6048272691054284347L;
}

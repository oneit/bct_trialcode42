/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;

import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.Criteria;
import org.quantumleaphealth.model.trial.Criterion;
import org.quantumleaphealth.model.trial.EligibilityCriterion;
import org.quantumleaphealth.model.trial.Trial;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Exports a protocol to Portable Document Format (PDF). This class formats the
 * eligibility criteria by library.
 * 
 * @author Tom Bechtold
 * @version 2008-06-03
 */
class EligibilityCriteriaExport
{
	/**
	 * The font to use for normal text
	 */
	private static final com.lowagie.text.Font FONT = FontFactory.getFont(FontFactory.HELVETICA, 12f);
	/**
	 * The font to use for italic text
	 */
	private static final com.lowagie.text.Font ITALIC = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 12f);
	/**
	 * The font to use for bolded text
	 */
	private static final com.lowagie.text.Font BOLD = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12f);
	/**
	 * The font for the primary id
	 */
	private static final com.lowagie.text.Font FONT_LARGE = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 16f);
	/**
	 * The font for smaller text
	 */
	private static final com.lowagie.text.Font FONT_SMALL = FontFactory.getFont(FontFactory.HELVETICA, 10f);
	/**
	 * The font for smaller text in bold
	 */
	private static final com.lowagie.text.Font FONT_SMALLBOLD = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10f);
	/**
	 * The font for smaller text in italics
	 */
	private static final com.lowagie.text.Font FONT_SMALLITALICS = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE,
			10f);
	/**
	 * The font for smaller text in italics
	 */
	private static final com.lowagie.text.Font FONT_SMALLBOLDITALICS = FontFactory.getFont(
			FontFactory.HELVETICA_BOLDOBLIQUE, 10f);

	/**
	 * The ratio of the widths of the table columns
	 */
	private static final float[] CATEGORY_WIDTH = { 1f, 9f };

	/**
	 * The ratio of the widths for IF/THEN criteria table
	 */
	private static final float[] IFTHEN_WIDTH = { 1f, 9f };

	/**
	 * Formats date/times
	 */
	private static final DateFormat FORMATTER = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);

	/**
	 * Adds a criterion/criteria cell to a table. Each added cell has no border.
	 * 
	 * @param parent
	 *            the parent table to add the matchable to
	 * @param criterion
	 *            the criteria/criterion to represent
	 * @param userInterface
	 *            the user interface used to build criteria
	 */
	private static void addCell(PdfPTable parent, Criterion criterion, CriteriaUserInterface userInterface)
	{
		// Validate parameters
		if ((parent == null) || (criterion == null))
			return;

		// Generator for criterion's text is in panel class
		if (criterion instanceof AbstractCriterion)
		{
			AbstractCriterion abstractCriterion = (AbstractCriterion) (criterion);
			PdfPCell cell = new PdfPCell(new Phrase(userInterface.getDescription(abstractCriterion), FONT));
			cell.setBorder(0);
			parent.addCell(cell);
			return;
		}
		// Ignore invalid criteria (less than two children)
		if (!(criterion instanceof Criteria))
			return;
		Criteria criteria = (Criteria) (criterion);
		if (criteria.getChildren().size() < 2)
			return;

		// If/Then operator displays the first two children on their own lines
		// in inner table
		if (criteria.getOperator() == Criteria.Operator.IF)
		{
			PdfPTable table = new PdfPTable(IFTHEN_WIDTH);
			PdfPCell cell = new PdfPCell(new Phrase(Common.getString(Common.PREFIX_OPERATOR
					+ Criteria.Operator.IF.toString() + Common.SEPARATOR + Common.SUFFIX_IF), FONT));
			cell.setBorder(0);
			table.addCell(cell);
			addCell(table, criteria.getChildren().get(0), userInterface);
			cell = new PdfPCell(new Phrase(Common.getString(Common.PREFIX_OPERATOR + Criteria.Operator.IF.toString()
					+ Common.SEPARATOR + Common.SUFFIX_THEN), FONT));
			cell.setBorder(0);
			table.addCell(cell);
			addCell(table, criteria.getChildren().get(1), userInterface);
			cell = new PdfPCell(table);
			cell.setBorder(0);
			parent.addCell(cell);
			return;
		}

		// Other operator: display each child in its own table row
		PdfPTable table = new PdfPTable(1);
		addCell(table, criteria.getChildren().get(0), userInterface);
		String conjunction = Common.getString(Common.PREFIX_OPERATOR + criteria.getOperator());
		for (int index = 1; index < criteria.getCount(); index++)
		{
			PdfPCell cell = new PdfPCell(new Phrase(conjunction, FONT));
			cell.setBorder(0);
			table.addCell(cell);
			addCell(table, criteria.getChildren().get(index), userInterface);
		}
		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);
		parent.addCell(cell);
	}

	/**
	 * Exports eligibility criteria into a PDF format. This method writes the
	 * protocol name, primary identifier and summary information to the top of
	 * the page. It then lists the eligibility criteria using a table for each
	 * library.
	 * 
	 * @param trial
	 *            the protocol to export
	 * @param outputStream
	 *            the output stream to write the format to
	 * @param userInterface
	 *            the user interface used to build criteria
	 * @param logo
	 *            optional logo to place in upper-right corner
	 * @throws IOException
	 *             if the output stream could not be written to
	 */
	static void write(Trial trial, OutputStream outputStream, CriteriaUserInterface userInterface, URL logo)
			throws IOException
	{
		// Validate parameters
		if (trial == null)
			throw new IllegalArgumentException("Trial not specified");
		if (outputStream == null)
			throw new IllegalArgumentException("Output stream not specified");
		// Create new letter-sized document
		Document document = new Document(PageSize.LETTER);
		try
		{
			PdfWriter.getInstance(document, outputStream);
			document.open();
			// Header contains logo and protocol's scalar fields
			if (logo != null)
				try
				{
					Image image = Image.getInstance(logo);
					image.setAlignment(Image.RIGHT | Image.TEXTWRAP);
					document.add(image);
				}
				catch (Throwable throwable)
				{
					// Logo is not important, so ignore any errors
				}
			addPhrase(document, trial.getPrimaryID(), FONT_LARGE, null, null);
			if ((trial.getName() != null) && (trial.getName().trim().length() > 0))
				addPhrase(document, ' ' + trial.getName().trim(), FONT_LARGE, null, null);
			document.add(Chunk.NEWLINE);
			addPhrase(document, trial.getTitle(), FONT_SMALLITALICS, null, null);
			document.add(Chunk.NEWLINE);
			addPhrase(document, trial.getSponsor(), FONT_SMALL, "Sponsor: ", FONT_SMALLBOLD);
			document.add(Chunk.NEWLINE);
			addPhrase(document, trial.getPurpose(), FONT_SMALL, "Purpose: ", FONT_SMALLBOLD);
			document.add(Chunk.NEWLINE);
			addPhrase(document, trial.getTreatmentPlan(), FONT_SMALL, "Treatment Plan: ", FONT_SMALLBOLD);
			document.add(Chunk.NEWLINE);
			addPhrase(document, trial.getProcedures(), FONT_SMALL, "Procedures: ", FONT_SMALLBOLD);
			document.add(Chunk.NEWLINE);
			addPhrase(document, trial.getBurden(), FONT_SMALL, "Burden: ", FONT_SMALLBOLD);
			document.add(Chunk.NEWLINE);
			addPhrase(document, trial.getDuration(), FONT_SMALL, "Duration: ", FONT_SMALLBOLD);
			document.add(Chunk.NEWLINE);
			addPhrase(document, trial.getFollowup(), FONT_SMALL, "Follow-up: ", FONT_SMALLBOLD);
			document.add(Chunk.NEWLINE);
			long eligibilityCriteriaLastModified = (trial.getEligibilityCriteria() == null) ? 0l : trial
					.getEligibilityCriteria().getLastModified();
			if ((trial.getLastModified() != null) || (eligibilityCriteriaLastModified > 0l))
			{
				if (trial.getLastModified() != null)
					addPhrase(document, FORMATTER.format(trial.getLastModified()), FONT_SMALLITALICS,
							"Last modified: ", FONT_SMALLBOLDITALICS);
				if (eligibilityCriteriaLastModified > 0l)
				{
					if (trial.getLastModified() != null)
						addPhrase(document, "; ", FONT_SMALLITALICS, null, null);
					addPhrase(document, FORMATTER.format(new Date(eligibilityCriteriaLastModified)), FONT_SMALLITALICS,
							"Criteria modified: ", FONT_SMALLBOLDITALICS);
				}
				document.add(Chunk.NEWLINE);
			}

			// Loop over all criteria, creating a table for each library
			if (trial.getEligibilityCriteria() == null)
				return;
			int count = 0;
			PdfPTable table = null;
			String currentLibrary = null;
			for (EligibilityCriterion eligibilityCriterion : trial.getEligibilityCriteria())
			{
				// Validate library name
				if (eligibilityCriterion.getLibrary() == null)
					throw new IOException("Eligibility criterion's library not available");
				// If library changed then display library name and create new
				// table
				if (!eligibilityCriterion.getLibrary().equals(currentLibrary))
				{
					if (table != null)
						document.add(table);
					// The two-column table spans the entire width of the
					// document
					table = new PdfPTable(CATEGORY_WIDTH);
					table.setWidthPercentage(100f);
					table.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
					currentLibrary = eligibilityCriterion.getLibrary();
					document
							.add(new Paragraph(currentLibrary, FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 14f)));
					count = 0;
				}
				// Add number and text on first row
				PdfPCell cell = new PdfPCell(new Phrase(Integer.toString(++count) + Common.SEPARATOR, FontFactory
						.getFont(FontFactory.HELVETICA, 12f)));
				cell.setBorder(0);
				table.addCell(cell);
				String string = Common.trimString(eligibilityCriterion.getText());
				cell = new PdfPCell(new Phrase(string == null ? "<not specified>" : string, FONT));
				cell.setBorder(0);
				table.addCell(cell);
				// Add qualifier on its own row in italics if available
				string = Common.trimString(eligibilityCriterion.getQualifier());
				if (string != null)
				{
					cell = new PdfPCell();
					cell.setBorder(0);
					table.addCell(cell);
					cell = new PdfPCell(new Phrase(string, ITALIC));
					cell.setBorder(0);
					table.addCell(cell);
				}
				// Add bolded type and matchable if available
				if (eligibilityCriterion.getCriterion() != null)
				{
					cell = eligibilityCriterion.getType() == null ? new PdfPCell() : new PdfPCell(new Phrase(Common
							.getString(Common.PREFIX_ACTION + eligibilityCriterion.getType().name()), BOLD));
					cell.setBorder(0);
					table.addCell(cell);
					addCell(table, eligibilityCriterion.getCriterion(), userInterface);
				}
			}
			// Write the table for the final library
			if (table != null)
				document.add(table);
		}
		catch (DocumentException documentException)
		{
			// Wrap any document writing problems into an IOException
			throw new IOException(documentException.getMessage());
		}
		finally
		{
			try
			{
				document.close();
			}
			catch (Throwable throwable)
			{
				// Recast as IO Exception
				throw new IOException(throwable.getMessage());
			}
		}
	}

	/**
	 * Adds a phrase to a document. If either <code>document</code> or
	 * <code>string</code> is empty then this method does nothing.
	 * 
	 * @param document
	 *            the document to add to
	 * @param string
	 *            the string to add to
	 * @param font
	 *            the font to use or <code>null</code> for default font
	 * @param label
	 *            the label to prepend
	 * @param font
	 *            the font to use for the label or <code>null</code> for default
	 *            font
	 * @throws DocumentException
	 *             if the document is not open
	 */
	private static void addPhrase(Document document, String string, Font font, String label, Font fontLabel)
			throws DocumentException
	{
		if ((document == null) || (string == null) || (string.trim().length() == 0))
			return;
		if ((label != null) && (label.trim().length() > 0))
			document.add(fontLabel == null ? new Phrase(label) : new Phrase(label, fontLabel));
		document.add(font == null ? new Phrase(string) : new Phrase(string, font));
	}
}

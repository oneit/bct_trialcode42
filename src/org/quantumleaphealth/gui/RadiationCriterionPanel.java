/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.COMPLETED;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CONCURRENT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.WHOLE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PARTIAL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MOSTRECENT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION_AGGREGATORS;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION_BREAST_CONTRALATERAL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION_BREAST_IPSILATERAL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.SPINAL_CORD;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BONE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CHESTWALL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LYMPH_NODE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.THORAX;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LIVER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LUNG;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_SKIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.OVARIAN_RADIOTHERAPY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_LEPTOMENINGES;

import java.awt.Dimension;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Displays a radiation treatment procedure criterion.
 * 
 * @author Tom Bechtold
 * @version 2008-06-24
 */
class RadiationCriterionPanel extends JPanel implements AbstractCriterionUserInterface
{
	/**
	 * Contains the prior/concurrent comparator. Its maximum size is restricted
	 * to its preferred size.
	 */
	private final JComboBox currentComboBox = new PreferredComboBox();
	/**
	 * Contains malignancy comparators
	 */
	private final JComboBox malignancyComboBox = new PreferredComboBox();

	/**
	 * A JComboBox whose maximum size is restricted to its preferred size.
	 */
	private class PreferredComboBox extends JComboBox
	{
		/**
		 * Returns the component's preferred size
		 * 
		 * @see javax.swing.JComponent#getMaximumSize()
		 */
		@Override
		public Dimension getMaximumSize()
		{
			return getPreferredSize();
		}

		// Version for serialization
		private static final long serialVersionUID = 276171609992846443L;
	}

	/**
	 * Displays locations
	 */
	private final AssociativeCriterionUserInterface locationPanel;

	/**
	 * Displays interventions
	 */
	private final AssociativeCriterionUserInterface interventionPanel;

	/**
	 * The set of radiation therapy techniques
	 */
	public static final CharacteristicCode[] RADIATION_TECHNIQUE_SET = { WHOLE, PARTIAL };

	/**
	 * The set of sites for radiation therapy
	 */
	public static final CharacteristicCode[] RADIATION_SITE_SET = { RADIATION_BREAST_IPSILATERAL,
			RADIATION_BREAST_CONTRALATERAL, BRAIN, SPINAL_CORD, BONE, CHESTWALL, LYMPH_NODE, OVARIAN_RADIOTHERAPY,
			THORAX, LIVER, LUNG, DIAGNOSISAREA_SKIN, DIAGNOSISAREA_LEPTOMENINGES};

	/**
	 * Adds the two combobox comparators and the interventions checkbox group to
	 * one row and sites to the bottom section. The sites are added by the
	 * superclass so these other input elements are inserted on top of them.
	 * 
	 * @param attribute
	 *            the attribute to represent
	 * @param bundle
	 *            the resource bundle used to localize text
	 * @throws IllegalArgumentException
	 *             if the attribute is <code>null</code> or not of type
	 *             <code>RADIATION</code>
	 */
	public RadiationCriterionPanel() throws IllegalArgumentException
	{
		// Lay out panel as a vertical box
		super(null);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setName(Common.getString(RADIATION.toString()));
		// Current combobox supports both prior and concurrent fields and their
		// inverse
		currentComboBox.addItem("Prior and concurrent");
		currentComboBox.addItem("Prior");
		currentComboBox.addItem("Concurrent");
		currentComboBox.addItem("Neither prior nor concurrent");
		currentComboBox.addItem("No prior");
		currentComboBox.addItem("No concurrent");
		// Malignancy combobox supports any, breast cancer or recent
		malignancyComboBox.addItem("For any malignancy");
		malignancyComboBox.addItem("For breast cancer");
		malignancyComboBox.addItem("For recent malignancy");
		// Interventions allow any site
		interventionPanel = new AssociativeCriterionUserInterface(RADIATION, RADIATION_TECHNIQUE_SET, null, true, false);

		// Place left-aligned combo boxes in first column, intervention
		// top-aligned at right
		Box column = Box.createVerticalBox();
		currentComboBox.setAlignmentX(LEFT_ALIGNMENT);
		malignancyComboBox.setAlignmentX(LEFT_ALIGNMENT);
		column.add(currentComboBox);
		column.add(malignancyComboBox);
		Box row = Box.createHorizontalBox();
		column.setAlignmentY(TOP_ALIGNMENT);
		interventionPanel.setAlignmentY(TOP_ALIGNMENT);
		row.add(column);
		row.add(interventionPanel);
		row.setAlignmentX(LEFT_ALIGNMENT);
		add(row);

		// Add locations underneath
		locationPanel = new AssociativeCriterionUserInterface(RADIATION, RADIATION_SITE_SET, RADIATION_AGGREGATORS,
				true, false);
		locationPanel.setAlignmentX(LEFT_ALIGNMENT);
		add(locationPanel);
	}

	/**
	 * Set this component's visibility and clear the input fields
	 * 
	 * @param active
	 *            <code>true</code> to activate, <code>false</code> to
	 *            deactivate
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#setActive(boolean)
	 */
	public void setActive(boolean active)
	{
		// Activation focuses on current combobox; deactivation resets all
		// fields
		interventionPanel.setActive(active);
		locationPanel.setActive(active);
		setVisible(active);
		if (active)
			currentComboBox.requestFocusInWindow();
		else
			resetFields();
	}

	/**
	 * Resets all input fields. This method sets each combo box to its first
	 * choice.
	 */
	private void resetFields()
	{
		currentComboBox.setSelectedIndex(0);
		malignancyComboBox.setSelectedIndex(0);
	}

	/**
	 * Returns a new criterion that contains the comparator and requirement from
	 * the panel's input elements
	 * 
	 * @return a new criterion
	 * @throws IllegalStateException
	 *             if no procedures were chosen
	 */
	public AbstractCriterion getCriterion() throws IllegalStateException
	{
		// If the location panel does not return anything then return null
		AbstractCriterion abstractCriterion = locationPanel.getCriterion();
		if (!isVisible() || (abstractCriterion == null) || !(abstractCriterion instanceof AssociativeCriterion))
			return null;
		// Create the object using the fields from the location panel
		TreatmentProcedureCriterion criterion = new TreatmentProcedureCriterion();
		AssociativeCriterion associativeCriterion = (AssociativeCriterion) abstractCriterion;
		criterion.setParent(associativeCriterion.getParent());
		criterion.setRequirement(associativeCriterion.getRequirement());
		criterion.setSetOperation(associativeCriterion.getSetOperation());
		// Build qualifiers and invert from the combo boxes
		List<CharacteristicCode> qualifiers = new LinkedList<CharacteristicCode>();
		int index = currentComboBox.getSelectedIndex();
		if (index >= (currentComboBox.getItemCount() / 2))
		{
			criterion.invert();
			index -= currentComboBox.getItemCount() / 2;
		}
		if (index == 1)
			qualifiers.add(COMPLETED);
		else if (index == 2)
			qualifiers.add(CONCURRENT);
		index = malignancyComboBox.getSelectedIndex();
		if (index == 1)
			qualifiers.add(BREAST_CANCER);
		else if (index == 2)
			qualifiers.add(MOSTRECENT);
		if (qualifiers.size() != 0)
			criterion.setQualifiers(qualifiers.toArray(new CharacteristicCode[qualifiers.size()]));
		// Get the interventions from the nested panel
		AssociativeCriterion interventions = (AssociativeCriterion) interventionPanel.getCriterion();
		criterion.setInterventions(interventions.getRequirement());

		return criterion;
	}

	/**
	 * Sets the panel's input elements to match a criterion.
	 * 
	 * @param abstractCriterion
	 *            the criterion to set
	 */
	public void setCriterion(AbstractCriterion abstractCriterion)
	{
		// Do nothing if incompatible
		if (!isCompatible(abstractCriterion))
			return;
		// Reset the other fields and set the locations first
		resetFields();
		locationPanel.setCriterion(abstractCriterion);

		// Prior and concurrent share a combobox; inverted choices are found in
		// second half
		TreatmentProcedureCriterion criterion = (TreatmentProcedureCriterion) abstractCriterion;
		LinkedList<CharacteristicCode> qualifiers = (criterion.getQualifiers() == null) ? new LinkedList<CharacteristicCode>()
				: new LinkedList<CharacteristicCode>(Arrays.asList(criterion.getQualifiers()));
		int index = 0;
		if (qualifiers.remove(COMPLETED))
			index = 1;
		if (qualifiers.remove(CONCURRENT))
			index = 2;
		if (criterion.isInvert())
			index += currentComboBox.getItemCount() / 2;
		currentComboBox.setSelectedIndex(index);

		// Breast cancer and recent malignancy share a combobox
		index = 0;
		if (qualifiers.remove(BREAST_CANCER))
			index = 1;
		if (qualifiers.remove(MOSTRECENT))
			index = 2;
		malignancyComboBox.setSelectedIndex(index);

		// Set interventions
		AssociativeCriterion interventions = new AssociativeCriterion();
		interventions.setParent(RADIATION);
		interventions.setRequirement(criterion.getInterventions());
		interventionPanel.setCriterion(interventions);

		// Finally, set this panel active
		setActive(true);
	}

	/**
	 * Returns the panel's name.
	 * 
	 * @return the panel's name.
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#getLabel()
	 */
	public String getLabel()
	{
		return getName();
	}

	/**
	 * Returns whether a criterion is a radiation criterion.
	 * 
	 * @param abstractCriterion
	 *            the criterion to test
	 * @return whether a criterion is a radiation criterion
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#isCompatible(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	public boolean isCompatible(AbstractCriterion abstractCriterion)
	{
		return (abstractCriterion != null) && (abstractCriterion instanceof TreatmentProcedureCriterion)
				&& RADIATION.equals(((TreatmentProcedureCriterion) (abstractCriterion)).getParent());
	}

	/**
	 * Version uid for serialization
	 */
	private static final long serialVersionUID = 2922979448955845910L;
}

/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

import org.quantumleaphealth.model.trial.EligibilityCriterion;

/**
 * Parses eligibility criteria from text.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public class EligibilityCriteriaParser
{
	/**
	 * Minimum length of parsed line
	 */
	private static final int SHORT_LINE = 2;

	/**
	 * Do not allow instantiation of this class
	 */
	private EligibilityCriteriaParser()
	{
	}

	/**
	 * Returns a list of eligibility criteria parsed from the system clipboard.
	 * This method does not categorize or otherwise filter the contents; it
	 * simply constructs a new criterion for each non-blank line.
	 * 
	 * @return a list of eligibility criteria or <code>null</code> if no
	 *         criteria found
	 * @param userInterface
	 *            the user interface used to build the criteria
	 */
	static List<EligibilityCriterion> parseSystemClipboard(CriteriaUserInterface userInterface)
			throws UnsupportedOperationException
	{
		// If nothing in the clipboard then do nothing
		Transferable contents = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
		if (contents == null)
			return null;
		// The default library is null
		String libraryName = null;
		BufferedReader reader = null;
		ArrayList<EligibilityCriterion> list = new ArrayList<EligibilityCriterion>();
		try
		{
			// Get the best unicode plain text flavor
			reader = new BufferedReader(DataFlavor.getTextPlainUnicodeFlavor().getReaderForText(contents));
			while (true)
			{
				String line = reader.readLine();
				if (line == null)
					break;
				// If there is only one letter and it is capital at end of line
				// then update the library name
				line = line.trim();
				if (userInterface != null)
				{
					// Find first letter
					int index = -1;
					while (++index < line.length())
						if (Character.isLetter(line.charAt(index)))
							break;
					// If first letter is capital and at end of line then update
					// library name
					if ((index == line.length() - 1) && Character.isUpperCase(line.charAt(index)))
					{
						char firstLetter = line.charAt(index);
						libraryName = null;
						for (String userInterfacelibraryName : userInterface.getLibraryNames())
							if (userInterfacelibraryName.charAt(0) == firstLetter)
							{
								libraryName = userInterfacelibraryName;
								break;
							}
						continue;
					}
				}
				// Ignore short lines
				if (line.length() < SHORT_LINE)
					continue;
				EligibilityCriterion criterion = new EligibilityCriterion();
				criterion.setText(line);
				if (libraryName != null)
					criterion.setLibrary(libraryName);
				list.add(criterion);
			}
		}
		catch (Exception exception)
		{
			// Recast as unsupported operation exception
			throw new UnsupportedOperationException(exception);
		}
		finally
		{
			try
			{
				reader.close();
			}
			catch (Throwable throwable)
			{
			}
		}
		return list;
	}
}

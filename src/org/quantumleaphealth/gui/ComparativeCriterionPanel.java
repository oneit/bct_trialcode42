/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.gui;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.quantumleaphealth.model.trial.AbstractCriterion;
import org.quantumleaphealth.model.trial.ComparativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Displays a comparative criterion using a combo-box for minimum/maximum
 * comparator, text field for requirement and optional combobox for units of
 * measure.
 * 
 * @author Tom Bechtold
 * @version 2008-02-22
 */
class ComparativeCriterionPanel extends JPanel implements AbstractCriterionUserInterface
{

	/**
	 * The characteristic
	 */
	private final CharacteristicCode characteristicCode;

	/**
	 * The units of measure or <code>null</code> for simple count
	 */
	private final CharacteristicCode[] unitsOfMeasure;

	/**
	 * Contains minimum and maximum comparators
	 */
	private final JComboBox comparator = new JComboBox();

	/**
	 * Contains units of measure or <code>null</code> if there is only one unit
	 */
	private final JComboBox units;

	/**
	 * Contains the requirement
	 */
	private final JTextField requirement = new JTextField(4);

	/**
	 * Adds the comparator combobox (minimum and maximum), numeric requirement
	 * text field and (optional) units of measure combobox to one left-justified
	 * row. When the comparator changes the requirement field is focused and
	 * selected.
	 * 
	 * @param characteristicCode
	 *            the characteristic
	 * @param unitsOfMeasure
	 *            the units of measure or <code>null</code> for simple count
	 * @throws IllegalArgumentException
	 *             if <code>characteristicCode</code> is <code>null</code>
	 */
	public ComparativeCriterionPanel(CharacteristicCode characteristicCode, CharacteristicCode[] unitsOfMeasure)
			throws IllegalArgumentException
	{
		// Use default centered flow layout and store parameters
		super();
		this.characteristicCode = characteristicCode;
		this.unitsOfMeasure = unitsOfMeasure;
		if (characteristicCode == null)
			throw new IllegalArgumentException("must specify characteristic");
		String key = characteristicCode.toString();
		setName(Common.getString(key));
		// Comparator uses static labels
		comparator.addItem("Minimum:");
		comparator.addItem("Maximum:");
		// When the comparator is selected then select and focus the requirement
		comparator.addItemListener(new ItemListener()
		{
			/**
			 * Selects and sets focus to the requirement
			 */
			public void itemStateChanged(ItemEvent itemEvent)
			{
				requirement.setSelectionStart(0);
				requirement.setSelectionEnd(requirement.getText().length());
				requirement.requestFocusInWindow();
			}
		});
		// Place the comparator and requirement into one row
		add(comparator);
		add(requirement);
		// Support one unit of measure as a label, multiple units of measure in
		// combobox
		int count = (unitsOfMeasure == null) ? 0 : unitsOfMeasure.length;
		units = (count > 1) ? new JComboBox() : null;
		if (count == 1)
			add(new JLabel(Common.getString(key + '.' + unitsOfMeasure[0].toString())));
		else if (count > 1)
		{
			for (CharacteristicCode unit : unitsOfMeasure)
				units.addItem(Common.getString(key + '.' + unit.toString()));
			add(units);
		}
	}

	/**
	 * Set this component's visibility and clear the input fields
	 * 
	 * @param active
	 *            <code>true</code> to activate, <code>false</code> to
	 *            deactivate
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#setActive(boolean)
	 */
	public void setActive(boolean active)
	{
		setVisible(active);
		// Activation selects text field; deactivation resets all fields
		if (active)
		{
			requirement.requestFocusInWindow();
			requirement.setSelectionStart(0);
			requirement.setSelectionEnd(requirement.getText().length());
		}
		else
			resetFields();
	}

	/**
	 * Resets all input fields. This method enables all components; it deselects
	 * check boxes and sets the combo box to its first choice.
	 */
	private void resetFields()
	{
		comparator.setSelectedIndex(0);
		requirement.setText(null);
		if (units != null)
			units.setSelectedIndex(0);
	}

	/**
	 * Returns a new criterion that contains the comparator, requirement and
	 * optional units of measure from the panel's input elements
	 * 
	 * @return a new criterion or <code>null</code> if this panel is not visible
	 * @throws IllegalStateException
	 *             if the number cannot be parsed
	 */
	public AbstractCriterion getCriterion() throws IllegalStateException
	{
		// If invisible then return nothing
		if (!isVisible())
			return null;
		// Validate an entry
		if ((requirement.getText() == null) || (requirement.getText().trim().length() == 0))
			throw new IllegalStateException("Please enter a number for " + getName() + '.');
		// Store characteristic, min/max and parse number
		ComparativeCriterion comparativeCriterion = new ComparativeCriterion();
		comparativeCriterion.setCharacteristicCode(characteristicCode);
		if (comparator.getSelectedIndex() > 0)
			comparativeCriterion.invert();
		try
		{
			comparativeCriterion.setRequirement(Double.parseDouble(requirement.getText()));
		}
		catch (NumberFormatException numberFormatException)
		{
			// NumberFormatException means the input is not a number
			throw new IllegalStateException('"' + requirement.getText() + "\" is not a number for the " + getName()
					+ " criterion.");
		}
		// Store optional unit of measure
		if ((unitsOfMeasure != null) && (unitsOfMeasure.length > 0))
			comparativeCriterion.setUnitOfMeasure(unitsOfMeasure[units == null ? 0 : units.getSelectedIndex()]);
		return comparativeCriterion;
	}

	/**
	 * Sets the comparator and numeric requirement into the panel's input
	 * elements.
	 * 
	 * @param abstractCriterion
	 *            the criterion to set
	 * @see #isCompatible(AbstractCriterion)
	 */
	public void setCriterion(AbstractCriterion abstractCriterion)
	{
		// Do nothing if incompatible
		if (!isCompatible(abstractCriterion))
			return;
		// Set all fields and activate
		resetFields();
		ComparativeCriterion comparativeCriterion = (ComparativeCriterion) abstractCriterion;
		comparator.setSelectedIndex(comparativeCriterion.isInvert() ? 1 : 0);
		requirement.setText(Double.toString(comparativeCriterion.getRequirement()));
		// If provided, verify that unit of measure is supported in this panel
		CharacteristicCode unit = comparativeCriterion.getUnitOfMeasure();
		if ((unitsOfMeasure != null) && (unitsOfMeasure.length > 0))
		{
			int index = 0;
			while ((index < unitsOfMeasure.length) && (unitsOfMeasure[index] != unit))
				index++;
			// Provided unit is not supported by this panel
			if (index == unitsOfMeasure.length)
				throw new IllegalStateException("Unit of measure " + unit + " not supported in " + getName());
			else if (index > 0)
				units.setSelectedIndex(index);
		}
		else if (unit != null)
			throw new IllegalStateException("Unit of measure " + unit + " not supported in " + getName());
		setActive(true);
	}

	/**
	 * Returns the panel's name.
	 * 
	 * @return the panel's name.
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#getLabel()
	 */
	public String getLabel()
	{
		return getName();
	}

	/**
	 * Returns whether a criterion is appropriate for this interface.
	 * 
	 * @param criterion
	 *            the criterion to test
	 * @return whether a criterion is appropriate for this interface
	 * @see org.quantumleaphealth.gui.AbstractCriterionUserInterface#isCompatible(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	public boolean isCompatible(AbstractCriterion abstractCriterion)
	{
		if ((abstractCriterion == null) || !(abstractCriterion instanceof ComparativeCriterion))
			return false;
		CharacteristicCode otherCode = ((ComparativeCriterion) (abstractCriterion)).getCharacteristicCode();
		return (characteristicCode == null) ? (otherCode == null) : characteristicCode.equals(otherCode);
	}

	/**
	 * Version uid for serialization
	 */
	private static final long serialVersionUID = -1794104808451872937L;
}
